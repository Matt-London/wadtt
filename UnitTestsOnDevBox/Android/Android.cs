﻿//+++++++++++++++++++++++++++++++++
// DUMMY VERSION FOR UNIT TESTING +
//+++++++++++++++++++++++++++++++++

namespace Android.Content
{
    public interface Context
    {

    }
}

namespace Android.Content.Res
{
    public class Resources
    {

    }
}

namespace Android.Util
{
    public class Log
    {
        public static int Debug(string tag, string format)
        {
            //Console.WriteLine("{0}: {1}", tag, format);
            return 0;
        }

        public static int Debug(string tag, string format, int arg1)
        {
            //Console.WriteLine("{0}: {1} {2}", tag, format, arg1);
            return 0;
        }

        public static int Debug(string tag, string format, int arg1, int arg2)
        {
            return 0;
        }

        public static int Debug(string tag, string format, int arg1, int arg2,
                                int arg3, int arg4, int arg5)
        {
            return 0;
        }

        public static int Debug(string tag, string format, string arg1)
        {
            //Console.WriteLine("{0}: {1} {2}", tag, format, arg1);
            return 0;
        }

        public static int Debug(string tag, string format, int arg1, string arg2)
        {
            //Console.WriteLine("{0}: {1} {2}", tag, format, arg1, arg2);
            return 0;
        }

        public static int Debug(string tag, string format, string arg1, int arg2)
        {
            //Console.WriteLine("{0}: {1} {2}", tag, format, arg1, arg2);
            return 0;
        }

        public static int Error(string tag, string message)
        {
            return 0;
        }
    }
}

namespace Android.Speech.Tts
{
    public class TextToSpeech
    {
        public interface IOnInitListener
        {
        }

        public TextToSpeech(Android.App.Activity activity, IOnInitListener listener)
        {

        }
    }
}

namespace Android.App
{
    public class Activity
    {
        public void FinishAffinity()
        {

        }
    }
}

namespace Android.OS
{
    public class Process
    {
        public static int KillProcess(int pid) { return 0;  }
        public static int MyPid() { return 0; }
    }
}

namespace Java.Util
{
    public class Random
    {
        public bool NextBoolean() { return true; }
    }
}