//===================
// UNIT TEST VERSION.
//===================
namespace wadtt
{
    public class Item
    {
        //[PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Synonyms { get; set; }

        public int Weight { get; set; }
        public bool Floats { get; set; }
        public bool Waterproof { get; set; }
        public State Empty { get; set; }
        public bool Perishable { get; set; }
        public int LifeSpan { get; set; }

        public enum State { empty, full, solid };
        public Item() { }   // Needed for DB creation.
    }
}