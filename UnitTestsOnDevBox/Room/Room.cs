//+++++++++++++++++++++++++++++++++
// DUMMY VERSION FOR UNIT TESTING +
//+++++++++++++++++++++++++++++++++
namespace wadtt
{
    public class Room : IRoom
    {
        public int ID { get; set; }
        public string Desc { get; set; }
        public int Level { get; set; }  // 0 is low-water level. Tides may vary.
        public Sound.Noise Sound { get; set; }
        public int Left { get; set; }
        public int Straight { get; set; }
        public int Right { get; set; }
        public string Contents { get; set; }
        public string Floor { get; set; }
        public Rooms.Style RoomStyle { get; set; }

        public static int GetRoomCount()
        {
            return 10;  // This can be set by the test!
        }

        public string getContents()
        {
            return "knife"; // This can be set by the test!
        }

        public string getRawContents()
        {
            return "1,2"; // This can be set by the test!
        }

        // FIXME: This will become more intelligent based on what we carry and tides..
        public int getTunnelCount()
        {
            return 3; // This can be set by the test!
        }

        public string getLeading()
        {
            return "There are passages leading straight up";  // Test can change.
        }

        // In the real world this is SQL.
        public static Room GetRoomById(int id)
        {
            Room room = new Room();
            room.ID = id;

            switch (id)
            {
                case 10:
                    room.Desc = "The main entrance";
                    room.Level = 1;
                    room.Left = 1;
                    room.Right = 1;
                    room.Straight = 1;
                    room.Contents = "1,2,3,1,4,1";
                    break;

                case 1:
                    room.Desc = "The primary cave";
                    room.Level = 4;
                    room.Left = 2;
                    room.Right = 2;
                    room.Straight = 2;
                    room.Contents = "1,2,3,1,4,1";
                    break;

                case 2:
                    room.Desc = "The secondary cave";
                    room.Level = 7;
                    room.Left = 3;
                    room.Right = 3;
                    room.Straight = 3;
                    room.Contents = "1,2,3,1,4,1";
                    break;

                case 3:
                    room.Desc = "The tertiary cave";
                    room.Level = 2;
                    room.Left = 27;
                    room.Right = 27;
                    room.Straight = 27;
                    room.Contents = "1,2,3,1,4,1";
                    break;

                case 27:
                    room.Desc = "A small cave entrance";
                    room.Level = 1;
                    room.Left = 10;
                    room.Right = 10;
                    room.Straight = 2;
                    room.Contents = "1,2,3,1,4,1";
                    break;

                default:
                    break;
            }

            return room;
        }
    }
}