﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using wadtt;
using Android.Content.Res;   // This is a dummy test when in test mode.
using Android.App;
using Android.Speech.Tts;
using System.Collections.Generic;

namespace UnitTestsonDevbox
{
    [TestClass]
    public class GameTests
    {
        // The bare minimum for a Player to enter the Game in a given Environment.
        //
        //  Game game = new Game();
        //  IEnvironment env = new Environment(game, "Cave");
        //
        //  IPlayer player = new Player("Matt");
        //  game.register(player);
        //  player.register(env);
        //  player.enter(env);
        //
        //
        [TestMethod]
        public void GameTest()
        {
            Console.WriteLine("+++ Start GameTest()");
            Resources resources = new Resources();
            IDatabase database = new Database();
            Activity activity = new Activity();
            TextToSpeech.IOnInitListener listener = null;

            Game game = new Game("Game", database, activity, listener);
            Assert.AreEqual(0, game.getRegisteredPlayers().Count);

            // First we register a few environments with a Game.
            IEnvironment cave = new wadtt.Environment(game, "Cave");
            Assert.AreEqual("Cave", cave.Name);
            Assert.AreEqual(false, game.register(cave));
            Assert.AreEqual("Cave", game.getRegisteredEnvironment("Cave").Name);

            IEnvironment village = new wadtt.Environment(game, "Village");
            Assert.AreEqual("Village", village.Name);
            Assert.AreEqual(false, game.register(village));
            Assert.AreEqual(2, game.getRegisteredEnvironments().Count);

            IEnvironment castle = new wadtt.Environment(game, "Castle");
            Assert.AreEqual("Castle", castle.Name);
            Assert.AreEqual(false, game.register(castle));
            Assert.AreEqual(3, game.getRegisteredEnvironments().Count);

            Assert.AreEqual(null, game.getRegisteredEnvironment("WestWorld"));
            Assert.AreEqual("Cave", game.getRegisteredEnvironment("Cave").Name);

            // A Player must register with the Game and then after that
            // can enter or leave an Environment.
            IPlayer player = new Player("Matt");
            Assert.AreEqual("Matt", player.Name);
            Assert.AreEqual(true, game.register(player));
            Assert.AreEqual(player.Name, game.getRegisteredPlayer("Matt").Name);
            Assert.AreEqual(game, player.Game);
            Assert.AreEqual(false, game.register(player));

            Assert.AreEqual(null, player.CurrentEnvironment);
            Assert.AreEqual(false, player.leave());
            Assert.AreEqual(false, player.enter(null));
            Assert.AreEqual(null, player.CurrentEnvironment);

            // Now find all the current environment names.
            Assert.AreEqual(1, game.getAvailableEnvironmentsFromDB().Count);
            Assert.AreEqual("Cave", game.getAvailableEnvironmentsFromDB()[0]);

            IEnvironment nomansland = new wadtt.Environment(game, "NoMansLand");
            Assert.AreEqual(true, player.enter(nomansland));

            Assert.AreEqual(true, player.enter(game.getRegisteredEnvironment("Castle")));
            Assert.AreEqual("Castle", player.CurrentEnvironment.Name);

            Assert.AreEqual(true, player.leave());
            Assert.AreEqual(null, player.CurrentEnvironment);
            Assert.AreEqual(false, player.leave());

            List<string> availableEnvs = game.getAvailableEnvironmentsFromDB();
            Assert.AreEqual(1, availableEnvs.Count);

            IEnvironment env = game.getRegisteredEnvironment("Cave");
            List<Creature> creatures = env.getAvailableCreatures();
            foreach (Creature creature in creatures)
            {
                env.register(creature);
            }
            Assert.AreEqual(env.getRegisteredCreatures().Count, env.getRegisteredCreatures().Count);

            Console.WriteLine("+++ Finished GameTest()");
        }

        [TestMethod]
        public void GameTest2()
        {
            Console.WriteLine("+++ Start GameTest2()");
            Resources resources = new Resources();
            IDatabase database = new Database();
            Activity activity = new Activity();
            TextToSpeech.IOnInitListener listener = null;

            IGame game = new Game("Game", database, activity, listener);
            Assert.AreEqual(1, game.getAvailableEnvironmentsFromDB().Count);
            Assert.AreEqual("Cave", game.getAvailableEnvironmentsFromDB()[0]);

            Console.WriteLine("+++ Finished GameTest()");
        }

        [TestMethod]
        public void BaggageHandlerTests()
        {
            Console.WriteLine("+++ Start BaggageHandlerTests()");
            Resources resources = new Resources();
            IDatabase database = new Database();
            Activity activity = new Activity();
            TextToSpeech.IOnInitListener listener = null;

            IGame game = new Game("Baggage Handler Tests", database, activity, listener);
            IEnvironment env = new wadtt.Environment(game, "Cave");
            Assert.AreEqual(false, game.register(env));  // Constructor registered env.

            IPlayer player = new Player("Matt");
            Assert.AreEqual(true, game.register(player));
            Assert.AreEqual(true, player.register(env));
            Assert.AreEqual(true, player.enter(env));

            IBaggageHandler baggage = player.CurrentEnvironment.getBaggageHandler();
            Assert.AreEqual("", Rooms.bagContents(player));
            Assert.AreEqual("", player.bagContents());
            Assert.AreEqual("", Rooms.roomContents(player, 27));

            // Now move an item around ..
            Room room1 = new Room();
            room1.Contents = null;
            room1.ID = 27;
            Assert.AreEqual(0, baggage.transferAllItems(player, room1));
            Assert.AreEqual(0, baggage.transferAllItems(room1, player));

            Assert.AreEqual(false, baggage.transferItem(27, player, room1));
            Assert.AreEqual(false, baggage.transferItem(27, room1, player));

            Console.WriteLine("+++ Finished BaggageHandlerTests()");
        }

        [TestMethod]
        public void CreatureHandlerTests()
        {
            Console.WriteLine("+++ Start CreatureHandlerTests()");
            Resources resources = new Resources();
            IDatabase database = new Database();
            Activity activity = new Activity();
            TextToSpeech.IOnInitListener listener = null;

            IGame game = new Game("Creature Handler Tests", database, activity, listener);
            IEnvironment env = new wadtt.Environment(game, "Cave");
            Assert.AreEqual(false, game.register(env));  // Constructor registered env.

            IPlayer player = new Player("Matt");
            Assert.AreEqual(true, game.register(player));
            Assert.AreEqual(true, player.register(env));
            Assert.AreEqual(true, player.enter(env));

            // FIXME: Really the room details come from our dummy SQL.
            Room room1 = new Room();
            room1.Contents = null;
            room1.ID = 1;
            Assert.AreEqual(true, player.CurrentEnvironment.register(room1));
            Assert.AreEqual(false, player.CurrentEnvironment.register(room1));

            Room room10 = new Room();
            room1.Contents = null;
            room1.ID = 10;
            Assert.AreEqual(true, player.CurrentEnvironment.register(room10));
            Assert.AreEqual(false, player.CurrentEnvironment.register(room10));

            ICreatureHandler creature = player.CurrentEnvironment.getCreatureHandler();
            Assert.AreEqual(0, creature.inRoom(1).Count);
            Assert.AreEqual(false, creature.move(1));
            Assert.AreEqual(false, creature.move());

            Assert.AreEqual(0, creature.getRegisterCount());
            Assert.AreEqual(0, creature.getDeadCount());
            Assert.AreEqual(0, creature.getAsleepCount());
            Assert.AreEqual(0, creature.getAliveCount());
            Assert.AreEqual(0, creature.getAwakeCount());

            Creature awake = new Creature();
            awake.ID = 1;
            awake.Delay = 0;
            awake.Back = 1;
            awake.Right = 1;
            awake.Straight = 1;
            awake.StartRoom = 10;
            awake.Left = 1;
            Assert.AreEqual(true, creature.register(awake));
            Assert.AreEqual(false, creature.register(awake));
            Assert.AreEqual(1, creature.getRegisterCount());
            Assert.AreEqual(0, creature.getDeadCount());
            Assert.AreEqual(0, creature.getAsleepCount());
            Assert.AreEqual(1, creature.getAliveCount());
            Assert.AreEqual(1, creature.getAwakeCount());

            Creature asleep = new Creature();
            asleep.ID = 1;
            asleep.Delay = 2;
            asleep.Back = 1;
            asleep.Right = 1;
            asleep.Straight = 1;
            asleep.StartRoom = 10;
            asleep.Left = 1;
            Assert.AreEqual(false, creature.register(asleep));

            asleep.ID = 2;
            Assert.AreEqual(true, creature.register(asleep));
            // Both creatures are initially in room 10. Though one is asleep.
            Assert.AreEqual(2, creature.inRoom(10).Count);

            Assert.AreEqual(2, creature.getRegisterCount());
            Assert.AreEqual(0, creature.getDeadCount());
            Assert.AreEqual(1, creature.getAsleepCount());
            Assert.AreEqual(2, creature.getAliveCount());
            Assert.AreEqual(1, creature.getAwakeCount());

            // Move sleeping creature twice.
            Assert.AreEqual(false, creature.move(2));
            Assert.AreEqual(false, creature.move(3));
            Assert.AreEqual(false, creature.move(2));
            Assert.AreEqual(2, creature.inRoom(10).Count);

            // Now it is awake it will leave the room..
            Assert.AreEqual(false, creature.move(2));
            Assert.AreEqual(true, creature.move(2));
            Assert.AreEqual(1, creature.inRoom(10).Count);

            // Now move the already awake creature.
            Assert.AreEqual(true, creature.move(1));
            Assert.AreEqual(0, creature.inRoom(10).Count);
            Assert.AreEqual(2, creature.inRoom(1).Count);

            Assert.AreEqual(true, creature.move());
            Assert.AreEqual(0, creature.inRoom(1).Count);
            Assert.AreEqual(2, creature.inRoom(2).Count);

            Assert.AreEqual(true, creature.move());
            Assert.AreEqual(0, creature.inRoom(2).Count);
            Assert.AreEqual(2, creature.inRoom(3).Count);

            Assert.AreEqual(true, creature.move());
            Assert.AreEqual(0, creature.inRoom(3).Count);
            Assert.AreEqual(2, creature.inRoom(27).Count);

            Assert.AreEqual(2, creature.getRegisterCount());
            Assert.AreEqual(0, creature.getDeadCount());
            Assert.AreEqual(0, creature.getAsleepCount());
            Assert.AreEqual(2, creature.getAliveCount());
            Assert.AreEqual(2, creature.getAwakeCount());

            Console.WriteLine("+++ Finished CreatureHandlerTests()");
        }
    }
}