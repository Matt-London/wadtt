using System;
using System.Collections.Generic;

namespace wadtt
{
    public class Items
    {
        private static Dictionary<string, int> m_synonyms = null;

        public static Item GetItemById(int id)
        {
            return new Item();
        }

        public static int GetCount()
        {
            return 0;
        }

        private static char[] m_delimiters = { '|' };

        public enum ItemState { known, unknown, ambiguous };

        public static ItemState getItemId(string words, out int itemId)
        {
            itemId = 1;
            return ItemState.known;
        }

        public static Dictionary<string, int> getSynonyms()
        {
            if (m_synonyms == null)
            {
                getInstance();
            }
            return m_synonyms;
        }

        private static void getInstance()
        {
            m_synonyms = new Dictionary<string, int>();
            for (int itemId = 1; itemId <= GetCount(); ++itemId)
            {
                Item item = GetItemById(itemId);
                if (m_synonyms.ContainsKey(item.Name))
                {
                    // This is ambiguous
                    m_synonyms[item.Name] = 0;
                }
                else
                {
                    m_synonyms.Add(item.Name, itemId);
                }

                // Emample of synonyms: "tin|milk tin|tin of milk"
                string[] synonyms =
                    item.Synonyms.ToLower().Split(m_delimiters, StringSplitOptions.RemoveEmptyEntries);
                foreach (var variant in synonyms)
                {
                    if (m_synonyms.ContainsKey(variant))
                    {
                        // This is ambiguous
                        m_synonyms[variant] = 0;
                    }
                    else
                    {
                        m_synonyms.Add(variant, itemId);
                    }
                }
            }
        }
    }
}