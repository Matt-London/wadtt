using System.Collections.Generic;

namespace wadtt
{
    public class EnvironmentList
    {
        public int ID { get; set; }
        public string Name { get; set; }
        // Effectively defines the scpe of the rooms.
        // Currently rooms are statically dependent .. we don't
        // dynamically create an Environment from a pool of rooms.
        public int RoomStartID { get; set; }
        public int RoomEndID { get; set; }
        public string Creatures { get; set; }
        public string Players { get; set; }
        public string Puzzles { get; set; }
        public string Rules { get; set; }
        public string Weather { get; set; }

        public EnvironmentList()
        {
        }

        public EnvironmentList(string name)
        {
        }

        public EnvironmentList(EnvironmentList environment)
        {
            // Classic! there is no copy constructor in C#!!!
            ID = environment.ID;
            Name = environment.Name;
            RoomStartID = environment.RoomStartID;
            RoomEndID = environment.RoomEndID;
            Creatures = environment.Creatures;
            Players = environment.Players;
            Puzzles = environment.Puzzles;
            Rules = environment.Rules;
            Weather = environment.Weather;
        }

        public static int GetTotalCount()
        {
            // FIXME: Allow for several Creatures at some point soon.
            return 1;
        }

        public List<Creature> getAvailableCreatures()
        {
            List<Creature> creatures = new List<Creature>();
            return creatures;
        }

        public static int internalID = 1;
        public static EnvironmentList GetRow(string envName)
        {
            // For now we say we always find it.
            EnvironmentList dummy = new EnvironmentList();
            dummy.ID = internalID++;
            dummy.Name = envName;
            // FIXME: Allow for several animals and Environments at some point
            dummy.Creatures = "Cat";
            dummy.RoomEndID = 1;
            dummy.RoomStartID = 1;
            return dummy;
        }
    }
}