using Android.Util;
using System.Collections.Generic;

namespace wadtt
{
    public class CreatureList
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Movement { get; set; }

        public CreatureList()
        {
        }

        public CreatureList(string name)
        {
        }

        public CreatureList(CreatureList creature)
        {
            // Classic! there is no copy constructor in C#!!!
            ID = creature.ID;
            Name = creature.Name;
            Movement = creature.Movement;
        }

        public static int GetTotalCount()
        {
            return 1;

        }

        public static List<Creature> GetAvailableCreatures(string envName)
        {
            List<Creature> creatures = new List<Creature>();
            return creatures;
        }

        public static CreatureList GetRow(int id)
        {
            return new CreatureList();
        }

        public static CreatureList GetRow(string name)
        {
            return new CreatureList();
        }
    }
}