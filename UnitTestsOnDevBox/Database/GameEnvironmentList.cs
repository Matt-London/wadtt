using Android.Util;
using System.Collections.Generic;

namespace wadtt
{
    public class GameEnvironmentList
    {
        public int ID { get; set; }
        public int GameListID { get; set; }
        public int EnvironmentListID { get; set; }

        public GameEnvironmentList()
        {
        }

        public GameEnvironmentList(GameEnvironmentList gameEnvironment)
        {
            // Classic! there is no copy constructor in C#!!!
            ID = gameEnvironment.ID;
            GameListID = gameEnvironment.GameListID;
            EnvironmentListID = gameEnvironment.EnvironmentListID;
        }

        public static List<string> GetEnvironmentNames(string gameName)
        {
            List<string> names = new List<string>();
            names.Add("Cave");
            return names;
        }

        public static int GetTotalCount()
        {
            return 1;       // Alter somehow!!

        }

        public static GameEnvironmentList GetRow(int id)
        {
            GameEnvironmentList gl = new GameEnvironmentList();
            gl.ID = id;
            gl.GameListID = 1;
            gl.EnvironmentListID = 1;

            return gl;
        }
    }
}