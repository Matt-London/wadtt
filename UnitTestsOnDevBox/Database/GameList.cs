using System;
using System.Collections.Generic;

namespace wadtt
{
    public class GameList
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public GameList()
        {
        }

        public GameList(string name)
        {
        }

        public GameList(GameList gameEntry)
        {
            // Classic! there is no copy constructor in C#!!!
            ID = gameEntry.ID;
            Name = gameEntry.Name;
        }

        public static int GetTotalCount()
        {
            // FIXME: Allow for several Games at some point.
            return 1;
        }

        public static GameList GetRow(int id)
        {
            GameList dummy = new GameList();
            dummy.ID = id;
            dummy.Name = "Game";
            return dummy;
        }

        public static GameList GetRow(string gameName)
        {
            GameList dummy = new GameList();
            dummy.ID = 1;
            dummy.Name = gameName;
            return dummy;
        }
    }
}