﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using wadtt;

namespace UnitTestsonDevbox
{
    [TestClass]
    public class CommandTests
    {
        public class test
        {
            public string original;
            public string compressed;

            public test(string original, string compressed)
            {
                this.original = original;
                this.compressed = compressed;
            }
        }

        [TestMethod]
        public void LexiconTests()
        {
            // Test  out Lexicon.getWord
            foreach (var lex in Utils.GetValues<Lexicon.lex>())
            {
                var token = new Lexicon.Token(lex);
                string received = Lexicon.getWord(token).ToLower();
                string tokenStringValue = token.lex.ToString().ToLower();
                if (received != tokenStringValue)
                {
                    Console.WriteLine("getWord({0}): Returned {1} Expected: {2}", lex, received, tokenStringValue);
                }

                // Now take each word and test Lexicon.getToken to ensure
                // we recognise each and every lexical value.
                var receivedToken = Lexicon.getToken(tokenStringValue);
                if (receivedToken.lex != lex)
                {
                    Console.WriteLine("getToken({0}): Returned {1} Expected: {2}", tokenStringValue, receivedToken.lex, token.lex);
                }
            }
        }

        [TestMethod]
        public void TokeniserTests()
        {
            // As far as is sensible conert phrases to:
            // a) A single word such as "left"
            // b) Double words such as "take torch"
            test[] tests =
            {
                // 1. Simple Actions - that require no compression.
                new test("ascend", "ascend"),
                new test("baggage", "baggage"),
                new test("descend", "descend"),
                new test("hint", "hint"),
                new test("left", "left"),
                new test("listen", "hear"),
                new test("repeat", "repeat"),
                new test("return", "return"),
                new test("right", "right"),
                new test("score", "score"),
                new test("smell", "smell"),
                new test("straight", "straight"),

                // 2. Questions
                new test("can i hear anything", "hear"),
                new test("can you hear anything","hear"),
                new test("what can i hear", "hear"),
                new test("are there any sounds", "sounds"),
                new test("is there any sound", "sound"),
                new test("can i smell anything", "smell"),
                new test("can you smell anything","smell"),
                new test("what can i smell", "smell"),
                new test("are there any smells", "smells"),
                new test("is there any smell", "smell"),
                new test("is there anything to smell", "smell"),
                new test("climb down", "descend"),
                new test("climb up", "ascend"),
                new test("can i have a hint please", "hint"),
                new test("give me a hint", "hint"),
                new test("what have i scored", "what scored"),
                new test("what is my score", "whats score"),
                new test("what's the score", "whats score"),
                new test("tidal information", "tidal information"),
                new test("where is the tide", "wheres tide"),
                new test("where are the tides", "wheres tides"),
                new test("are any goblins near here", "goblins near"),
                new test("are goblins anywhere about here", "goblins here"),
                new test("where are the goblins", "wheres goblins"),
                new test("what am i carrying", "what carrying"),
                new test("what do i carry",  "what carry"),
                new test("what am i holding",  "what holding"),
                new test("what do i hold", "what hold"),
                new test("what do i have", "what have"),
                new test("what have i got", "what got"),

                new test("what is my route", "describe route"),
                new test("what was my route here", "route here"),
                new test("what was my route to this room", "route room"),
                new test("how did i get here", "route here"),
                new test("how did i get to this room", "route room"),

                // 3. Take items.
                new test("take everything", "take all"),
                new test("pick up everything", "take all"),
                new test("collect everything", "take all"),
                new test("gather everything", "take all"),
                new test("take everything from here", "take all"),
                new test("take everything from room", "take all"),
                new test("pick up everything in this room", "take all"),
                new test("pick up everything that is in this room", "take all"),
                new test("pick up everything that is in here", "take all"),
                new test("pick up everything that is visible", "take all"),
                new test("pick up everything that is present", "take all"),
                new test("pick up everything that is around", "take all"),
                new test("pick up everything that is about here", "take all"),
                new test("pick up everything that is around this cave", "take all"),
                new test("pick up everything that is here", "take all"),

                // 4. Drop items
                new test("discard everything", "drop all"),
                new test("drop everything", "drop all"),
                new test("drop everything i have", "drop all"),
                new test("drop everything i'm carrying", "drop all"),
                new test("drop everything that i carry", "drop all"),
                new test("drop all that i carry", "drop all"),

                // 6. Verbose phrases that need to be reduced
                new test("please could you tell me where the tides are please", "where tides"),
                new test("please would you tell me where are the tides thank you", "where tides"),

                // 7. Vicinity tests..
                new test("are there coins here",                    "coins here"),
                new test("are there any goblins here",              "goblins here"),
                new test("are goblins around",                      "goblins around"),
                new test("are there some goblins in this room",     "goblins here"),
                new test("are there any goblins here",              "goblins here"),
                new test("is there a goblin around",                "goblin around"),
                new test("is there a goblin in this room",          "goblin here"),
                new test("is there a goblin here",                  "goblin here"),
                new test("are there any goblins around here",       "goblins near"),
                new test("are cats near here",                      "cats near"),
                new test("is there a cat around here",              "cat around"),
                new test("is there a cat in this room",             "cat here"),
                new test("is there a cat here",                     "cat here"),
                new test("are goblins close by",                    "goblins near"),
                new test("are there many goblins close by here",    "goblins near"),
                new test("are there any spiders near here",         "spiders near"),
                new test("are goblins close to here",               "goblins near"),
                new test("are goblins close to this room",          "goblins near"),
                new test("are there some coins near here",          "coins near"),
                new test("are goblins nearby",                      "goblins near"),

                // 8. Baggage
                new test("what do i have in my possession",         "what have"),
                new test("what do i have on my possession",         "what have"),
                new test("what do i have on me",                    "what have"),
                new test("what do i have with me",                  "what have"),
                new test("what do i have",                          "what have"),
                new test("what have i got in my possession",        "what got"),
                new test("what have i got on my possession",        "what got"),
                new test("what have i got on me",                   "what got"),
                new test("what have i got with me",                 "what got"),
                new test("what have i got",                         "what got"),
                new test("what do i carry",                         "what carry"),
                new test("what am i carrying around with me",       "baggage"),

                // 9. Specific request..
                new test("where are goblins",          "where goblins"),
                new test("where is a goblin",          "where goblin"),
                new test("where is a cat",  "wheres cat"),
                new test("where is the nearest cat", "where cat"),
                new test("where are the nearest cats", "where cats"),
                new test("are there any cats around", "where cats"),
                new test("are cats around here", "where cats"),

                // 10. Senses
                new test("what are the sounds i can hear", "sounds hear"),
                new test("what can i hear", "hear"),
                new test("what noises can i hear", "hear"),
                new test("what noise is there in this cave", "hear"),
                new test("can i hear any noise", "hear"),
                new test("can i hear any noises", "hear"),
                new test("can i hear anything", "hear"),
                new test("can you hear anything", "hear"),
                new test("can i hear anything in the distance", "hear"),
                new test("can i hear anything in the tunnels", "hear"),
                new test("can i hear anything in the distant caves", "hear"),
                new test("can i smell anything", "smell"),
                new test("what can i smell", "smell"),
                new test("are there any smells", "smells"),
                new test("what can you smell", "smell"),
                new test("does it smell in here", "smell"),
                new test("does it smell in this place", "smell"),
                new test("does it smell in this room", "smell"),
                new test("does it smell in this cave", "smell"),
                new test("does it smell around here", "smell"),
                new test("what does it smell of around here", "smell"),

                // 11. Movement .. always reduce to a single word
                new test("go back",                         "return"),
                new test("go back to last cave",            "return"),
                new test("please head back to my last room",     "return"),
                new test("please head back to my last cavern",   "return"),
                new test("please head back to my last place",    "return"),
                new test("please head back to my last location", "return"),
                new test("please head back to my last cave",     "return"),
                new test("run back to last cave",           "return"),
                new test("return to previous room",         "return"),
                new test("retreat to the previous room",    "return"),
                new test("return to previous cave",         "return"),
                new test("previous room please",            "return"),
                new test("head straight up",                "ascend"),
                new test("head straight down",              "descend"),
                new test("head straight on",                "straight"),
                new test("head straight onto next cave",    "straight"),
                new test("head straight into next room",    "straight"),
                new test("walk straight up",                "ascend"),
                new test("walk straight down",              "descend"),
                new test("carry straight on",               "straight"),
                new test("carry straight up",               "ascend"),
                new test("carry straight down",             "descend"),
                new test("walk through the left door",      "left"),
                new test("walk into the right passage",     "right"),
                new test("head into the right tunnel",      "right"),
                new test("pass into the left opening",      "left"),
                new test("enter the left room",             "left"),
                new test("enter into the left opening",     "left"),
                new test("descend to the left",             "left"),
                new test("go up to the right",              "right"),

                // Take
                new test("pick up everything from here",     "take all"),
                new test("pick up everything from around here",   "take all"),
                new test("pick up everything here",          "take all"),
                new test("pick up everything",               "take all"),
                new test("collect everything",               "take all"),
                new test("take everything",               "take all"),

                // Humour
                new test("what do i own",                         "what own"),
                new test("do i own anything",                     "what do i own"),

                // Interaction
                new test("please could you pick up the knife thank you", "take knife"),
                new test("fit the key into the lock", "fit key"),
                new test("use the key", "use key"),
                new test("slot the key into the lock", "fit key"),
                new test("drink some water from the water bottle", "fit key"),
                new test("slot the key into the lock", "fit key"),
                new test("fill the bottle from the stream", "fill bottle"),
                new test("fill up the water bottle with stream water", "fit key"),
                new test("feed the cat", "feed cat"),

                new test("hide in the closet", "hide in closet"),
                new test("hide under the bed", "hide under bed"),
                new test("get in cupboard", "in cupboard"),
                new test("get into boat", "in boat"),
                new test("climb into boat", "in boat"),

                // Unknown interactions.
                new test("discharge the rifle", "fire rifle"),
                new test("paint the room", "paint room"),
                new test("I think I will have a sleep on the bed", "sleep on bed"),
                new test("open the door", "open door"),

                new test("go forward", "forward"),
                new test("okay go forward", "forward"),
                new test("okay go straight on", "straight"),
                new test("carry straight forward", "forward"),

                // 1. These are interactions to do with eyes .. looking, reading and seeing..
                new test("what do I see", "look"),
                new test("What can I see", "look"),
                new test("Look what's on the table", "look on table"),
                new test("Collect whats on the table", "take on table"),
                new test("Take whats under the table", "take under table"),
                new test("Get what's under the water", "get under water"),
                new test("Go under the water", "go under water"),
                new test("Retrieve what is in the water", "get whats in water"),
                new test("Get what's on the water", "get whats on water"),
                new test("Get under the table", "get under table"),
                new test("Is there anything to see around here", "look"),
                new test("Is there anything to see in here", "look in here"),
                new test("Look on the floor", "look on floor"),
                new test("Look under the table", "look under table"),
                new test("Is there anything to see on the walls", "look walls"),
                new test("Look in the mirror", "look mirror"),
                new test("Look behind the mirror", "look behind mirror"),
                new test("Move the mirror", "move mirror"),
                new test("Look at the windows", "look windows"),
                new test("what is on the floor", "whats on floor"),
                new test("is there anything on the floor", "look floor"),
                new test("what is in this room", "whats here"),
                new test("what is in here", "whats here"),
                new test("what can i see around me", "look around"),
                new test("describe this room", "describe room"),
                new test("describe the table", "describe table"),
                new test("describe this place", "describe place"),
                new test("What did you just say", "repeat"),
                new test("Repeat what you just said", "repeat"),

                new test("please look around the room", "look"),
                new test("look around room please", "look around room"),
                new test("examine the room", "examine room"),
                new test("look around here", "look around here"),
                new test("look under the bed", "look under bed"),
                new test("examine room carefully", "examine room"),

                new test("look up", "look up"),
                new test("look up at the ceiling", "look at ceiling"),
                new test("look up at the roof", "look at roof"),

                new test("what is in this cave", "look"),
                new test("describe what can I see on the ground", "look on ground"),
                new test("what is on the table", "whats on table"),
                new test("what is under the table", "whats under table"),
                new test("see what is under the table", "whats under table"),
                new test("see if there is anything under the table", "whats under table"),
                new test("please could you tell me where i am", "where i"),

                // Read is subtly different from look
                new test("read the walls", "read walls"),
                new test("read anything on the walls", "read walls"),
                new test("read any writing on the wall", "read walls"),
                new test("read what is written on the walls", "read walls"),
                new test("read the the words written on the wall", "read walls"),
                new test("read what is written on the walls", "read walls"),

                // These are really questions tha demand a negative or affirmative type answer.
                new test("can you see anything near here", "look near"),
                new test("can you see anything nearby", "look near"),
                new test("can you see anyone moving around", "look moving")
            };

            int failCount = 0;
            foreach (var t in tests)
            {
                Tokeniser tokeniser = new Tokeniser(t.original);
                var tokens = tokeniser.getTokens();

                var compressedWords = Tokeniser.getString(tokens);
                if (compressedWords != t.compressed)
                {
                    Console.WriteLine("{0} Compress==> {1} Expected: {2}", t.original, compressedWords, t.compressed);
                    failCount++;
                }
            }
            Console.WriteLine("+++ {0} tests out of {1} failed +++", failCount, tests.Length);
        }

        static void Main()
        {
            Console.WriteLine("wadtt Unit Tests");
            CommandTests c = new CommandTests();
            c.TokeniserTests();
            c.LexiconTests();

            GameTests g = new GameTests();
            g.GameTest();
            g.GameTest2();
            g.BaggageHandlerTests();
            g.CreatureHandlerTests();
        }
    }
}
