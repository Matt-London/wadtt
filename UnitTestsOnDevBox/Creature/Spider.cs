//+++++++++++++++++++++++++++++++++
// DUMMY VERSION FOR UNIT TESTING +
//+++++++++++++++++++++++++++++++++
using Android.Util;
using System.Collections.Generic;

namespace wadtt
{
    public class Spider : Creature
    {
        public Dimensions Size { get; set; }
        public int Count { get; set; }

        public enum Dimensions { baby, small, fullygrown, big, verylarge, enormous };

        public Spider()
        {
        }

        public Spider(Spider spider)
        {
            // Classic! there is no copy constructor in C#!!!
            ID = spider.ID;
            Size = spider.Size;
            Count = spider.Count;
            StartRoom = spider.StartRoom;
            Left = spider.Left;
            Right = spider.Right;
            Straight = spider.Straight;
            Back = spider.Back;

        }

        public string dimensionsToText()
        {
            switch (Size)
            {
                case Dimensions.baby:
                    return "baby";

                case Dimensions.small:
                    return "small";

                case Dimensions.fullygrown:
                    return "fully grown";

                case Dimensions.big:
                    return "big";

                case Dimensions.verylarge:
                    return "very large";

                case Dimensions.enormous:
                    return "enormous";

                default:
                    return "unknown sized";
            }
        }

        public static int GetTotalCount()
        {
            return 0;
        }

        public static Spider GetSpider(int id)
        {
            Spider spider = new Spider();
            return spider;
        }
    }
}