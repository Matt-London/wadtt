//+++++++++++++++++++++++++++++++++
// DUMMY VERSION FOR UNIT TESTING +
//+++++++++++++++++++++++++++++++++
namespace wadtt
{
    public class Cat : Creature
    {
        public string Desc { get; set; }
        public int Count { get; set; }

        public Cat()
        {
        }

        public Cat(Cat cat)
        {
            // Classic! there is no copy constructor in C#!!!
            ID = cat.ID;
            Desc = cat.Desc;
            Count = cat.Count;
            StartRoom = cat.StartRoom;
            Left = cat.Left;
            Right = cat.Right;
            Straight = cat.Straight;
            Back = cat.Back;          
        }

        public static int GetTotalCount()
        {
            return 0;       // Modify at test setup.
        }

        public static Cat GetCat(int id)
        {
            Cat cat = new Cat();
            return cat;
        }
    }
}