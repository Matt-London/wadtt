//+++++++++++++++++++++++++++++++++
// DUMMY VERSION FOR UNIT TESTING +
//+++++++++++++++++++++++++++++++++
using Android.Util;
using System.Collections.Generic;

namespace wadtt
{
    public class Goblin : Creature
    {
        public int Count { get; set; }
        public string Baggage { get; set; }

        public Goblin()
        {
            // Used by SQLite
        }

        public Goblin(Goblin goblin)
        {
            // Classic! there is no copy constructor in C#!!!
            ID = goblin.ID;
            Count = goblin.Count;
            Left = goblin.Left;
            Right = goblin.Right;
            Straight = goblin.Straight;
            Back = goblin.Back;
            Baggage = goblin.Baggage;
        }

        public static int GetTotalCount()
        {
            return 0;
        }

        public static Goblin GetGoblin(int id)
        {
            Goblin goblin = new Goblin();
            return goblin;
        }
    }
}