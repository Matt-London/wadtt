sqlite>.open game.db
sqlite>.read game.txt
sqlite> SELECT Count(*) FROM Creature;
sqlite> SELECT * FROM Creature;
sqlite>.dump

========================================================================
SELECT EnvironmentList.Name as EnvName FROM GameList
INNER JOIN EnvironmentList ON
  GameEnvironmentList.EnvironmentListID = EnvironmentList.ID
INNER JOIN GameEnvironmentList ON
  GameEnvironmentList.GameListID = GameList.ID
WHERE GameList.Name = "Adventure";
========================================================================
SELECT
  CreatureList.Name as Name,
  EnvironmentCreatureList.Left as Left,
  EnvironmentCreatureList.Right as Right,
  EnvironmentCreatureList.Straight as Straight,
  EnvironmentCreatureList.Back as Back,
  EnvironmentCreatureList.StartRoom as StartRoom,
  EnvironmentCreatureList.Delay as Delay
FROM EnvironmentCreatureList
INNER JOIN EnvironmentList ON
  EnvironmentCreatureList.EnvironmentListID = EnvironmentList.ID
INNER JOIN CreatureList ON
  EnvironmentCreatureList.CreatureListID = CreatureList.ID
WHERE EnvironmentList.Name = "Cave";
