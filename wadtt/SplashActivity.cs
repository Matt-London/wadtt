﻿using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Webkit;
using Android.Views;
using Android.Speech.Tts;

namespace wadtt
{
    [Activity(Label = "wadtt", MainLauncher = true, NoHistory = true)]
    public class SplashActivity : Activity
    {
        static readonly string TAG = "wadtt Splash";

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Log.Debug(TAG, "+++ Splash.OnCreate");

            SetContentView(Resource.Layout.Splash);
            // Get our button from the layout resource,
            // and attach an event to it

            var wv = FindViewById<WebView>(Resource.Id.mywebview);

            wv.LoadUrl("file:///android_asset/splash.html");

            wv.SetWebViewClient(new MonkeyWebViewClient());

            wv.Settings.LoadWithOverviewMode = false;
            wv.Settings.UseWideViewPort = false;

            // scrollbar stuff
            wv.ScrollBarStyle = ScrollbarStyles.OutsideOverlay; // so there's no 'white line'
            wv.ScrollbarFadingEnabled = false;
        }

        class MonkeyWebViewClient : WebViewClient
        {
            public override bool ShouldOverrideUrlLoading(WebView view, string url)
            {
                view.LoadUrl(url);
                return true;
            }
        }

        protected override void OnResume()
        {
            Log.Debug(TAG, "+++ Splash.OnResume");
            base.OnResume();

            Task startupWork = new Task(async () =>
            {
                Log.Debug(TAG, "Start delay");
                await Task.Delay(2000); // Simulate a bit of startup work.
                Log.Debug(TAG, "Completed delay");
                StartActivity(new Intent(Application.Context, typeof(MainActivity)));
            });

            startupWork.ContinueWith(t =>
            {
                Log.Debug(TAG, "Work is finished - start MainActivity");
                //StartActivity(new Intent(Application.Context, typeof(MainActivity)));
            }, TaskScheduler.FromCurrentSynchronizationContext());

            startupWork.Start();
        }

        protected override void OnStart()
        {
            Log.Debug(TAG, "+++ Splash.OnStart");
            base.OnStart();
        }

        protected override void OnPause()
        {
            Log.Debug(TAG, "+++ Splash.OnPause");
            base.OnStop();
            //game.getSound().Stop();
        }

        protected override void OnStop()
        {
            Log.Debug(TAG, "+++ Splash.OnStop");
            base.OnStop();
        }
    }
}