using System;
using System.Collections.Generic;
using Android.Content;
using Android.Util;
using SQLite;
using System.IO;
using Android.Content.Res;
using System.Resources;

// For now I just go synchronous on SQLite. Worry about Async another time.
// http://stackoverflow.com/questions/2493331/what-are-the-best-practices-for-sqlite-on-android
// ://msdn.microsoft.com/en-us/library/mt674882.aspx
namespace AlgebraOne
{
    public class ItemNotes
    {
        SQLiteConnection db = null;
        private static Resources resources = null;
        private static ItemNotes instance;

        // We need to acquire resources from the Activity ContextWrapper.
        public static ItemNotes getInstance(Resources res)
        {
            if (instance == null)
            {
                if (resources == null)
                {
                    resources = res;
                }
                instance = new ItemNotes();
            }
            return instance;
        }

        private ItemNotes()
        {
            string roomsFile = "items.db";
            try
            {
                string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                string path = System.IO.Path.Combine(folder, roomsFile);

                // We read from the .apk resources into our personal storage and then open.
                // Shame we can't simply stream into sqlite!!
                // FIXME: Handle updates!!
                if (!System.IO.File.Exists(path))
                {
                    var s = resources.OpenRawResource(Resource.Raw.rooms);
                    FileStream writeStream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
                    ReadWriteStream(s, writeStream);
                }
                db = new SQLiteConnection(path);
            }
            catch (SQLiteException ex)
            {
                Log.Error(MainActivity.TAG, ex.Message);
            }
        }

        // readStream is the stream you need to read
        // writeStream is the stream you want to write to
        private void ReadWriteStream(Stream readStream, Stream writeStream)
        {
            int Length = 256;
            Byte[] buffer = new Byte[Length];
            int bytesRead = readStream.Read(buffer, 0, Length);
            // write the required bytes
            while (bytesRead > 0)
            {
                writeStream.Write(buffer, 0, bytesRead);
                bytesRead = readStream.Read(buffer, 0, Length);
            }
            readStream.Close();
            writeStream.Close();
        }

        
    }
}