namespace wadtt
{
    public class Hint
    {
        private static string[] easyHints =
        {
            "The cave is tidal.",
            "A bag is always useful",
            "Beware the tides.",
            "Not all goblins are the same.",
            "The sirens sound for a reason.",
            "If you are sensible you will use all of your senses.",
            "It's not always bad when the lights go out.",
            "You have ears, so use them.",
            "You have a nose for danger.",
            "The odd please and thank you can go a long way."
        };

        private static string[] questions =
        {
            "do i recognise this room",
            "have i been here before"
        };

        public static string getEasyHint()
        {
            return Utils.getAny(easyHints);
        }


    }
}