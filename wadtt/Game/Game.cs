using Android.App;
using Android.OS;
using Android.Speech.Tts;
using Android.Util;
using System;
using System.Collections.Generic;
using System.Linq;

// Everything happens inside a game
// A game has:
// 1. A User Interface.
//
// And the following can register with a game:
// 
// Game.register(IPlayer)

// Game.listEnvironments

// Notes
// 1. We must do all we can to avoid any concrete dependencies within our base,
//    Otherwise to replace or update these dependencies, we will need to change
//    our classes' source code and recompile the solution.
// 2. The concrete implementation of any dependencies must be available at
//    compile time, rather than being suppliable through dynamic data injection.
// 3. Unit testing becomes a lot harder.
//
namespace wadtt
{
    // A Game consist of Environments, Players and some handlers such as
    // A BaggageHandler, A CreatureHandler.
    public class Game : IGame
    {
        private string TAG = "Game";

        public string Name { get; set; }
        private TextToSpeech m_textToSpeech = null;
        private Activity m_activity;

        private ISounds m_sounds;

        private Dictionary<string, IEnvironment> m_Environments = null;
        private Dictionary<string, IPlayer> m_Players = null;

        public Game(string name, IDatabase database, Activity activity,
                    TextToSpeech.IOnInitListener listener)
        {
            Name = name;
            m_activity = activity;

            // Taken from: https://github.com/xamarin/monodroid-samples/blob/master/PlatformFeatures/TextToSpeech/README.md
            m_textToSpeech = new TextToSpeech(activity, listener);

            m_sounds = new Sounds();
            m_Environments = new Dictionary<string, IEnvironment>();
            m_Players = new Dictionary<string, IPlayer>();

            // FIXME: Currently we set up every Environment completely
            //        whether or not we actually use it.
            //        Rather than when we enter an environment.
            List<string> environmentNames = getAvailableEnvironmentsFromDB();
            foreach (string envName in environmentNames)
            {
                IEnvironment env = new wadtt.Environment(this, envName);
            }
        }

        public TextToSpeech getTextToSpeech()
        {
            return m_textToSpeech;
        }

        public Activity getActivity()
        {
            return m_activity;
        }

        public ISounds getSound()
        {
            return m_sounds;
        }

        public void finish()
        {
            Log.Debug(MainActivity.TAG, "The Game has finished");
            m_activity.FinishAffinity();
            Process.KillProcess(Process.MyPid());
        }

        //---------------------------------------------------------------------------
        public IEnvironment getRegisteredEnvironment(string name)
        {
            if (!m_Environments.ContainsKey(name))
            {
                return null;
            }

            return m_Environments[name];
        }

        public List<string> getRegisteredEnvironments()
        {
            List<string> list = new List<string>();
            foreach (string key in m_Environments.Keys)
            {
                list.Add(key);
            }

            return list;
        }

        public List<string> getAvailableEnvironmentsFromDB()
        {
            return GameEnvironmentList.GetEnvironmentNames(Name);
        }

        public EnvironmentList getAvailableEnvironmentFromDB(string name)
        {
            return EnvironmentList.GetRow(name);
        }

        public bool register(IEnvironment env)
        {
            if (env == null)
            {
                return false;
            }

            // We cannot re-register the same environment, certainly not here.
            if (m_Environments.ContainsKey(env.Name))
            {
                return false;
            }

            bool registered = false;
            try
            {
                m_Environments[env.Name] = env;

                // We set up the Environment for anyone who wishes to register and enter.
                int roomStartId = env.getStartRoomId();
                int roomEndId = env.getEndRoomId();
                for (int roomId = roomStartId; roomId <= roomEndId; ++roomId)
                {
                    env.register(Room.GetRoomById(roomId));
                }

                List<Creature> creatures = env.getAvailableCreatures();
                foreach (Creature creature in creatures)
                {
                    env.register(creature);
                }
                registered = true;
            }
            catch (Exception ex)
            {
                Log.Error(TAG, "Environment: " + env.Name + " register failed: " + ex.Message);
            }
            return registered;
        }

        public IPlayer getRegisteredPlayer(string name)
        {
            if (!m_Players.ContainsKey(name))
            {
                return null;
            }

            return m_Players[name];
        }

        public List<string> getRegisteredPlayers()
        {
            return m_Players.Keys.ToList();
        }

        public bool register(IPlayer player)
        {
            if (player == null)
            {
                return false;
            }

            // We cannot re-register the same player, certainly not here.
            if (m_Players.ContainsKey(player.Name))
            {
                return false;
            }

            bool registered = false;
            try
            {
                m_Players[player.Name] = player;
                player.register(this);
                registered = true;
            }
            catch (Exception ex)
            {
                Log.Error(TAG, "Player: " + player.Name + " register failed: " + ex.Message);
            }
            return registered;
        }
    }
}