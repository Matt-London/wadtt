using System.Collections.Generic;

namespace wadtt
{
    public interface ICreatureHandler
    {
        bool register(Creature creature);
        bool move(int creatureId = 0);

        List<Creature> inRoom(int roomId);

        int getRegisterCount();
        int getAliveCount();
        int getDeadCount();
        int getAsleepCount();
        int getAwakeCount();

        // locate(int depth);
        // Locate creatures near here to a depth of 3 rooms
        // return List of creatures and depth. (0 == in this room).

        // Can also search based on live creatures & creature types.
        // May need something similar for lokking for items based on attributes.
    }
}