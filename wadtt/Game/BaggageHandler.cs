using System;
using Android.Util;
using System.Collections.Generic;

namespace wadtt
{
    public class BaggageHandler : IBaggageHandler
    {
        private string TAG = "BaggageHandler";

        // This is purely used as a sanity check to avoid recording duplicate rooms.
        private List<IRoom> m_RoomsSanityCheck = null;

        // A BaggageHandler is specific to one environment.
        private IEnvironment m_env = null;

        //The item descriptions don't change except, only their ownership.
        // Except they can get used up or destroyed.
        private enum Ownership { Player = 0, Creature = 1, Room = 2, Destroyed = 3};
        class ItemOwner
        {
            public int itemId;
            public Ownership ownership;
            public object owner;

            public ItemOwner(int itemId, Ownership ownership, object owner)
            {
                this.itemId = itemId;
                this.ownership = ownership;
                this.owner = owner;
            }
        };

        // Every item has an owner.
        // There could be seven matches all with the same itemId but different owners.
        // Some could be in rooms, so with goblins or players, some destroyed.
        // FIXME:  A count of items owned by each Owner type might help.
        //         Maximum items is about 50 at the moment I guess.
        //         Could have separate lists for each owner type?
        private List<ItemOwner> m_Owners = null;

        public BaggageHandler(IEnvironment env)
        {
            m_RoomsSanityCheck = new List<IRoom>();
            m_Owners = new List<ItemOwner>();

            // Extract all the items from this Environment and track them.
            // This means Items in Rooms and Items currently "owned" by Creatures.
            // Items "owned" by Players are only added when the Player registers
            // and are removed when the Player unregisters.
            m_env = env;
        }

        public bool register(IRoom room)
        {
            if (room == null)
            {
                return false;
            }

            if (m_RoomsSanityCheck.Contains(room))
            {
                Log.Error(TAG, "Room id: " + room.ID + " already registered");
                return false;
            }

            bool registered = false;
            try
            {
                m_RoomsSanityCheck.Add(room);

                string contents = room.getRawContents();
                if (contents != null && contents.Length != 0)
                {
                    string[] tokens = contents.Split(',');
                    int[] roomItems = Array.ConvertAll<string, int>(tokens, int.Parse);
                    foreach (int itemId in roomItems)
                    {
                        ItemOwner item = new ItemOwner(itemId, Ownership.Room, room);
                        m_Owners.Add(item);
                    }
                }
                Log.Debug(MainActivity.TAG, "{0} items in room {1}", getManifest(room).Count, room.ID);
                registered = true;
            }
            catch (Exception ex)
            {
                Log.Error(TAG, "Room " + room.ID + " register failed: " + ex.Message);
            }
            return registered;
        }

        public List<string> getManifest(ICreature creature)
        {
            List<string> manifest = new List<string>();
            foreach (ItemOwner itemOwner in m_Owners)
            {
                if (itemOwner.ownership == Ownership.Creature)
                {
                    ICreature creatureOwns = (ICreature)itemOwner.owner;
                    if (creatureOwns.getID() == creature.getID())
                    {
                        // The creatures match. So we are interested in this item.
                        Item item = Items.GetItemById(itemOwner.itemId);
                        string desc = item.Name;
                        manifest.Add(desc);
                    }
                }
            }
            return manifest;
        }

        public List<string> getManifest(IPlayer player)
        {
            List<string> manifest = new List<string>();
            foreach (ItemOwner itemOwner in m_Owners)
            {
                if (itemOwner.ownership == Ownership.Player)
                {
                    IPlayer playerOwns = (IPlayer)itemOwner.owner;
                    if (playerOwns.Name.Equals(player.Name))
                    {
                        // The players match. So we are interested in this item.
                        Item item = Items.GetItemById(itemOwner.itemId);
                        string desc = item.Name;
                        manifest.Add(desc);
                    }
                }
            }
            return manifest;
        }

        public List<string> getManifest(IRoom room)
        {
            List<string> manifest = new List<string>();
            foreach (ItemOwner itemOwner in m_Owners)
            {
                if (itemOwner.ownership == Ownership.Room)
                {
                    IRoom roomOwns = (IRoom)itemOwner.owner;
                    if (roomOwns.ID == room.ID)
                    {
                        // The rooms match. So we are interested in this item.
                        Item item = Items.GetItemById(itemOwner.itemId);
                        string desc = item.Name;
                        manifest.Add(desc);
                    }
                }
            }
            return manifest;
        }

        public bool transferItem(int itemId, IRoom fromRoom, IPlayer toPlayer)
        {
            bool found = false;
            // We can't use foreach because we are modifying contents.
            for (int i = 0; i < m_Owners.Count; i++)
            {
                ItemOwner itemOwner = m_Owners[i];
                if (itemOwner.ownership == Ownership.Room)
                {
                    // The rooms match. So we are interested in this item.
                    IRoom roomOwns = (IRoom)itemOwner.owner;
                    if (roomOwns.ID == fromRoom.ID)
                    {
                        if (itemOwner.itemId == itemId)
                        {
                            m_Owners[i] = new ItemOwner(itemId, Ownership.Player, toPlayer);
                            found = true;
                        }
                    }
                }
            }

            return found;
        }

        public bool transferItem(int itemId, IPlayer fromPlayer, IRoom toRoom)
        {
            bool found = false;
            // We can't use foreach because we are modifying contents.
            for (int i = 0; i < m_Owners.Count; i++)
            {
                ItemOwner itemOwner = m_Owners[i];
                if (itemOwner.ownership == Ownership.Player)
                {
                    IPlayer playerOwns = (IPlayer)itemOwner.owner;
                    if (playerOwns.Name.Equals(fromPlayer.Name))
                    {
                        if (itemOwner.itemId == itemId)
                        {
                            m_Owners[i] = new ItemOwner(itemId, Ownership.Room, toRoom);
                            found = true;
                        }
                    }
                }
            }

            return found;
        }

        public bool transferItem(int itemId, IRoom fromRoom, ICreature toCreature)
        {
            bool found = false;
            // We can't use foreach because we are modifying contents.
            for (int i = 0; i < m_Owners.Count; i++)
            {
                ItemOwner itemOwner = m_Owners[i];
                if (itemOwner.ownership == Ownership.Room)
                {
                    // The rooms match. So we are interested in this item.
                    IRoom roomOwns = (IRoom)itemOwner.owner;
                    if (roomOwns.ID == fromRoom.ID)
                    {
                        if (itemOwner.itemId == itemId)
                        {
                            m_Owners[i] = new ItemOwner(itemId, Ownership.Creature, toCreature);
                            found = true;
                        }
                    }
                }
            }

            return found;
        }

        public bool transferItem(int itemId, ICreature fromCreature, IRoom toRoom)
        {
            bool found = false;
            // We can't use foreach because we are modifying contents.
            for (int i = 0; i < m_Owners.Count; i++)
            {
                ItemOwner itemOwner = m_Owners[i];
                if (itemOwner.ownership == Ownership.Creature)
                {
                    ICreature creatureOwns = (ICreature)itemOwner.owner;
                    if (creatureOwns.getID() == fromCreature.getID())
                    {
                        if (itemOwner.itemId == itemId)
                        {
                            m_Owners[i] = new ItemOwner(itemId, Ownership.Room, toRoom);
                            found = true;
                        }
                    }
                }
            }

            return found;
        }

        public int transferAllItems(IRoom fromRoom, IPlayer toPlayer)
        {
            int count = 0;
            // We can't use foreach because we are modifying contents.
            for (int i = 0; i < m_Owners.Count; i++)
            {
                ItemOwner itemOwner = m_Owners[i];
                if (itemOwner.ownership == Ownership.Room)
                {
                    IRoom roomOwns = (IRoom)itemOwner.owner;
                    if (roomOwns.ID == fromRoom.ID)
                    {
                        // The rooms match. So we are interested in this item.
                        int itemId = m_Owners[i].itemId;
                        m_Owners[i] = new ItemOwner(itemId, Ownership.Player, toPlayer);
                        count++;
                    }
                }
            }

            return count;
        }

        public int transferAllItems(IPlayer fromPlayer, IRoom toRoom)
        {
            int count = 0;
            // We can't use foreach because we are modifying contents.
            for (int i = 0; i < m_Owners.Count; i++)
            {
                ItemOwner itemOwner = m_Owners[i];
                if (itemOwner.ownership == Ownership.Player)
                {
                    IPlayer playerOwns = (IPlayer)itemOwner.owner;
                    if (playerOwns.Name.Equals(fromPlayer.Name))
                    {
                        // Player details match.
                        int itemId = m_Owners[i].itemId;
                        m_Owners[i] = new ItemOwner(itemId, Ownership.Room, toRoom);
                        count++;
                    }
                }
            }

            return count;
        }
    }
}