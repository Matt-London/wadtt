using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wadtt
{
    // This interface should be language agnostic.
    public interface IBaggageHandler
    {
        bool register(IRoom room);

        List<string> getManifest(ICreature creature);
        List<string> getManifest(IPlayer player);
        List<string> getManifest(IRoom room);

        // This is the action of items being picked up and discarded or stolen by creatures.
        bool transferItem(int itemId, IRoom fromRoom, IPlayer toPlayer);
        bool transferItem(int itemId, IPlayer fromPlayer, IRoom toRoom);
        bool transferItem(int itemId, IRoom fromRoom, ICreature toCreature);
        bool transferItem(int itemId, ICreature fromCreature, IRoom toRoom);

        int transferAllItems(IRoom fromRoom, IPlayer toPlayer);
        int transferAllItems(IPlayer fromPlayer, IRoom toRoom);
    }
}