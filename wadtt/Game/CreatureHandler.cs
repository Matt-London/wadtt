using System;
using System.Collections.Generic;
using Android.Util;

namespace wadtt
{
    // Extract all the creatures from this Environment and track them.
    // We are responsible for:
    // 1. The movements of creatures (When they wake, when they move room).
    // 2. How they react and interact with Players and each other.
    // 3. Whether they steal item, destroy items, drop items, pickup items.
    // 4. The sounds and smells they create.

    public class CreatureHandler : ICreatureHandler
    {
        private string TAG = "CreatureHandler";

        // A CreatureHandler is specific to one environment.
        private IEnvironment m_env = null;

        public enum CreatureActivity { Asleep = 0, Awake = 1, Dead = 2};
        class CreatureState
        {
            public Creature state;
            public int creatureId;
            public CreatureActivity activity { get; set; }
            // All our Baggage is handled by the BaggageHandler.

            public CreatureState(Creature initialState)
            {
                this.state = initialState;
                this.creatureId = initialState.getID();
                this.activity = (state.Delay > 0) ? CreatureActivity.Asleep : CreatureActivity.Awake;
            }
        };

        private List<CreatureState> m_CreatureStates = null;

        public CreatureHandler(IEnvironment env)
        {
            m_env = env;
            m_CreatureStates = new List<CreatureState>();
        }

        private bool getStateIndex(int creatureId, out int index)
        {
            bool found = false;
            index = 0;

            for (int i = 0; i < m_CreatureStates.Count; i++)
            {
                CreatureState state = m_CreatureStates[i];
                if (state.creatureId == creatureId)
                {
                    index = i;
                    found = true;
                }
            }

            return found;
        }

        public int getRegisterCount()
        {
            return m_CreatureStates.Count;
        }

        private int getCount(CreatureActivity activity)
        {
            int total = 0;
            foreach (CreatureState state in m_CreatureStates)
            {
                if (state.activity == activity)
                {
                    total++;
                }
            }

            return total;
        }

        public int getAliveCount()
        {
            return getRegisterCount() - getDeadCount();
        }

        public int getDeadCount()
        {
            return getCount(CreatureActivity.Dead);
        }

        public int getAsleepCount()
        {
            return getCount(CreatureActivity.Asleep);
        }

        public int getAwakeCount()
        {
            return getCount(CreatureActivity.Awake);
        }

        public bool register(Creature creature)
        {
            if (creature == null)
            {
                return false;
            }

            int index = 0;
            if (getStateIndex(creature.getID(), out index) == true)
            {
                Log.Error(TAG, "Creature: " + creature.getID() + " already registered");
                return false;
            }

            bool registered = false;
            try
            {
                CreatureState state = new CreatureState(creature);
                m_CreatureStates.Add(state);
                registered = true;
            }
            catch (Exception ex)
            {
                Log.Error(TAG, "Creature: " + creature.getID() + " register failed: " + ex.Message);
            }
            return registered;
        }

        public bool move(int creatureId)
        {
            bool movement = false;
            // We can't use foreach because we are modifying contents.
            for (int i = 0; i < m_CreatureStates.Count; i++)
            {
                CreatureState creature = m_CreatureStates[i];
                if ((creatureId == 0) || (creatureId == creature.creatureId))
                {
                    switch (creature.activity)
                    {
                        case CreatureActivity.Asleep:
                            if (creature.state.Delay > 0)
                            {
                                m_CreatureStates[i].state.Delay--;
                                Log.Debug(TAG, "Creature: " + creature.creatureId + " remains asleep: " + creature.state.Delay);
                            }
                            else
                            {
                                m_CreatureStates[i].activity = CreatureActivity.Awake;
                                Log.Debug(TAG, "Creature: " + creature.creatureId + " awakens");
                            }
                            break;

                        case CreatureActivity.Awake:
                            int old = creature.state.getCurrentRoom();
                            int newRoom = m_CreatureStates[i].state.getNextRoom();
                            Log.Debug(TAG, "Creature: " + creature.creatureId + " moves: " + old + " -> " + newRoom);
                            movement = true;
                            break;

                        case CreatureActivity.Dead:
                            Log.Debug(TAG, "Creature: " + creature.creatureId + " is dead");
                            continue;
                    }
                }
            }

            return movement;
        }

        public List<Creature> inRoom(int roomId)
        {
            List<Creature> manifest = new List<Creature>();
            for (int i = 0; i < m_CreatureStates.Count; i++)
            {
                CreatureState creature = m_CreatureStates[i];
                if (creature.state.getCurrentRoom() == roomId)
                {
                    manifest.Add(creature.state);
                }
            }

                return manifest;
        }
    }
}