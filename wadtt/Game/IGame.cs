using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wadtt
{
    public interface IGame
    {
        // Available is not the same as registered.
        List<string>    getAvailableEnvironmentsFromDB();
        EnvironmentList getAvailableEnvironmentFromDB(string name);

        IEnvironment getRegisteredEnvironment(string name);
        List<string> getRegisteredEnvironments();

        IPlayer      getRegisteredPlayer(string name);
        List<string> getRegisteredPlayers();

        bool register(IEnvironment environment);
        bool register(IPlayer player);
    }
}