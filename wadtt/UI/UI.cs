using Android.App;
using Android.Content;
using Android.Util;
using Android.Views;
using Android.Widget;
using System.Collections.Generic;

namespace wadtt
{
    public class UI
    {
        public static readonly int VOICE = 1;
        public static SpeechIntent speechIntent = null;

        private static RadioButton getradioButton(Context context, string describe, System.EventHandler ev)
        {
            var rb = new RadioButton(context);
            rb.Text = describe;
            rb.SetPadding(20, 5, 5, 20);
            rb.Click += ev;
            return rb;
        }

        // Xamarin.Forms uses the word View to refer to visual objects such as buttons,
        // labels or text entry boxes.
        // Which may be more commonly known as controls of widgets.
        public static void display(Game game, IPlayer player, string around)
        {
            Activity activity = game.getActivity();
            int currentRoomId = player.CurrentRoom.ID;
            var scrollView = new ScrollView(activity)
            {
                LayoutParameters =
                    new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent,
                                    ViewGroup.LayoutParams.MatchParent)
            };
            activity.SetContentView(scrollView);

            var mainLayout = new LinearLayout(activity)
            {
                Orientation = Android.Widget.Orientation.Vertical,
                LayoutParameters =
                    new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent,
                                                    ViewGroup.LayoutParams.MatchParent)
            };

            // Text description of the room.
            var describe = new TextView(activity);
            describe.Text = around;
            describe.SetPadding(20, 5, 5, 20);
            mainLayout.AddView(describe);

                if (player.isTextInputEnabled())
                {
                    // Text input for commands.
                    // Half-way house to speech input.
                    var editText = new EditText(activity);
                    editText.SetLines(1);
                    editText.KeyPress += (object sender, View.KeyEventArgs ke) =>
                    {
                        ke.Handled = false;

                        if (ke.Event.Action == KeyEventActions.Down && ke.KeyCode == Keycode.Enter)
                        {
                            // The enter key is preserved...
                            // See http://stackoverflow.com/questions/6070805/prevent-enter-key-on-edittext-but-still-show-the-text-as-multi-line
                            // We can either strip it by overloading EditText with our own MyEditText to
                            // overload onkeyDown() and return true without preserving Keycode.Enter
                            // and uses super.onKeyDown(keyCode, event); for everything else.
                            // But better to leave it to the parser.
                            string input = editText.Text;
                            editText.Text = "";
                            var tokens = Parser.findTokens(input);
                            Engine.execute(tokens, game, player);
                        }
                    };
                    mainLayout.AddView(editText);
                    scrollView.AddView(mainLayout);
                }
                else if (player.isRadioButtonsEnabled())
                {
                    // Radio Buttons
                    var commands = new RadioGroup(activity);

                    if (player.CurrentRoom.Left != 0)
                    {
                        int gradient = Rooms.getGradient(currentRoomId, player.CurrentRoom.Left);
                        var left = getradioButton(activity, Route.GetRouteText("left", gradient), delegate
                        {
                            LinkedList<Lexicon.Token> tokens = new LinkedList<Lexicon.Token>();
                            Lexicon.Token token = new Lexicon.Token(Lexicon.lex.left);
                            tokens.AddLast(token);
                            Engine.execute(tokens, game, player);
                        });
                        commands.AddView(left);
                    }

                    if (player.CurrentRoom.Straight != 0)
                    {
                        int gradient = Rooms.getGradient(currentRoomId, player.CurrentRoom.Straight);
                        var straight = getradioButton(activity, Route.GetRouteText("straight ahead", gradient), delegate
                        {
                            LinkedList<Lexicon.Token> tokens = new LinkedList<Lexicon.Token>();
                            Lexicon.Token token = new Lexicon.Token(Lexicon.lex.straight);
                            tokens.AddLast(token);
                            Engine.execute(tokens, game, player);
                        });
                        commands.AddView(straight);
                    }

                    if (player.CurrentRoom.Right != 0)
                    {
                        int gradient = Rooms.getGradient(currentRoomId, player.CurrentRoom.Right);
                        var right = getradioButton(activity, Route.GetRouteText("right", gradient), delegate
                        {
                            LinkedList<Lexicon.Token> tokens = new LinkedList<Lexicon.Token>();
                            Lexicon.Token token = new Lexicon.Token(Lexicon.lex.right);
                            tokens.AddLast(token);
                            Engine.execute(tokens, game, player);
                        });
                        commands.AddView(right);
                    }

                    int toRoomId;
                    if (player.getRouteHere().canGoBack(currentRoomId, out toRoomId))
                    {
                        int gradient = Rooms.getGradient(currentRoomId, toRoomId);
                        var back = getradioButton(activity, Route.GetRouteText("back", gradient), delegate
                        {
                            LinkedList<Lexicon.Token> tokens = new LinkedList<Lexicon.Token>();
                            Lexicon.Token token = new Lexicon.Token(Lexicon.lex.back);
                            tokens.AddLast(token);
                            Engine.execute(tokens, game, player);
                        });
                        commands.AddView(back);
                    }

                    var manifest = getradioButton(activity, "Bag contents", delegate
                    {
                        LinkedList<Lexicon.Token> tokens = new LinkedList<Lexicon.Token>();
                        Lexicon.Token token = new Lexicon.Token(Lexicon.lex.baggage);
                        tokens.AddLast(token);
                        Engine.execute(tokens, game, player);
                    });
                    commands.AddView(manifest);

                    var roomContents = getradioButton(activity, "Room contents", delegate
                    {
                        LinkedList<Lexicon.Token> tokens = new LinkedList<Lexicon.Token>();
                        Lexicon.Token token = new Lexicon.Token(Lexicon.lex.describe);
                        tokens.AddLast(token);
                        Engine.execute(tokens, game, player);
                    });
                    commands.AddView(roomContents);

                    var takeall = getradioButton(activity, "Take everything", delegate
                    {
                        LinkedList<Lexicon.Token> tokens = new LinkedList<Lexicon.Token>();
                        Lexicon.Token token = new Lexicon.Token(Lexicon.lex.take);
                        tokens.AddLast(token);
                        token = new Lexicon.Token(Lexicon.lex.all);
                        tokens.AddLast(token);
                        Engine.execute(tokens, game, player);
                    });
                    commands.AddView(takeall);

                    var dropall = getradioButton(activity, "Drop everything", delegate
                    {
                        LinkedList<Lexicon.Token> tokens = new LinkedList<Lexicon.Token>();
                        Lexicon.Token token = new Lexicon.Token(Lexicon.lex.drop);
                        tokens.AddLast(token);
                        token = new Lexicon.Token(Lexicon.lex.all);
                        tokens.AddLast(token);
                        Engine.execute(tokens, game, player);
                    });
                    commands.AddView(dropall);

                    var tidalInfo = getradioButton(activity, "Tidal info", delegate
                    {
                        LinkedList<Lexicon.Token> tokens = new LinkedList<Lexicon.Token>();
                        Lexicon.Token token = new Lexicon.Token(Lexicon.lex.tides);
                        tokens.AddLast(token);
                        Engine.execute(tokens, game, player);
                    });
                    commands.AddView(tidalInfo);

                    var wayHere = getradioButton(activity, "Way here", delegate
                    {
                        LinkedList<Lexicon.Token> tokens = new LinkedList<Lexicon.Token>();
                        Lexicon.Token token = new Lexicon.Token(Lexicon.lex.route);
                        tokens.AddLast(token);
                        Engine.execute(tokens, game, player);
                    });
                    commands.AddView(wayHere);

                    var goblinPlaces = getradioButton(activity, "Goblin tracker", delegate
                    {
                        LinkedList<Lexicon.Token> tokens = new LinkedList<Lexicon.Token>();
                        Lexicon.Token token = new Lexicon.Token(Lexicon.lex.goblins);
                        tokens.AddLast(token);
                        Engine.execute(tokens, game, player);
                    });
                    commands.AddView(goblinPlaces);

                    var debugInfo = getradioButton(activity, "Debug Info", delegate
                    {
                        LinkedList<Lexicon.Token> tokens = new LinkedList<Lexicon.Token>();
                        Lexicon.Token token = new Lexicon.Token(Lexicon.lex.debug);
                        tokens.AddLast(token);
                        Engine.execute(tokens, game, player);
                    });
                    commands.AddView(debugInfo);

                    mainLayout.AddView(commands);
                    scrollView.AddView(mainLayout);
                }
                else if (player.isVoiceInputEnabled())
                {
                }
        }
    }
}