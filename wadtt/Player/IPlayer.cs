using System.Collections.Generic;

namespace wadtt
{
    public interface IPlayer
    {
        string Name { get; set; }
        Game Game { get; set; }
        IEnvironment CurrentEnvironment { get; set; }
        IRoom CurrentRoom { get; set; }

        void register(Game game);
        bool register(IEnvironment env);

        bool enter(IEnvironment environment);
        bool leave();

        // FIXME: Conversion from a list to a string is really the job of "something" else.
        //        Also can I ask question like have I got something to eat/drink
        //        Do I have any keys?? Anything sharp? Anything that can help me here??

        string bagContents();

        List<string> carrying();

        void enterRoom(int roomId);
        void leaveCurrentRoom();
        string lookAround(bool alreadyHere);

        string listen();
        string smell();

        Route getRouteHere();
        string getRouteHereAsText();

        // Not sure these should be here..
        bool isTextInputEnabled();
        bool isRadioButtonsEnabled();
        bool isVoiceInputEnabled();
    }
}