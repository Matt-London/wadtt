using System.Collections.Generic;
using Android.Util;

namespace wadtt
{
    public class Route
    {
        public Route(Route r)
        {
            m_route = (r == null) ? new List<int>() : new List<int>(r.m_route);
        }

        public Route()
        {
            m_route = new List<int>();
        }

        private List<int> m_route;

        public bool isEmpty()
        {
            return (m_route.Count == 0);
        }

        public int getCount()
        {
            return m_route.Count;
        }

        public int Peek()
        {
            return m_route[m_route.Count - 1];
        }

        public int Count()
        {
            return m_route.Count;
        }

        public bool Contains(int roomId)
        {
            return m_route.Contains(roomId);
        }

        public int cameFrom()
        {
            if (m_route.Count == 0)
            {
                return 0;
            }
            return Peek();
        }

        public string routeAsText()
        {
            Stack<int> reverse = new Stack<int>(m_route);
            Stack<int> forward = new Stack<int>(reverse);

            string wayHere = "";
            while (forward.Count > 0)
            {
                wayHere += (wayHere.Length > 0) ? ", " : "";
                wayHere += "room " + forward.Pop() + " ";
            }
            return wayHere;
        }

        public void saveRoom(int roomId)
        {
            if (m_route.Count == 0 || Peek() != roomId)
            {
                // Some rooms loop back to themselves.
                m_route.Add(roomId);
            }
        }

        public bool goBack(ref int roomId)
        {
            if (!canGoBack(roomId))
            {
                return false;
            }

            roomId = Peek();
            return true;
        }

        public bool canGoBack(int fromRoomId)
        {
            if (m_route.Count == 0)
            {
                Log.Debug(MainActivity.TAG, "You cannot remember the way back from here.");
                return false;
            }

            // Is there actually a way back from fromRoomId -> last room we were in.
            Room room = Room.GetRoomById(fromRoomId);
            int lastRoomId = Peek();
            if ( (room.Left != lastRoomId) && (room.Right != lastRoomId) && (room.Straight != lastRoomId) )
            {
                Log.Debug(MainActivity.TAG, "There is no way back from room {0} to room {1}", fromRoomId, lastRoomId);
                return false;
            }
            return true;
        }

        public bool canGoBack(int fromRoomId, out int toRoomId)
        {
            if (!canGoBack(fromRoomId))
            {
                toRoomId = 0;
                return false;
            }

            toRoomId = Peek();
            return true;
        }

        public static Route shortestRouteToRoom(int fromRoomId, int ToRoomId)
        {
            //Log.Debug(MainActivity.TAG, "Shortest route from {0} -> {1}", fromRoomId, ToRoomId);
            Rooms.roomCondition f = delegate(int roomid)
            {
                return (roomid == ToRoomId) ? 1 : 0;
            };

            return Rooms.shortestRoute(fromRoomId, f);
        }

        public bool getBackGradient(int fromRoomId, out int gradient)
        {
            int toRoomId;
            if (!canGoBack(fromRoomId, out toRoomId))
            {
                gradient = 0;
                return false;
            }

            gradient = Rooms.getGradient(fromRoomId, toRoomId);
            return true;
        }

        public static string GetRouteText(string direction, int gradient)
        {
            if (gradient == 0)
            {
                return "Go " + direction;
            }
            else if (gradient > 0)
            {
                return "Ascend " + direction;
            }

            return "Descend " + direction;
        }
    }
}