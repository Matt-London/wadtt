using Android.Util;
using System.Collections.Generic;

namespace wadtt
{
    // FIXME: Currently we associate some dubious attributes with the player:
    // a) Text To Speech
    // b) If Text Input is enabled
    // c) If Voice Input is enabled.

    public class Player : IPlayer
    {
        public string Name { get; set; }
        public Game Game { get; set; }
        public IEnvironment CurrentEnvironment { get; set; }
        public IRoom CurrentRoom { get; set; }

        public void register(Game game)
        {
            Game = game;
        }

        protected Route m_route = null;

        // FIXME: Dead or Alive is Environment dependent
        static private bool m_alive = true;

        private bool m_textInput = false;
        private bool m_voiceInput = true;
        private bool m_radioButtons = false;

        public bool register(IEnvironment env)
        {
            // We can register with as many Environments as we like.
            // But we remain an inactive user until we enter an Environment.
            // Returning to inactive when we leave an environment.
            return env.register(this);
        }

        public bool enter(IEnvironment env)
        {
            if (env == null)
            {
                return false;
            }

            // 1. If we enter a new environment:
            //    a) Set up our baggage, our health, reputation and location.
            //
            // 2. If we are returning we:
            //   a) Reinstate our baggage, our health, reputation and location.
            CurrentEnvironment = env;
            CurrentEnvironment.activate(this);

            return true;
        }

        public bool leave()
        {
            if (CurrentEnvironment == null)
            {
                return false;
            }

            // If we are awake in one world, we are asleep in others. 
            CurrentEnvironment.sleep(this);

            // FIXME: Maybe persist our baggage, our health, reputation and location.
            CurrentEnvironment = null;
            return true;
        }

        public bool isTextInputEnabled()
        {
            return m_textInput;
        }

        public void setTextInput(bool setting)
        {
            m_textInput = setting;
        }

        public bool isRadioButtonsEnabled()
        {
            return m_radioButtons;
        }

        public void setRadioButtonInput(bool setting)
        {
            m_radioButtons = setting;
        }

        public bool isVoiceInputEnabled()
        {
            return m_voiceInput;
        }

        public void setVoiceInput(bool setting)
        {
            m_voiceInput = setting;
        }

        public Player(string name)
        {
            Name = name;
            m_route = new Route();
        }

        public int getStartRoom()
        {
            if (CurrentRoom == null)
            {
                return CurrentEnvironment.getStartRoomId();
            }

            return CurrentRoom.ID;
        }

        public string getRouteHereAsText()
        {
            return m_route.routeAsText();
        }

        public void enterRoom(int roomId)
        {
            Debug.assert(roomId != 0);

            // FIXME: Unconvinced this is the right place to do this check.
            if (!m_alive)
            {
                // game.finish(); ????
                return;
            }

            Log.Debug(MainActivity.TAG, "We have entered room {0}", roomId);
            CurrentRoom = Room.GetRoomById(roomId);
        }

        public void leaveCurrentRoom()
        {
            // We need to remember every room we have left,
            // so the player can potentially backtrack.
            Log.Debug(MainActivity.TAG, "We are leaving room {0}", CurrentRoom.ID);
            m_route.saveRoom(CurrentRoom.ID);
        }

        public Route getRouteHere()
        {
            return m_route;
        }

        public string smell()
        {
            return Rooms.roomSmells(CurrentRoom.ID);
        }

        public string listen()
        {
            return Rooms.roomSounds(CurrentRoom.ID);
        }

        public string lookAround(bool alreadyHere)
        {
            int currentLevel = (m_route.getCount() == 0) ? CurrentRoom.Level : Room.GetRoomById(m_route.cameFrom()).Level;
            int gradient = CurrentRoom.Level - currentLevel;
            Rooms.Style roomStyle = Rooms.majorRoomStyle(m_route.cameFrom(), CurrentRoom.ID);

            string contents = Rooms.roomContents(this, CurrentRoom.ID);
            string creatures = Rooms.roomCreatures(this, CurrentRoom.ID);
            string leading = CurrentRoom.getLeading();
            string floor = Tide.getTidalText(CurrentRoom.Level);
            string[] stand = { "You are standing in ", "You're in ", "You look around and see " };
            string startText = (alreadyHere) ? Utils.getAny(stand)
                                             : Rooms.getGradientText(gradient, roomStyle);

            switch (Tide.getSeaLevel(CurrentRoom.Level))
            {
                case Tide.Sea.dry:
                    if (contents.Length != 0)
                    {
                        return string.Format("{0}{1}. On the {2} {3} There {4}. {5}", startText, CurrentRoom.Desc, CurrentRoom.Floor, contents, leading, creatures);
                    }
                    return string.Format("{0}{1} There {2}.", startText, CurrentRoom.Desc, leading);

                case Tide.Sea.damp:
                    if (contents.Length != 0)
                    {
                        return string.Format("{0}{1}. {2} {3} {4} There {5}.", startText, CurrentRoom.Desc, floor, contents, creatures, leading);
                    }
                    return string.Format("{0}{1} There {2}. {3}", startText, CurrentRoom.Desc, leading, creatures);

                case Tide.Sea.kneeLevel:
                case Tide.Sea.waistLevel:
                case Tide.Sea.neckLevel:
                    return string.Format("{0}{1} You stand {2} in water. {3} There {4}.", startText, CurrentRoom.Desc, floor, creatures, leading);

                case Tide.Sea.dead:
                    dies();
                    return "The water is too deep here. You have drowned.";
            }
            return "The room appears to be completely empty.";
        }

        public void dies()
        {
            m_alive = false;
        }

        public string bagContents()
        {
            return Rooms.bagContents(this);
        }

        public List<string> carrying()
        {
            IBaggageHandler baggageHandler = CurrentEnvironment.getBaggageHandler();
            return baggageHandler.getManifest(this);
        }
    }
}