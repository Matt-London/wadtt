namespace wadtt
{
    public class Score
    {
        static private int m_score = 0;

        static private string[] scoreText =
        {
            "You have currently scored ", "Your score is ", "So far you have "
        };

        static public int getScore()
        {
            return m_score;
        }

        static public string getScoreText()
        {
            return Utils.getAny(scoreText) + getScore().ToString();
        }
    }
}