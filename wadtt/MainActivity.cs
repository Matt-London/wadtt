﻿using Android.App;
using Android.OS;
using Android.Speech.Tts;
using Android.Util;
using Android.Content;
using Android.Speech;
using Android.Content.Res;
using System.Collections.Generic;
using System;

namespace wadtt
{
    // These values are used to populate AndroidManifest.xml.
    //[Activity(Label = "wadtt", MainLauncher = true, NoHistory = true,
    [Activity(Label = "wadtt",
              Icon = "@drawable/icon",
              ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation |
                                     Android.Content.PM.ConfigChanges.ScreenSize)]
    public class MainActivity : Activity, TextToSpeech.IOnInitListener
    {
        public readonly int VOICE = 1;
        public SpeechIntent speechIntent = null;
        public static string TAG = "wadtt";
        private Game game = null;
        private Player player = null;

        private int roomid;

        protected override void OnResume()
        {
            Log.Debug(TAG, "+++ Main.OnResume");
            base.OnResume();

            string rec = Android.Content.PM.PackageManager.FeatureMicrophone;
            if (rec != "android.hardware.microphone")
            {
                Log.Debug(TAG, "No microphone available .. using UI");
            }
            else
            {
                Log.Debug(TAG, "<<<< Check if I am speaking");
                while (game.getTextToSpeech().IsSpeaking)
                {
                    Log.Debug(TAG, "<<<< Dont listen to self speak");
                    // FIXME: Do I speak for too long?
                    // FIXME: When I speak the background noise should die down!!
                    System.Threading.Thread.Sleep(500);
                }
                CreateSpeechSystemIntent();
            }
        }

        private void CreateSpeechSystemIntent()
        {
            Log.Debug(TAG, ">>>> Creating Speech System Intent");
            var voiceIntent = new Intent(RecognizerIntent.ActionRecognizeSpeech);
            voiceIntent.PutExtra(RecognizerIntent.ExtraLanguageModel, RecognizerIntent.LanguageModelFreeForm);
            voiceIntent.PutExtra(RecognizerIntent.ExtraPrompt, Application.Context.GetString(Resource.String.speakNow));
            voiceIntent.PutExtra(RecognizerIntent.ExtraSpeechInputCompleteSilenceLengthMillis, 4000);
            voiceIntent.PutExtra(RecognizerIntent.ExtraSpeechInputPossiblyCompleteSilenceLengthMillis, 4000);
            voiceIntent.PutExtra(RecognizerIntent.ExtraSpeechInputMinimumLengthMillis, 6000);
            voiceIntent.PutExtra(RecognizerIntent.ExtraMaxResults, 1);
            voiceIntent.PutExtra(RecognizerIntent.ExtraLanguage, Java.Util.Locale.Default);
            StartActivityForResult(voiceIntent, VOICE);
        }

        protected override void OnActivityResult(int requestCode, Result resultVal, Intent data)
        {
            base.OnActivityResult(requestCode, resultVal, data);

            if (requestCode == UI.VOICE)
            {
                Log.Debug(TAG, ">>>> OnActivityResult for Speech. Result=" + resultVal);
                if (resultVal == Result.Ok)
                {
                    IList<string> words = data.GetStringArrayListExtra(RecognizerIntent.ExtraResults);
                    string input = "";
                    foreach (string word in words)
                    {
                        input += word + " ";
                    }

                    var tokens = Parser.findTokens(input);
                    Engine.execute(tokens, game, player);
                }

            }
        }

        protected override void OnStart()
        {
            Log.Debug(TAG, "+++ Main.OnStart");
            base.OnStart();
        }

        protected override void OnPause()
        {
            Log.Debug(TAG, "+++ Main.OnPause");
            base.OnStop();
            //game.getSound().Stop();
        }

        protected override void OnStop()
        {
            Log.Debug(TAG, "+++ Main.OnStop");
            base.OnStop();
        }

        public override void OnConfigurationChanged(Configuration newConfig)
        {
            Log.Debug(TAG, "+++ Main.OnConfigurationChanged");
            base.OnConfigurationChanged(newConfig);
        }

        protected override void OnCreate(Bundle bundle)
        {
            // Android has just created this activity.
            // So we initialise any essential components
            // Next callback:  Always OnStart().
            Log.Debug(TAG, "+++ Main.OnCreate");
            base.OnCreate(bundle);
            string gamesFile = "game.db";
            IDatabase database = new Database(gamesFile, Resources);

            game = new Game("Adventure", database, this, this);
            player = new Player("Matt");
            game.register(player);

            // Start the Game proper; the Player enters the Game in a given Environment.
            IEnvironment env = game.getRegisteredEnvironment("Cave");
            player.register(env);
            player.enter(env);
        }

        private enum TTS_State { NotStarted = 0, Started = 1, Ready = 2 };
        private TTS_State state = TTS_State.NotStarted;

        void TextToSpeech.IOnInitListener.OnInit(OperationResult status)
        {
            if (state != TTS_State.NotStarted)
            {
                Log.Debug(TAG, "<<<< TTSOnInit() already STARTED");
                return;
            }
            Log.Debug(TAG, "<<<< TTSOnInit() STARTED");
            state = TTS_State.Started;

            game.getTextToSpeech().SetLanguage(Java.Util.Locale.Default);

            // set the speed and pitch
            //textToSpeech.SetPitch(.8f);  // Lower value = lower voice.
            //textToSpeech.SetSpeechRate(.6f);

            // Only once TTS is working can we enter our first room.
            roomid = player.getStartRoom();
            Engine.enterRoom(game, player, roomid);
            Log.Debug(TAG, "<<<< TTSOnInit() READY");
        }
    }
}
