using Android.App;
using Android.Content;
using Android.Speech;
using Android.Util;

namespace wadtt
{
    public class SpeechIntent
    {
        public SpeechIntent(Activity activity, int voice)
        {
            Log.Debug(MainActivity.TAG, "Creating Speech System Intent");
            var voiceIntent = new Intent(RecognizerIntent.ActionRecognizeSpeech);
            voiceIntent.PutExtra(RecognizerIntent.ExtraLanguageModel, RecognizerIntent.LanguageModelFreeForm);
            voiceIntent.PutExtra(RecognizerIntent.ExtraPrompt, Application.Context.GetString(Resource.String.speakNow));
            voiceIntent.PutExtra(RecognizerIntent.ExtraSpeechInputCompleteSilenceLengthMillis, 1500);
            voiceIntent.PutExtra(RecognizerIntent.ExtraSpeechInputPossiblyCompleteSilenceLengthMillis, 1500);
            voiceIntent.PutExtra(RecognizerIntent.ExtraSpeechInputMinimumLengthMillis, 15000);
            voiceIntent.PutExtra(RecognizerIntent.ExtraMaxResults, 1);
            voiceIntent.PutExtra(RecognizerIntent.ExtraLanguage, Java.Util.Locale.Default);
            activity.StartActivityForResult(voiceIntent, voice);
        }

        public static bool IsMicrophoneAvailable()
        {
            string rec = Android.Content.PM.PackageManager.FeatureMicrophone;
            if (rec != "android.hardware.microphone")
            {
                Log.Debug(MainActivity.TAG, "No microphone available ..");
                return false;
            }

            return true;
        }
    }
}