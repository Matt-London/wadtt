using Android.Content;
using Android.Media;
using Android.Util;

namespace wadtt
{
    public class Sounds : ISounds
    {
        private MediaPlayer mp = null;

        public void Stop()
        {
            if (mp != null)
            {
                mp.Stop();
                mp.Release();
                mp = null;
            }
        }

        public void newSound(Context context, Sound.Noise noise)
        {
            if (mp != null)
            {
                // FIXME: Nothing accumulates at the moment.
                // This needs study ..how to layer sounds..
                Log.Debug(MainActivity.TAG, "Clear old sound..");
                mp.Pause();
                mp.Stop();
                mp.Release();
            }

            Log.Debug(MainActivity.TAG, "New sound..");
            mp = MediaPlayer.Create(context, Sounds.getSound(noise));
            mp.Start();
        }

        // FIXME: Some sounds can be accumulative, others are clearly not.
        //        Outside is outside only.
        public static int getSound(Sound.Noise sound)
        {
            int noise = Resource.Raw.rainforest;
            switch (sound)
            {
                case Sound.Noise.avalanche:
                    noise = Resource.Raw.avalanche;
                    break;

                case Sound.Noise.bats:
                    noise = Resource.Raw.bats;
                    break;

                case Sound.Noise.brook:
                    noise = Resource.Raw.brook;
                    break;

                case Sound.Noise.rain:
                    noise = Resource.Raw.rain;
                    break;

                case Sound.Noise.rain2:
                    noise = Resource.Raw.rain2;
                    break;

                case Sound.Noise.outside:
                    noise = Resource.Raw.rainforest;
                    break;

                case Sound.Noise.rockslide:
                    noise = Resource.Raw.rockslide;
                    break;

                case Sound.Noise.siren:
                    noise = Resource.Raw.siren;
                    break;

                case Sound.Noise.water:
                    noise = Resource.Raw.water;
                    break;

                case Sound.Noise.waterdrops:
                    noise = Resource.Raw.waterdrops;
                    break;

                case Sound.Noise.waterendford:
                    noise = Resource.Raw.waterendford;
                    break;

                default:
                    noise = Resource.Raw.rainforest;
                    break;

            }

            return noise;
        }
    }
}