namespace wadtt
{
    public class Sound
    {
        public enum Noise
        {
            outside = 0,
            siren,
            bats,
            brook,
            rain,
            rain2,
            rockslide,
            water,
            waterdrops,
            avalanche,
            waterendford = 10

        };
    }
}