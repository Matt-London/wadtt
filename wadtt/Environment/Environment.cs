using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wadtt
{
    public class Environment : IEnvironment
    {
        public string Name { get; set; }
        public IGame Game { get; set; }

        private List<Creature> m_Creatures;

        // A Player can be participate in more than one Environment, but can
        // only be Active in one at anytime.
        enum PlayerState {  Active = 1, InActive = 2 };
        class PlayerInEnvironment
        {
            public IPlayer player;
            public PlayerState state;

            public PlayerInEnvironment(IPlayer player, PlayerState state)
            {
                this.player = player;
                this.state = state;
            }
        };

        private Dictionary<string, PlayerInEnvironment> m_Players;
        private List<IPuzzle> m_Puzzles;

        // FIXME: Currently Environment doesn't use room information directly.
        //        only through the baggage handler.
        private List<IRoom> m_Rooms;
        private List<IWeather> m_Weathers;

        // Within an Environment the BaggageHandler is responsible for transferring items
        // between rooms, players and creatures.
        // It can in exceptional circumstances create and destroy items.
        private IBaggageHandler m_baggageHandler = null;
        // And the CreatureHandler handles all creature movement, action and reaction.
        private ICreatureHandler m_creatureHandler = null;

        public Environment(IGame game, string name)
        {
            Game = game;
            Name = name;

            m_Creatures = new List<Creature>();
            m_Players = new Dictionary<string, PlayerInEnvironment>();
            m_Puzzles = new List<IPuzzle>();
            m_Rooms = new List<IRoom>();
            m_Weathers = new List<IWeather>();
            game.register(this);
        }

        public IBaggageHandler getBaggageHandler()
        {
            if (m_baggageHandler == null)
            {
                m_baggageHandler = new BaggageHandler(this);
            }
            return m_baggageHandler;
        }

        public ICreatureHandler getCreatureHandler()
        {
            if (m_creatureHandler == null)
            {
                m_creatureHandler = new CreatureHandler(this);
            }
            return m_creatureHandler;
        }

        public int getStartRoomId()
        {
            return Game.getAvailableEnvironmentFromDB(Name).RoomStartID;
        }

        public int getEndRoomId()
        {
            return Game.getAvailableEnvironmentFromDB(Name).RoomEndID;
        }

        public List<Creature> getAvailableCreatures()
        {
            List<Creature> creatures = CreatureList.GetAvailableCreatures(Name);
            return creatures;
        }

        public List<string> getAvailableCreatureNames()
        {
            List<string> names = new List<string>();
            List<Creature> creatures = getAvailableCreatures();
            foreach (var creature in creatures)
            {
                // FIXME: This could be quite expensive if we have lots of creatures!!
                bool found = false;
                foreach (string name in names)
                {
                    if (name.Equals(creature.Name))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    names.Add(creature.Name);
                }
            }
            return names;
        }


        public List<string> getActivePlayers()
        {
            return m_Players.Keys.ToList();
        }

        public List<Creature> getRegisteredCreatures()
        {
            return m_Creatures;
        }

        public List<IPuzzle> getRegisteredPuzzles()
        {
            return m_Puzzles;
        }

        public List<IWeather> getRegisteredWeathers()
        {
            return m_Weathers;
        }

        public bool register(Creature creature)
        {
            if (creature == null)
            {
                return false;
            }

            if (m_Creatures.Contains(creature))
            {
                return false;
            }

            m_Creatures.Add(creature);

            // Every Environment has its own handlers with which we must register.
            ICreatureHandler creatureHandler = getCreatureHandler();
            return creatureHandler.register(creature) && creature.register(this);
        }

        public bool register(IPlayer player)
        {
            if (player == null)
            {
                return false;
            }

            if (m_Players.ContainsKey(player.Name))
            {
                return false;
            }

            PlayerInEnvironment newPlayer = new PlayerInEnvironment(player, PlayerState.InActive);
            m_Players.Add(player.Name, newPlayer);
            return true;
        }

        public bool activate(IPlayer player)
        {
            return changeState(player, PlayerState.Active);
        }

        public bool sleep(IPlayer player)
        {
            return changeState(player, PlayerState.InActive);
        }

        private bool changeState(IPlayer player, PlayerState newState)
        {
            if (player == null)
            {
                return false;
            }

            if (!m_Players.ContainsKey(player.Name))
            {
                return false;
            }

            try
            {
                if (newState == m_Players[player.Name].state)
                {
                    // Same state.
                    return false;
                }
                PlayerInEnvironment newDetails = new PlayerInEnvironment(player, newState);
                m_Players[player.Name] = newDetails;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool register(IPuzzle puzzle)
        {
            if (puzzle == null)
            {
                return false;
            }

            if (m_Puzzles.Contains(puzzle))
            {
                return false;
            }

            m_Puzzles.Add(puzzle);
            return true;
        }

        public bool register(IWeather weather)
        {
            if (weather == null)
            {
                return false;
            }

            if (m_Weathers.Contains(weather))
            {
                return false;
            }

            m_Weathers.Add(weather);
            return true;
        }

        public bool register(IRoom room)
        {
            if (room == null)
            {
                return false;
            }

            // We cannot re-register the same room, certainly not here.
            // Not sure if this is good .. lots of enumeration. Maybe a Hash table??
            if (m_Rooms.Exists(delegate(IRoom x) { return (x.ID == room.ID) ? true : false ;}))
            {
                return false;
            }

            bool registered = false;
            try
            {
                m_Rooms.Add(room);
                registered = true;
            }
            catch
            {
            }

            // Every Environment has its own handlers with which we must register.
            IBaggageHandler baggageHandler = getBaggageHandler();
            return registered && baggageHandler.register(room);
        }
    }
}