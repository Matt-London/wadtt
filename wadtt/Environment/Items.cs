using Android.Util;
using SQLite;
using System;
using System.Collections.Generic;

namespace wadtt
{
    public class Items
    {
        private static Dictionary<string, int> m_synonyms = null;

        public static Item GetItemById(int id)
        {
            try
            {
                string sql = "SELECT * FROM Item WHERE ID=?";
                object[] args = { id };
                List<Item> items = Database.db.Query<Item>(sql, args);
                if (items.Count == 0)
                {
                    Log.Error(MainActivity.TAG, "Oops we have no item number " + id);
                    return null;
                }
                return items[0];
            }
            catch (SQLiteException ex)
            {
                Log.Debug(MainActivity.TAG, ex.Message);
                return null;
            }
        }

        public static int GetCount()
        {
            return Database.GetTotalCount("Item");
        }

        private static char[] m_delimiters = { '|' };

        public enum ItemState { known, unknown, ambiguous };

        public static ItemState getItemId(string words, out int itemId)
        {
            itemId = 0;
            var synonyms = getSynonyms();
            if (!synonyms.ContainsKey(words))
            {
                return ItemState.unknown;
            }

            itemId = synonyms[words];
            return (itemId == 0) ? ItemState.ambiguous : ItemState.known;
        }

        public static Dictionary<string, int> getSynonyms()
        {
            if (m_synonyms == null)
            {
                getInstance();
            }
            return m_synonyms;
        }

        private static void getInstance()
        {
            m_synonyms = new Dictionary<string, int>();
            for (int itemId = 1; itemId <= GetCount(); ++itemId)
            {
                Item item = GetItemById(itemId);
                if (m_synonyms.ContainsKey(item.Name))
                {
                    // This is ambiguous
                    m_synonyms[item.Name] = 0;
                }
                else
                {
                    m_synonyms.Add(item.Name, itemId);
                }

                // Emample of synonyms: "tin|milk tin|tin of milk"
                string[] synonyms =
                    item.Synonyms.ToLower().Split(m_delimiters, StringSplitOptions.RemoveEmptyEntries);
                foreach (var variant in synonyms)
                {
                    if (m_synonyms.ContainsKey(variant))
                    {
                        // This is ambiguous
                        m_synonyms[variant] = 0;
                    }
                    else
                    {
                        m_synonyms.Add(variant, itemId);
                    }
                }
            }
        }
    }
}