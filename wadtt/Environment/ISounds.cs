using Android.Content;

namespace wadtt
{
    public interface ISounds
    {
        void newSound(Context context, Sound.Noise noise);
        void Stop();
    }
}