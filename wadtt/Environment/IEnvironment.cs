using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// Environment.Add(ICreature)
// Environment.Add(IPlayer)
// Environment.Add(IRoom)
// Environment.Add(IWeather)

namespace wadtt
{
    public interface IEnvironment
    {
        string Name { get; set; }
        IGame Game { get; set; }

        int getStartRoomId();
        int getEndRoomId();

        // Available means available to exist in this environment.
        List<Creature> getAvailableCreatures();
        List<string> getAvailableCreatureNames();
        List<Creature> getRegisteredCreatures();
        bool register(Creature animal);

        // Only when we register will we actually exist in the game.
        bool register(IPuzzle puzzle);
        bool register(IRoom room);
        bool register(IWeather weather);

        List<IPuzzle> getRegisteredPuzzles();
        List<IWeather> getRegisteredWeathers();

        bool register(IPlayer player);
        List<string> getActivePlayers();
        // Also List<IPlayer> of Active, InActive and All Players.
        bool activate(IPlayer player);
        bool sleep(IPlayer leave);

        IBaggageHandler getBaggageHandler();
        ICreatureHandler getCreatureHandler();
    }
}