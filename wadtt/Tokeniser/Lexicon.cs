using System.Collections.Generic;

namespace wadtt
{
    // Here is the complete list of English words we recognise
    public class Lexicon
    {
        private static Dictionary<string, Token> m_tokens;
        public static Dictionary<string, Token> getInstance()
        {
            if (m_tokens == null)
            {
                m_tokens = new Dictionary<string, Token>();

                // Add an entry for every word we recognise
                foreach (var token in Utils.GetValues<Lexicon.lex>())
                {
                    string key = token.ToString().ToLower();
                    m_tokens.Add(key, new Token(token));
                }
            }
            return m_tokens;
        }

        public enum lex
        {

            unknown = 0,
            eotext = 1,
            debug = 2,
            read = 9,
            left = 10,
            right = 11,
            straight = 12,
            go = 13,
            descend = 15,
            back = 16,
            head = 17,
            leave = 18,
            Return = 19,
            on = 20,
            up = 21,
            down = 22,
            across = 23,
            walk = 24,
            run = 25,
            carry = 26,
            pick = 27,
            hold = 28,
            holding = 29,
            carrying = 30,
            take = 31,
            close = 32,
            to = 33,
            enter = 34,
            into = 35,
            nearby = 36,
            through = 37,
            from = 38,
            got = 39,
            retreat = 40,
            last = 41,
            onto = 42,
            tell = 43,
            listen = 44,
            hear = 45,
            smell = 46,
            look = 47,
            describe = 48,
            see = 49,
            sounds = 50,
            smells = 51,
            climb = 52,
            examine = 53,
            carefully = 54,
            recognise = 55,
            been = 56,
            before = 57,
            away = 58,
            tie = 59,
            untie = 60,
            unhook = 61,
            light = 62,
            ascend = 63,
            near = 64,
            far = 65,
            In = 66,
            previous = 67,
            the = 68,
            place = 69,
            location = 70,
            me = 71,
            next = 72,
            sleep = 73,
            sit = 74,
            trapdoor = 75,
            trap = 76,
            leaves = 77,
            follow = 78,
            after = 79,
            call = 80,
            talk = 81,
            speak = 82,
            we = 83,
            give = 84,
            tidal = 85,
            information = 86,
            am = 87,
            collect = 88,
            gather = 89,
            that = 90,
            open = 91,
            hand = 92,
            boil = 93,
            fry = 94,
            cut = 95,
            baggage = 96,
            notice = 97,
            writing = 98,
            shovel = 99,

            // Creatures
            cat = 100,
            cats = 101,
            goblin = 102,
            goblins = 103,
            spider = 104,
            spiders = 105,
            s = 106,        // For what's etc
            m = 107,        // For i'm Is this a sensible approach??

            // Questions
            are = 200,
            there = 202,
            any = 203,
            many = 204,
            where = 205,
            some = 206,
            Is = 207,
            my = 208,
            This = 209,
            please = 210,
            thank = 211,
            you = 212,
            thanks = 213,
            could = 214,
            would = 215,
            i = 216,
            here = 217,
            around = 218,
            by = 219,
            with = 220,
            can = 221,
            anything = 222,
            hint = 223,
            Do = 224,
            have = 225,
            a = 226,
            and = 227,
            put = 228,
            get = 229,
            pull = 230,
            plant = 231,
            row = 232,
            swim = 233,
            wade = 234,
            help = 235,
            of = 236,
            feed = 237,
            stroke = 238,
            lift = 239,
            use = 240,
            blow = 241,
            play = 242,
            Throw = 243,
            strike = 244,
            Break = 245,
            kick = 246,
            push = 247,
            drop = 248,
            jump = 249,
            pay = 250,
            empty = 251,
            fill = 252,
            turn = 253,
            eat = 254,
            drink = 255,
            pass = 256,
            what = 257,
            whats = 258,
            knock = 259,
            bang = 260,
            sweep = 261,
            an = 262,
            score = 263,
            scored = 264,
            something = 265,
            someone = 266,
            anyone = 267,
            does = 268,
            it = 269,
            friends = 270,
            off = 271,
            analyse = 272,
            analysis = 273,
            mode = 274,
            friendly = 275,
            cook = 276,
            few = 277,
            hot = 278,
            cold = 279,
            fire = 280,
            warm = 281,
            own = 283,
            under = 284,
            Switch = 285,
            analysing = 286,
            start = 287,
            stop = 288,
            noise = 289,
            noises = 290,
            sound = 291,
            yes = 292,
            no = 293,
            okay = 294,
            forward = 295,
            at = 296,
            written = 297,
            If = 298,
            thats = 299,

            // Environment
            room = 300,
            cave = 301,
            cavern = 302,
            door = 303,
            passage = 304,
            tunnel = 305,
            stairs = 310,
            all = 311,
            everything = 312,
            possession = 313,
            chamber = 314,
            opening = 315,
            way = 316,
            tide = 317,
            tides = 318,
            floor = 319,
            route = 320,
            stairway = 321,
            staircase = 322,
            distant = 323,
            distance = 324,
            caves = 325,
            rooms = 326,
            passages = 327,
            tunnels = 328,
            caverns = 329,
            area = 330,
            vicinity = 331,
            ground = 332,
            cellar = 333,
            hall = 334,
            about = 335,
            present = 336,
            available = 337,
            visible = 338,
            wheres = 339,
            north = 340,
            south = 341,
            east = 342,
            west = 343,
            side = 344,
            our = 345,
            your = 346,
            repeat = 347,
            hungry = 348,
            thirsty = 349,
            tired = 350,
            bored = 351,
            sleepy = 352,
            confused = 353,
            ok = 354,
            im = 355,
            just = 356,

            // Items
            one = 400,
            two = 401,
            three = 402,
            four = 403,
            five = 404,
            six = 405,
            seven = 406,
            eight = 407,
            nine = 408,
            ten = 409,
            eleven = 410,
            twelve = 411,
            thirteen = 412,

            coin = 420,
            coins = 421,
            gold = 422,
            candle = 423,
            candles = 424,
            matches = 425,
            tin = 426,
            bottle = 427,
            bed = 428,
            mattress = 429,
            key = 430,
            Lock = 431,
            boat = 432,
            water = 433,
            whistle = 434,
            flute = 435,
            knife = 436,
            bowl = 437,
            porridge = 438,
            liquid = 439,
            cup = 440,
            chair = 441,
            table = 442,
            spoon = 443,
            fork = 444,
            match = 445,
            broom = 446,
            exit = 447,
            soup = 448,
            bread = 449,
            closet = 450,
            tap = 451,
            taps = 452,
            ceiling = 453,
            mirror = 454,
            wall = 455,
            walls = 456,
            window = 457,
            windows = 458,
            how = 459,
            did = 460,
            was = 461,
            nearest = 462,
            moving = 463,
            will = 464,
            over = 465,
            become = 466,
            best = 467,
            think = 468


            // 161 unique words (I think I need around 2000).
        };

        // We cannot add a POS tag at this moment because some tags are contect sensitive
        // ie "plant the plant seed"
        public class Token
        {
            public string raw { get; set; }
            public Lexicon.lex lex { get; set; }

            public Token(Lexicon.lex lex)
            {
                this.lex = lex;
                this.raw = null;
            }

            public Token(Lexicon.lex lex, string raw)
            {
                this.lex = lex;
                this.raw = raw;
            }
        };

        public static Token getToken(string word)
        {
            Token token;
            return getInstance().TryGetValue(word, out token) ? token : new Token(lex.unknown, word);
        }

        public static string getWord(Token token)
        {
            string ret;
            if (token.lex != lex.unknown)
            {
                ret = token.lex.ToString();
            }
            else
            {
                ret = (token.raw == null) ? "unknown" : ("unknown(" + token.raw + ")");
            }

            switch (ret)
            {
                case "Break":
                case "Do":
                case "If":
                case "In":
                case "Is":
                case "Lock":
                case "Return":
                case "Switch":
                case "This":
                case "Throw":
                    return ret.ToLower();

                default:
                    return ret;
            }
        }
    }
}