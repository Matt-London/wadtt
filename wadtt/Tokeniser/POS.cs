using System.Collections.Generic;

namespace wadtt
{
    public class POS
    {
        // Derived from the Universal Parts Of Speech tags under
        // http://universaldependencies.org/u/pos/index.html
        // A part-of-speech tag represents the abstract lexical category
        // associated with a word.
        //
        // I checked words using http://nlp.stanford.edu:8080/parser/
        public enum pos
        {
            // Adjectives, typically modify nouns to add property or attributes.
            // Examples: big, old, green, first, second, third.
            ADJ,

            // Adverbs typically modify verbs to add time, place, direction or manner.
            // Examples: very, where, when, never, up, today, soon
            ADV,

            // Interjection is a word most used as an exclamation to express emotion.
            // Examples: Hi, Hello.
            INTJ,

            // Nouns denote a person, place, thing, an animal, an idea.
            // Examples: cat, room, goblin, sword, air
            NOUN,

            // Words that signify events or actions.
            // Examples: run, eat, sleep, swim
            VERB,

            // Adposition cover prepositions and postpositions, word that complement
            // a noun phrase
            // Examples: in, to, under
            ADP,

            // An Auxiliary is a verb that accompanies another verb to express
            // grammatical distinction not carried by the word itself such as
            // mood, person, tense.
            // Examples: has, is, do, should, must
            AUX,

            // Coordinating conjunction links words to provide a semantic relationship
            // between them.
            // Examples: and, or, but
            CONJ,

            // Determiners modify nouns to express the reference of the nound phrase
            // in this context.
            // Examples: this, which
            DET,

            // Particles are function words that must be associated with another word
            // or phrase to imprt meaning
            // Examples: May I, Please can I.
            PART,

            // A numeral expresses a number and a relation such as quantity, sequence.
            // Examples: one, two, three
            NUM,

            // Pronouns substitute for nouns, whose menaing is recoverable from the
            // linguistic context.
            // Examples: i, we, they, this, what, everything, anything, somebody, something.
            PRON,

            SCONJ,
            // A subordinating conjuction links constructions by making one a constituent
            // of another.
            // Examples: if, while, until

            // These are words that for some reason cannot be assigned another POS tag.
            X,

            //-----------------------------------------------------------------------------------
            // These are my refinements.

            // Sense verbs: hear, see, feel, taste, smell
            SENSE_VERB,
            
            // Personal pronoun: I, we, you
            PERSONAL_PRON,

            // Creature type noun: goblin, cat, dog, spider
            CREATURE,

            LOCATION,

            OPENING,

            ITEM,

            HAND_VERB,

            MOVE_VERB,

            INDEFINITE_ADVERB,

            // "am", "are".
            AM_OR_ARE_WORD,

            // "can", "do".
            CAN_OR_DO_WORD,

            // "the", "this".
            THE_OR_THIS_WORD,

            // "the", "my", "our", "your"
            THE_WORD,

            // "any", "some", "many"
            MULTIPLE_DETERMINER,

            // "a", "an", "the"
            SINGLE_DETERMINER,

            // "about", "around", "here"
            GENERAL_PLACE,

            // "left", "right", "straight"
            DIRECTION,
        };

        // Perhaps we make a dictionary at some point, But for now we just use switch statements.
        // private static Dictionary<Lexicon.Token,pos> m_tokens;

        public static bool POSMatch(pos p, Lexicon.lex token)
        {
            switch (p)
            {
                case pos.NOUN:
                    return POSCheck.isCreature(token) || POSCheck.isLocation(token) || POSCheck.isOpening(token)
                           || POSCheck.isItem(token);

                case pos.CREATURE:
                    return POSCheck.isCreature(token);

                case pos.ITEM:
                    return POSCheck.isItem(token);

                case pos.LOCATION:
                    return POSCheck.isLocation(token);

                case pos.OPENING:
                    return POSCheck.isOpening(token);

                case pos.NUM:
                    return POSCheck.isNumber(token);

                case pos.PERSONAL_PRON:
                    return POSCheck.isPersonalPronoun(token);

                case pos.SENSE_VERB:
                    return POSCheck.isSenseVerb(token);

                case pos.HAND_VERB:
                    return POSCheck.isHandVerb(token);

                case pos.MOVE_VERB:
                    return POSCheck.isMoveVerb(token);

                case pos.INDEFINITE_ADVERB:
                    return POSCheck.isIndefiniteAdverb(token);

                case pos.AM_OR_ARE_WORD:
                    return POSCheck.isAmOrAre(token);

                case pos.CAN_OR_DO_WORD:
                    return POSCheck.isCanOrDo(token);

                case pos.THE_OR_THIS_WORD:
                    return POSCheck.isTheOrThis(token);

                case pos.MULTIPLE_DETERMINER:
                    return POSCheck.isMultipleDeterminer(token);

                case pos.SINGLE_DETERMINER:
                    return POSCheck.isSingleDeterminer(token);

                case pos.GENERAL_PLACE:
                    return POSCheck.isGeneralPlace(token);

                case pos.DIRECTION:
                    return POSCheck.isDirection(token);

                case pos.THE_WORD:
                    return POSCheck.isTheWord(token);
                default:
                    return false;
            }
        }
    }
}