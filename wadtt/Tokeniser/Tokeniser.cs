using Android.Util;
using System;
using System.Collections.Generic;

// Note that we store tokens inside a LinkedList for ease of removing redundant tokens in the
// middle of a sentence.

namespace wadtt
{
    public class Tokeniser
    {
        private static char[] m_delimiters = { ' ', ',', '.', '\'', '\n', '\r' };

        // We convert m_fromTokens -> m_tokens.
        // Sort of Natural language compression.
        private LinkedList<Lexicon.Token> m_tokens = null;

        private int m_polite = 0;

        // So that we can say "Actually you are in a room, but I get your drift."
        private bool m_caveLikeWords = false;
        private bool m_roomLikeWords = false;
        private bool m_stairLikeWords = false;
        private bool m_tunnelLikeWords = false;

        // Reduce the sentence.
        public Tokeniser(string input)
        {
            string[] words = input.ToLower().Split(m_delimiters, StringSplitOptions.RemoveEmptyEntries);
            LinkedList<Lexicon.Token> q = new LinkedList<Lexicon.Token>();
            bool converted = convert(words, q);
            if (!converted)
            {
                Log.Debug(MainActivity.TAG, "Tokenise: Unexpected word in .. {0}", input);
            }
            else
            {
                Log.Debug(MainActivity.TAG, "Tokenise: Converted into {0} tokens .. {1}", q.Count, input);
            }

            m_tokens = reduce(q);
        }

        private bool convert (string []strings, LinkedList<Lexicon.Token> q)
        {
            foreach (var s in strings)
            {
                var token = Lexicon.getToken(s);
                q.AddLast(token);
            }
            return true;
        }

        private LinkedList<Lexicon.Token> reduce(LinkedList<Lexicon.Token> q)
        {
            LinkedList<Lexicon.Token> reduced = new LinkedList<Lexicon.Token>();

            var token = getNextToken(q);
            while (token.lex != Lexicon.lex.eotext)
            {
                reduced.AddLast(token);
                token = getNextToken(q);
            }
            return reduced;
        }

        private Lexicon.Token getNextToken(LinkedList<Lexicon.Token> q)
        {
            bool useCurrentToken = false;
            Lexicon.lex token = Lexicon.lex.unknown;
            string raw = null;

            while ((useCurrentToken) || (q.Count > 0))
            {
                // Flush from readahead first.
                if (!useCurrentToken)
                {
                    token = q.First.Value.lex;
                    raw = q.First.Value.raw;
                    q.RemoveFirst();
                    useCurrentToken = false;
                }
                
                switch (token)
                {
                    //----------------------------------------------------------------------------
                    // Single words.
                    //----------------------------------------------------------------------------
                    // Ignoring supposedly non-selective "stop" words that add nothing to the request.
                    case Lexicon.lex.thanks:
                    case Lexicon.lex.carefully:
                        m_polite++;
                        continue;

                    case Lexicon.lex.about:
                        // "about" "here" -> "here"
                        if (Reductor.reduce(q, Lexicon.lex.here))
                        {
                            token = Lexicon.lex.here;
                            useCurrentToken = true;
                            continue;
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.all:
                        // "all" "thats" -> "all"
                        Reductor.reduce(q, Lexicon.lex.thats);
                        return new Lexicon.Token(token);

                    case Lexicon.lex.are:
                        // "are there" -> "are"
                        Reductor.reduce(q, Lexicon.lex.there);

                        if ( Reductor.reduceToLeft(q, POS.pos.MULTIPLE_DETERMINER))
                        {
                            // "are" "any|many|some" -> "are"
                        }
                        else if (Reductor.reduce(q, Lexicon.lex.the))
                        {
                            // "are" "the" -> "are"
                        }

                        // "are" "noun" -> "noun"
                        Reductor.reduceToRight(q, ref token, POS.pos.NOUN);

                        return new Lexicon.Token(token);

                    case Lexicon.lex.ascend:
                        // "ascend" "to "the" --> "ascend"
                        Reductor.reduce(q, Lexicon.lex.to, Lexicon.lex.the);

                        // "ascend" "left|right|straight|up|down|forward|back" -> "<direction>"
                        Reductor.reduceToRight(q, ref token, POS.pos.DIRECTION);
                        return new Lexicon.Token(token);

                    case Lexicon.lex.back:
                        token = Lexicon.lex.Return;
                        useCurrentToken = true;
                        continue;

                    case Lexicon.lex.can:                   
                        if (Reductor.reduce(q, Lexicon.lex.i, Lexicon.lex.have, Lexicon.lex.a, Lexicon.lex.hint))
                        {
                            token = Lexicon.lex.hint;
                            useCurrentToken = true;
                            continue;
                        }

                        // "can" "I|we|you" "see|hear|smell" -> "see|hear|smell"
                        if (Reductor.reduceToRight(q, ref token, POS.pos.PERSONAL_PRON, POS.pos.SENSE_VERB))
                        {
                            useCurrentToken = true;
                            continue;
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.carry:
                        if (Reductor.reduce(q, Lexicon.lex.on))
                        {
                            // "carry" "on" -> "go"
                            token = Lexicon.lex.go;
                            useCurrentToken = true;
                            continue;
                        }
                        else if (Reductor.reduceToRight(q, ref token, POS.pos.DIRECTION))
                        {
                            // "carry" "<direction>" -> "<direction>"
                            token = Lexicon.lex.go;
                            useCurrentToken = true;
                            continue;
                        }

                        return new Lexicon.Token(token);

                    case Lexicon.lex.climb:
                        if (Reductor.reduceToRight(q, ref token, POS.pos.DIRECTION))
                        {
                            // "climb" "<direction>" -> "<direction>"
                            useCurrentToken = true;
                        }
                        else
                        {
                            // "climb" -> "ascend"
                            token = Lexicon.lex.ascend;
                            useCurrentToken = true;
                        }
                        continue;

                    case Lexicon.lex.close:
                        if (Reductor.reduce(q, Lexicon.lex.to, Lexicon.lex.This, Lexicon.lex.room))
                        {
                            m_roomLikeWords = true;
                            return new Lexicon.Token(Lexicon.lex.near);
                        }
                        if (Reductor.reduce(q, Lexicon.lex.by, Lexicon.lex.here))
                        {
                            return new Lexicon.Token(Lexicon.lex.near);
                        }
                        if (Reductor.reduce(q, Lexicon.lex.to, Lexicon.lex.here))
                        {
                            return new Lexicon.Token(Lexicon.lex.near);
                        }
                        if (Reductor.reduce(q, Lexicon.lex.by))
                        {
                            return new Lexicon.Token(Lexicon.lex.near);
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.collect:
                        token = Lexicon.lex.take;
                        useCurrentToken = true;
                        continue;

                    case Lexicon.lex.descend:
                        // "descend" "to "the" --> "descend"
                        Reductor.reduce(q, Lexicon.lex.to, Lexicon.lex.the);

                        // "descend" "left|right|straight" -> "left|right|straight"
                        Reductor.reduceToRight(q, ref token, POS.pos.DIRECTION);
                        return new Lexicon.Token(token);

                    case Lexicon.lex.Do:
                        // "do" "I|we|you" "see|hear|smell" -> "see|hear|smell"
                        if (Reductor.reduceToRight(q, ref token, POS.pos.PERSONAL_PRON, POS.pos.SENSE_VERB))
                        {
                            useCurrentToken = true;
                            continue;
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.down:
                        token = Lexicon.lex.descend;
                        useCurrentToken = true;
                        continue;

                    case Lexicon.lex.enter:
                        if (Reductor.reduce(q, Lexicon.lex.into, Lexicon.lex.the))
                        {
                            token = Lexicon.lex.go;
                            useCurrentToken = true;
                            continue;
                        }
                        if (Reductor.reduce(q, Lexicon.lex.the))
                        {
                            token = Lexicon.lex.go;
                            useCurrentToken = true;
                            continue;
                        }
                        token = Lexicon.lex.go;
                        useCurrentToken = true;
                        continue;

                    case Lexicon.lex.everything:
                        if (Reductor.reduce(q, Lexicon.lex.around, Lexicon.lex.here))
                        {
                            return new Lexicon.Token(Lexicon.lex.all);
                        }
                        if (Reductor.reduce(q, Lexicon.lex.from, Lexicon.lex.here))
                        {
                            return new Lexicon.Token(Lexicon.lex.all);
                        }
                        return new Lexicon.Token(Lexicon.lex.all);

                    case Lexicon.lex.gather:
                        token = Lexicon.lex.take;
                        useCurrentToken = true;
                        continue;

                    case Lexicon.lex.give:
                        if (Reductor.reduce(q, Lexicon.lex.a, Lexicon.lex.hint))
                        {
                            return new Lexicon.Token(Lexicon.lex.hint);
                        }
                        if (Reductor.reduce(q, Lexicon.lex.my, Lexicon.lex.score))
                        {
                            return new Lexicon.Token(Lexicon.lex.score);
                        }
                        if (Reductor.reduce(q, Lexicon.lex.me))
                        {
                            useCurrentToken = true;
                            continue;
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.go:
                        if (Reductor.reduce(q, Lexicon.lex.into, Lexicon.lex.the))
                        {
                            // "go" "into "the" --> "go"
                        }
                        else if (Reductor.reduce(q, Lexicon.lex.to, Lexicon.lex.the))
                        {
                            // "go" "to "the" --> "go"
                        }
                        if (Reductor.reduce(q, Lexicon.lex.through, Lexicon.lex.the))
                        {
                        }

                        // "go" "left|right|straight" -> "left|right|straight"
                        if (Reductor.reduceToRight(q, ref token, POS.pos.DIRECTION))
                        {
                            useCurrentToken = true;
                            continue;
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.got:
                        if (Reductor.reduce(q, Lexicon.lex.In, Lexicon.lex.my, Lexicon.lex.possession))
                        {
                            return new Lexicon.Token(token);
                        }
                        if (Reductor.reduce(q, Lexicon.lex.on, Lexicon.lex.my, Lexicon.lex.possession))
                        {
                            return new Lexicon.Token(token);
                        }
                        if (Reductor.reduce(q, Lexicon.lex.on, Lexicon.lex.me))
                        {
                            return new Lexicon.Token(token);
                        }
                        if (Reductor.reduce(q, Lexicon.lex.with, Lexicon.lex.me))
                        {
                            return new Lexicon.Token(token);
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.head:
                        if (Reductor.reduce(q, Lexicon.lex.into, Lexicon.lex.the))
                        {
                            // "head" "into" "the" -> "head"
                        }
                        if (Reductor.reduce(q, Lexicon.lex.to, Lexicon.lex.the))
                        {
                            // "head" "to" "the" -> "head"
                        }
                        else 
                        // "head" <direction> -> "<direction>"
                        if (Reductor.reduceToRight(q, ref token, POS.pos.DIRECTION))
                        {
                            useCurrentToken = true;
                            continue;
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.hear:
                        if (Reductor.reduceToLeft(q, POS.pos.INDEFINITE_ADVERB))
                        {
                            useCurrentToken = true;
                            continue;
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.i:
                        if (Reductor.reduce(q, Lexicon.lex.m))
                        {
                            token = Lexicon.lex.im;
                            useCurrentToken = true;
                            continue;
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.In:
                        // "in" "the|this" "<location>" --> "<location>"
                        if (Reductor.reduceToRight(q, ref token, POS.pos.THE_OR_THIS_WORD, POS.pos.LOCATION))
                        {
                            return new Lexicon.Token(Lexicon.lex.here);
                        }

                        // "in" "here" -> "here"
                        if (Reductor.reduce(q, Lexicon.lex.here))
                        {
                            return new Lexicon.Token(Lexicon.lex.here);
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.Is:
                        // "is there" -> "is"
                        Reductor.reduce(q, Lexicon.lex.there);

                        // "is" "a|an|the" <noun> -> <noun>
                        Reductor.reduceToRight(q, ref token, POS.pos.SINGLE_DETERMINER, POS.pos.NOUN);

                        return new Lexicon.Token(token);

                    case Lexicon.lex.left:
                    case Lexicon.lex.right:
                        if (Reductor.reduce(q, Lexicon.lex.hand, Lexicon.lex.side))
                        {
                            // "left|right" "hand" "side" -> "left"
                        }
                        else if (Reductor.reduce(q, Lexicon.lex.hand))
                        {
                            // "left|right" "side" -> "left
                        }
                        return Reductor.reduceLocation(q, token);

                    case Lexicon.lex.listen:
                        token = Lexicon.lex.hear;
                        useCurrentToken = true;
                        continue;

                    case Lexicon.lex.look:
                        if (Reductor.reduceToLeft(q, POS.pos.INDEFINITE_ADVERB))
                        {
                            useCurrentToken = true;
                            continue;
                        }
                        // "look" "whats" -> "whats"
                        if (Reductor.reduce(q, Lexicon.lex.whats))
                        {
                            token = Lexicon.lex.look;
                            useCurrentToken = true;
                            continue;
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.moving:
                        // "moving" "around" -> "moving"
                        Reductor.reduce(q, Lexicon.lex.around);
                        return new Lexicon.Token(token);

                    case Lexicon.lex.near:
                        if (Reductor.reduce(q, Lexicon.lex.here))
                        {
                            return new Lexicon.Token(token);
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.nearby:
                        if (Reductor.reduce(q, Lexicon.lex.here))
                        {
                            return new Lexicon.Token(Lexicon.lex.near);
                        }
                        return new Lexicon.Token(Lexicon.lex.near);

                    case Lexicon.lex.no:
                        if (Reductor.reduceToRight(q, ref token, POS.pos.MOVE_VERB))
                        {
                            useCurrentToken = true;
                            continue;
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.okay:
                    case Lexicon.lex.ok:
                        if (Reductor.reduceToRight(q, ref token, POS.pos.MOVE_VERB))
                        {
                            useCurrentToken = true;
                            continue;
                        }
                        // ok "where|when|how"
                        if (Reductor.reduce(q, Lexicon.lex.where))
                        {
                            token = Lexicon.lex.where;
                            useCurrentToken = true;
                            continue;
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.pass:
                        token = Lexicon.lex.go;
                        useCurrentToken = true;
                        continue;

                    case Lexicon.lex.pick:
                        if (Reductor.reduce(q, Lexicon.lex.up))
                        {
                            token = Lexicon.lex.take;
                            useCurrentToken = true;
                            continue;
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.please:
                        m_polite++;
                        if (Reductor.reduce(q, Lexicon.lex.could, Lexicon.lex.you))
                        {
                            continue;
                        }
                        if (Reductor.reduce(q, Lexicon.lex.would, Lexicon.lex.you))
                        {
                            continue;
                        }                      
                        continue;

                    case Lexicon.lex.previous:
                        token = Lexicon.lex.Return;
                        useCurrentToken = true;
                        continue;

                    case Lexicon.lex.retreat:
                        if (Reductor.reduce(q, Lexicon.lex.to, Lexicon.lex.the))
                        {
                            token = Lexicon.lex.Return;
                            useCurrentToken = true;
                            continue;
                        }
                        token = Lexicon.lex.Return;
                        useCurrentToken = true;
                        continue;

                    case Lexicon.lex.Return:
                        // "return" "back" -> "return"
                        Reductor.reduce(q, Lexicon.lex.back);

                        if (Reductor.reduceToLeft(q, Lexicon.lex.to, POS.pos.THE_WORD))
                        {
                            // "return" "to" "my|the|our|your" -> "return"
                        }
                        else if (Reductor.reduce(q, Lexicon.lex.to, Lexicon.lex.the))
                        {
                            // "return" "to" "the" -> "return"
                        }
                        else if (Reductor.reduce(q, Lexicon.lex.to))
                        {
                            // "return" "to" -> "return"
                        }

                        if (Reductor.reduce(q, Lexicon.lex.last))
                        {
                            // "return" .. "last" -> "return"
                        }
                        else if (Reductor.reduce(q, Lexicon.lex.previous))
                        {
                            // "return" .. "previous" -> "return"
                        }

                        return Reductor.reduceLocation(q, token);

                    case Lexicon.lex.run:
                    case Lexicon.lex.walk:
                        token = Lexicon.lex.go;
                        useCurrentToken = true;
                        continue;

                    case Lexicon.lex.see:
                        // Always convert "see" to "look".
                        token = Lexicon.lex.look;
                        useCurrentToken = true;
                        continue;

                    case Lexicon.lex.smell:
                        if (Reductor.reduceToLeft(q, POS.pos.INDEFINITE_ADVERB))
                        {
                            useCurrentToken = true;
                            continue;
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.smells:
                        token = Lexicon.lex.smell;
                        useCurrentToken = true;
                        continue;

                    case Lexicon.lex.sounds:
                        token = Lexicon.lex.sound;
                        useCurrentToken = true;
                        continue;

                    case Lexicon.lex.straight:
 
                        if (Reductor.reduce(q, Lexicon.lex.onto, Lexicon.lex.next))
                        {
                            // "straight" "onto" "next" -> "straight"
                        }
                        else if (Reductor.reduce(q, Lexicon.lex.on))
                        {
                            // "straight" "on" -> "straight"
                        }
                    
                        if (Reductor.reduceToRight(q, ref token, POS.pos.DIRECTION))
                        {
                            // "straight" "<direction>" -> "direction"
                            useCurrentToken = true;
                            continue;
                        }

                        if (Reductor.reduce(q, Lexicon.lex.into))
                        {
                            // "<direction>" "into" -> "<direction>"
                        }
                        else if (Reductor.reduce(q, Lexicon.lex.onto))
                        {
                            // "<direction>" "onto" -> "<direction>"
                        }
                        else if (Reductor.reduce(q, Lexicon.lex.to))
                        {
                            // "<direction>" "to" -> "<direction>"
                        }

                        return Reductor.reduceLocation(q, token);

                    case Lexicon.lex.tell:
                        if (Reductor.reduce(q, Lexicon.lex.me))
                        {
                            continue;
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.thank:
                        if (Reductor.reduce(q, Lexicon.lex.you))
                        {
                            m_polite++;
                            continue;
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.that:
                        // "that" "is" -> "thats"
                        if (Reductor.reduce(q, Lexicon.lex.Is))
                        {
                            token = Lexicon.lex.thats;
                            useCurrentToken = true;
                            continue;
                        }
                        if (Reductor.reduceToRight(q, ref token, POS.pos.NOUN))
                        {
                            useCurrentToken = true;
                            continue;
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.the:
                    case Lexicon.lex.This:
                        if (Reductor.reduceToRight(q, ref token, POS.pos.NOUN))
                        {
                            useCurrentToken = true;
                            continue;
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.thats:
                        // "thats" "here|around|about" -> "here|about|around"
                        if (Reductor.reduceToRight(q, ref token, POS.pos.GENERAL_PLACE))
                        {
                            useCurrentToken = true;
                            continue;
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.up:
                        token = Lexicon.lex.ascend;
                        useCurrentToken = true;
                        continue;

                    case Lexicon.lex.what:
                        // "what" "can|do" "I|we|you" --> "what"
                        if (Reductor.reduceToLeft(q, POS.pos.CAN_OR_DO_WORD, POS.pos.PERSONAL_PRON))
                        {
                            useCurrentToken = true;
                            continue;
                        }

                        // "what" "am|are" "I|we|you" --> "what"
                        if (Reductor.reduceToLeft(q, POS.pos.AM_OR_ARE_WORD, POS.pos.PERSONAL_PRON))
                        {
                            useCurrentToken = true;
                            continue;
                        }

                        // "what" "have" "I|we|you" --> "what"
                        if (Reductor.reduceToLeft(q, Lexicon.lex.have, POS.pos.PERSONAL_PRON))
                        {
                            useCurrentToken = true;
                            continue;
                        }
                        // "what" "are" -> "whats"
                        if (Reductor.reduce(q, Lexicon.lex.are))
                        {
                            token = Lexicon.lex.whats;
                            useCurrentToken = true;
                            continue;
                        }
                        // "what" "is" -> "whats"
                        if (Reductor.reduce(q, Lexicon.lex.Is))
                        {
                            token = Lexicon.lex.whats;
                            useCurrentToken = true;
                            continue;
                        }
                        // "what" "s" -> "whats"
                        if (Reductor.reduce(q, Lexicon.lex.s))
                        {
                            token = Lexicon.lex.whats;
                            useCurrentToken = true;
                            continue;
                        }
                        // "what" "smell|hear|see" -> "smell|hear|see"
                        if (Reductor.reduceToRight(q, ref token, POS.pos.SENSE_VERB))
                        {
                            useCurrentToken = true;
                            continue;
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.whats:
                        if (Reductor.reduce(q, Lexicon.lex.my))
                        {
                            // "whats" "my" -> "whats"
                        }
                        else if (Reductor.reduceToRight(q, ref token, POS.pos.GENERAL_PLACE))
                        {
                            // "whats" "here|around|about" -> "here|about|around"
                            useCurrentToken = true;
                            continue;
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.where:
                        if (Reductor.reduce(q, Lexicon.lex.Is))
                        {
                            // "where" "is" -> "wheres"
                            token = Lexicon.lex.wheres;
                            useCurrentToken = true;
                            continue;
                        }
                        if (Reductor.reduce(q, Lexicon.lex.are))
                        {
                            // "where" "are" -> "wheres"
                            token = Lexicon.lex.wheres;
                            useCurrentToken = true;
                            continue;
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.wheres:
                        if (Reductor.reduce(q, Lexicon.lex.the))
                        {
                            // "wheres" "the" -> "wheres"
                        }
                        else if (Reductor.reduce(q, Lexicon.lex.a))
                        {
                            // "wheres" "a" -> "wheres"
                        }
                        else if (Reductor.reduce(q, Lexicon.lex.an))
                        {
                            // "wheres" "an" -> "wheres"
                        }
                        return new Lexicon.Token(token);

                    case Lexicon.lex.unknown:
                        Log.Debug(MainActivity.TAG, "Unknown token {0} encountered during compression", raw);
                        return new Lexicon.Token(token, raw);

                    default:               
                        return new Lexicon.Token(token);
                }
            }
            return new Lexicon.Token(Lexicon.lex.eotext);
        }

        public bool isPolite()
        {
            return (m_polite > 0);
        }

        public bool saidCaveLikeWords()
        {
            return m_caveLikeWords;
        }

        public bool saidRoomLikeWords()
        {
            return m_roomLikeWords;
        }

        public bool saidStairLikeWords()
        {
            return m_stairLikeWords;
        }

        public bool saidTunnelLikeWords()
        {
            return m_tunnelLikeWords;
        }

        public LinkedList<Lexicon.Token> getTokens()
        {
            return m_tokens;
        }

        public static string getString(LinkedList<Lexicon.Token> tokens)
        {
            string strippedCommand = null;
            bool firstWord = true;
            foreach (var token in tokens)
            {
                if (!firstWord)
                {
                    strippedCommand += " ";
                }
                strippedCommand += Lexicon.getWord(token);
                firstWord = false;
            }

            Log.Debug(MainActivity.TAG, "Tokenised: {0} words: {1}", tokens.Count, strippedCommand);
            return strippedCommand;
        }
    }
}