namespace wadtt
{
    public class Tide
    {
        public enum Sea { dry, damp, kneeLevel, waistLevel, neckLevel, dead};

        public static Sea getSeaLevel(int roomLevel)
        {
            int tide = getTide(false);

            if (roomLevel > tide)
            {
                return Sea.dry;
            }

            if (roomLevel == tide)
            {
                return Sea.damp;
            }

            if ((roomLevel - 1) == tide)
            {
                return Sea.kneeLevel;
            }

            if ((roomLevel - 2) == tide)
            {
                return Sea.waistLevel;
            }

            if ((roomLevel - 3) == tide)
            {
                return Sea.neckLevel;
            }

            return Sea.dead;
        }

        public static string getTidalText(int level)
        {
            int tide = getTide(false);

            if (level > tide)
            {
                string[] dry = { "On the dry floor ", "On the dusty floor", "On the uneven floor" };
                return Utils.getAny(dry);
            }

            if (level == tide)
            {
                string[] wet = { "On the damp ground ", "The floor is wet and you see ",
                                 "On the wet floor ", "The floor glistens with shallow water " };
                return Utils.getAny(wet);
            }

            if ((level - 1) == tide)
            {
                string[] knee = { "Water stands knee-high in this cave.",
                                  "You wade through knee-high water", "The water is deep and soaks your jeans " };
                return Utils.getAny(knee);
            }

            if ((level - 2) == tide)
            {
                string[] waist = { "Water stands waist-high in this cave.", "You swim through waist high water.",
                                  "You wade through waist-high water", "The water is deep and soaks you through " };
                return Utils.getAny(waist);
            }

            if ((level - 3) == tide)
            {
                string[] neck = { "Water stands neck-high in this cave.",
                                  "You wade through neck-high water", "You swim through deep cold water " };
                return Utils.getAny(neck);
            }

            // Do we lose our possessions at this point? (Maybe they sink down a level?
            return "You must swim for your life.";
        }

        static int cycle = 0;
        // 4.. 3.. 2.. 1.. 0.. 0.. 0.. 1...1... 2... 3... 4

        public static int checkTide()
        {
            return getTide(false);
        }

        public static int moveTide()
        {
            return getTide(true);
        }
 
        public static int getTide(bool change)
        {
            if (change)
            {
                cycle++;
            }

            if (cycle < 4)
            {
                return 4;
            }

            if (cycle < 7)
            {
                return 3;
            }

            if (cycle < 10)
            {
                return 2;
            }

            if (cycle < 13)
            {
                return 1;
            }

            if (cycle < 22)
            {
                return 0;
            }

            if (cycle < 26)
            {
                return 1;
            }

            if (cycle < 30)
            {
                return 2;
            }

            if (cycle < 34)
            {
                return 3;
            }

            if (cycle == 38)
            {
                cycle = 0;
            }
            return 4;
        }
    }
}