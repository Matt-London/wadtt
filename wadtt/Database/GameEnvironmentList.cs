using Android.Util;
using SQLite;
using System;
using System.Collections.Generic;

namespace wadtt
{
    public class EnvironmentNames
    {
        public string EnvName { get; set; }
    }

    public class GameEnvironmentList
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public int GameListID { get; set; }
        public int EnvironmentListID { get; set; }

        public GameEnvironmentList()
        {
        }

        public GameEnvironmentList(GameEnvironmentList gameEnvironment)
        {
            // Classic! there is no copy constructor in C#!!!
            ID = gameEnvironment.ID;
            GameListID = gameEnvironment.GameListID;
            EnvironmentListID = gameEnvironment.EnvironmentListID;
        }

        public static int GetTotalCount()
        {
            return Database.GetTotalCount("GameEnvironmentList");

        }

        public static List<string> GetEnvironmentNames(string gameName)
        {
            try
            {
                if (Database.db == null)
                {
                    return null;
                }

                string sql =
                    "SELECT " +
                    "  EnvironmentList.Name as EnvName " +
                    "FROM " +
                    "  GameList " +
                    "INNER JOIN EnvironmentList ON " +
                    "  GameEnvironmentList.EnvironmentListID = EnvironmentList.ID " +
                    "INNER JOIN GameEnvironmentList ON " +
                    "  GameEnvironmentList.GameListID = GameList.ID " +
                    "WHERE " +
                    "  GameList.Name = '" + gameName + "'";
                object[] args = { };

                List<EnvironmentNames> envNames = Database.db.Query<EnvironmentNames>(sql);
                List<string> names = new List<string>();
                foreach (var entry in envNames)
                {
                    names.Add(entry.EnvName);
                }
                return names;
            }
            catch (SQLiteException ex)
            {
                Log.Debug(MainActivity.TAG, ex.Message);
                return null;
            }
        }

        public static GameEnvironmentList GetRow(int id)
        {
            string sql = "SELECT * FROM GameEnvironmentList WHERE ID=?";
            object[] args = { id };

            return GetRow(sql, args);
        }

        private static GameEnvironmentList GetRow(string sql, object args)
        {
            try
            {
                if (Database.db == null)
                {
                    return null;
                }

                List<GameEnvironmentList> items = null;

                if (args == null)
                {
                    items = Database.db.Query<GameEnvironmentList>(sql);
                }
                else
                {
                    items = Database.db.Query<GameEnvironmentList>(sql, args);
                }

                if (items.Count == 0)
                {
                    Log.Error(MainActivity.TAG, "Error: No GameEnvironmentList entries");
                    return null;
                }
                return items[0];
            }
            catch (SQLiteException ex)
            {
                Log.Debug(MainActivity.TAG, ex.Message);
                return null;
            }
        }
    }
}