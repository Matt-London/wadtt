using Android.Content.Res;

namespace wadtt
{
    public interface IDatabase
    {
        Resources getResources();
    }
}