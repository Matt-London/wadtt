using Android.Util;
using SQLite;
using System;
using System.Collections.Generic;

namespace wadtt
{
    public class CreatureList
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Movement { get; set; }

        public CreatureList()
        {
        }

        public CreatureList(string name)
        {
        }

        public CreatureList(CreatureList creature)
        {
            // Classic! there is no copy constructor in C#!!!
            ID = creature.ID;
            Name = creature.Name;
            Movement = creature.Movement;
        }

        public static int GetTotalCount()
        {
            return Database.GetTotalCount("CreatureList");

        }

        public static List<Creature> GetAvailableCreatures(string envName)
        {
            // Actually want to return a list of creature information.
            try
            {
                if (Database.db == null)
                {
                    return null;
                }

                string sql =
                    "SELECT" +
                    "  EnvironmentCreatureList.ID as ID," +
                    "  CreatureList.Name as Name," +
                    "  EnvironmentCreatureList.Left as Left," +
                    "  EnvironmentCreatureList.Right as Right," +
                    "  EnvironmentCreatureList.Straight as Straight," +
                    "  EnvironmentCreatureList.Back as Back," +
                    "  EnvironmentCreatureList.StartRoom as StartRoom," +
                    "  EnvironmentCreatureList.Delay as Delay" +
                    " FROM" +
                    "  EnvironmentCreatureList" +
                    " INNER JOIN EnvironmentList ON" +
                    "  EnvironmentCreatureList.EnvironmentListID = EnvironmentList.ID" +
                    " INNER JOIN CreatureList ON" +
                    "  EnvironmentCreatureList.CreatureListID = CreatureList.ID" +
                    " WHERE" +
                    "  EnvironmentList.Name = '" + envName + "'";
                object[] args = { };

                List<Creature> creatures = Database.db.Query<Creature>(sql);
                return creatures;
            }
            catch (SQLiteException ex)
            {
                Log.Debug(MainActivity.TAG, ex.Message);
                return null;
            }
        }

        public static CreatureList GetRow(int id)
        {
            string sql = "SELECT * FROM CreatureList WHERE ID=?";
            object[] args = { id };

            return GetRow(sql, args);
        }

        public static CreatureList GetRow(string name)
        {
            string sql = "SELECT * FROM CreatureList WHERE Name='" + name + "'";
            return GetRow(sql, null);
        }

        private static CreatureList GetRow(string sql, object args)
        {
            try
            {
                if (Database.db == null)
                {
                    return null;
                }

                List<CreatureList> creatures = null;

                if (args == null)
                {
                    creatures = Database.db.Query<CreatureList>(sql);
                }
                else
                {
                    creatures = Database.db.Query<CreatureList>(sql, args);
                }

                if (creatures.Count == 0)
                {
                    Log.Error(MainActivity.TAG, "Oops we have no CreatureList entries");
                    return null;
                }
                return creatures[0];
            }
            catch (SQLiteException ex)
            {
                Log.Debug(MainActivity.TAG, ex.Message);
                return null;
            }
        }
    }
}