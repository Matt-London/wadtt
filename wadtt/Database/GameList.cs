using Android.Util;
using SQLite;
using System;
using System.Collections.Generic;

namespace wadtt
{
    public class GameList
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Name { get; set; }

        public GameList()
        {
        }

        public GameList(string name)
        {
        }

        public GameList(GameList gameEntry)
        {
            // Classic! there is no copy constructor in C#!!!
            ID = gameEntry.ID;
            Name = gameEntry.Name;
        }

        public static int GetTotalCount()
        {
            return Database.GetTotalCount("GameList");
        }

        public static GameList GetRow(int id)
        {
            string sql = "SELECT * FROM GameList WHERE ID=?";
            object[] args = { id };

            return GetRow(sql, args);
        }

        public static GameList GetRow(string name)
        {
            string sql = "SELECT * FROM GameList WHERE Name='" + name + "'";
            return GetRow(sql, null);
        }

        private static GameList GetRow(string sql, object args)
        {
            try
            {
                if (Database.db == null)
                {
                    return null;
                }

                List<GameList> items = null;

                if (args == null)
                {
                    items = Database.db.Query<GameList>(sql);
                }
                else
                {
                    items = Database.db.Query<GameList>(sql, args);
                }

                if (items.Count == 0)
                {
                    Log.Error(MainActivity.TAG, "Oops we have no GameList entries");
                    return null;
                }
                return items[0];
            }
            catch (SQLiteException ex)
            {
                Log.Debug(MainActivity.TAG, ex.Message);
                return null;
            }
        }
    }
}