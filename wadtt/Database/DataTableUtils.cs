using System;
using System.Collections.Generic;

// Share between unit test and real life.
namespace wadtt
{
    public class DataTableUtils
    {
        public delegate List<string> GetEnvNames(string gameName);
        public static List<string> GetEnvironmentNames(GetEnvNames getEnvironmentNames, string gameName)
        {
            List<string> envNames = getEnvironmentNames(gameName);
            return envNames;
        }
    }
}