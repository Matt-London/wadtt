using Android.Util;
using SQLite;
using System;
using System.Collections.Generic;

namespace wadtt
{
    public class EnvironmentList
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Name { get; set; }
        // Effectively defines the scope of the rooms.
        // Currently rooms are statically dependent .. we don't
        // dynamically create an Environment from a pool of rooms.
        public int RoomStartID { get; set; }
        public int RoomEndID { get; set; }
        public string Players { get; set; }
        public string Puzzles { get; set; }
        public string Rules { get; set; }
        public string Weather { get; set; }

        public EnvironmentList()
        {
        }

        public EnvironmentList(string name)
        {
        }

        public EnvironmentList(EnvironmentList environment)
        {
            // Classic! there is no copy constructor in C#!!!
            ID = environment.ID;
            Name = environment.Name;
            RoomStartID = environment.RoomStartID;
            RoomEndID = environment.RoomEndID;
            Players = environment.Players;
            Puzzles = environment.Puzzles;
            Rules = environment.Rules;
            Weather = environment.Weather;
        }

        public static int GetTotalCount()
        {
            return Database.GetTotalCount("EnvironmentList");
            
        }

        public static EnvironmentList GetRow(int id)
        {
            string sql = "SELECT * FROM EnvironmentList WHERE ID=?";
            object[] args = { id };

            return GetRow(sql, args);
        }

        public static EnvironmentList GetRow(string name)
        {
            string sql = "SELECT * FROM EnvironmentList WHERE Name='" + name + "'";
            return GetRow(sql, null);
        }

        private static EnvironmentList GetRow(string sql, object args)
        {
            try
            {
                if (Database.db == null)
                {
                    return null;
                }

                List<EnvironmentList> items = null;

                if (args == null)
                {
                    items = Database.db.Query<EnvironmentList>(sql);
                }
                else
                {
                    items = Database.db.Query<EnvironmentList>(sql, args);
                }

                if (items.Count == 0)
                {
                    Log.Error(MainActivity.TAG, "Oops we have no EnvironmentList entries");
                    return null;
                }
                return items[0];
            }
            catch (SQLiteException ex)
            {
                Log.Debug(MainActivity.TAG, ex.Message);
                return null;
            }
        }
    }
}