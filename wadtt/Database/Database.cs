using System;
using System.Collections.Generic;
using Android.Content;
using Android.Util;
using SQLite;
using System.IO;
using Android.Content.Res;
using System.Resources;

// For now I just go synchronous on SQLite. Worry about Async another time.
// http://stackoverflow.com/questions/2493331/what-are-the-best-practices-for-sqlite-on-android
// ://msdn.microsoft.com/en-us/library/mt674882.aspx
namespace wadtt
{
    public class Database : IDatabase
    {
        public static SQLiteConnection db = null;

        private readonly Resources m_resources;

        public Resources getResources()
        {
            return m_resources;
        }

        public Database(string roomsFile, Resources resources)
        {
            m_resources = resources;  
            try
            {
                string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                string path = System.IO.Path.Combine(folder, roomsFile);

                // We read from the .apk resources into our personal storage and then open.
                // Shame we can't simply stream into sqlite!!
                // FIXME: Handle persistence of data.
                if (!System.IO.File.Exists(path))
                {
                    // FIXME: This code is a bit of a pain when we need to update the database.
                    // Since an old copy will be located every time.
                    var s = resources.OpenRawResource(Resource.Raw.game);
                    FileStream writeStream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
                    ReadWriteStream(s, writeStream);
                    Log.Debug(MainActivity.TAG, "Loaded new database");
                }
                else
                {
                    Log.Debug(MainActivity.TAG, "Using existing database");
                }
                db = new SQLiteConnection(path);
            }
            catch (SQLiteException ex)
            {
                Log.Error(MainActivity.TAG, ex.Message);
            }
        }

        private void ReadWriteStream(Stream readStream, Stream writeStream)
        {
            int Length = 256;
            Byte[] buffer = new Byte[Length];
            int bytesRead = readStream.Read(buffer, 0, Length);
            while (bytesRead > 0)
            {
                writeStream.Write(buffer, 0, bytesRead);
                bytesRead = readStream.Read(buffer, 0, Length);
            }
            readStream.Close();
            writeStream.Close();
        }

        public static int GetTotalCount(string tableName)
        {
            try
            {
                if (db == null)
                {
                    return 0;
                }

                string sql = "SELECT Count(*) FROM " + tableName;
                int result = db.ExecuteScalar<int>(sql);
                return result;
            }
            catch (SQLiteException ex)
            {
                Log.Error(MainActivity.TAG, ex.Message);
                return 0;
            }
        }
    }
}