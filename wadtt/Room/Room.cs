using SQLite;
using System;
using System.Collections.Generic;
using Android.Util;

namespace wadtt
{
    public class Room : IRoom
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Desc { get; set; }
        public int Level { get; set; }  // 0 is low-water level. Tides may vary.     
        public Sound.Noise Sound { get; set; }
        public int Left { get; set; }
        public int Straight { get; set; }
        public int Right { get; set; }
        public string Contents { get; set; }
        public string Floor { get; set; }
        public Rooms.Style RoomStyle { get; set; }

        public Room() { }   // Needed for DB creation.

        public static int GetRoomCount()
        {
            return Database.GetTotalCount("Room");
        }

        public string getContents()
        {
            string contents = "";
            if (Contents != null && Contents.Length !=0)
            {
                string[] tokens = Contents.Split(',');

                int[] roomItems = Array.ConvertAll<string, int>(tokens, int.Parse);
                foreach (int value in roomItems)
                {
                    Item item = Items.GetItemById(value);
                    if (contents.Length > 0)
                    {
                        contents += ", ";
                    }
                    contents += item.Name;
                }
            }
            return contents;
        }

        public string getRawContents()
        {
            return Contents;
        }

        // FIXME: This will become more intelligent based on what we carry and tides..
        public int getTunnelCount()
        {
            int count = 0;
            if (Left > 0)
            {
                count++;
            }

            if (Straight > 0)
            {
                count++;
            }

            if (Right > 0)
            {
                count++;
            }

            return count;
        }

        public string getLeading()
        {
            int tunnels = getTunnelCount();
            if (tunnels == 0)
            {
                return "Oh dear there are no exits; you appear to be in a dead end.";
            }

            string leading;

            string direction = "";
            if (Left > 0)
            {
                int gradient = Rooms.getGradient(ID, Left);
                if (gradient == 0)
                {
                    direction = "left";
                }
                else if (gradient > 0)
                {
                    direction = "up to the left";
                }
                else
                {
                    direction = "down to the left";
                }
            }

            if (Right > 0)
            {
                int gradient = Rooms.getGradient(ID, Right);
                string dir = "";
                if (gradient == 0)
                {
                    dir = "right";
                }
                else if (gradient > 0)
                {
                    dir = "up to the right";
                }
                else
                {
                    dir = "down to the right";
                }

                if (tunnels == 1)
                {
                    direction = dir;
                }
                else if (tunnels == 2)
                {
                    direction += (Left > 0) ? ("and " + dir) : dir;
                }
                else if (tunnels == 3)
                {
                    direction += (", " + dir);
                }

            }

            if (Straight > 0)
            {
                int gradient = Rooms.getGradient(ID, Straight);
                string dir = "";
                if (gradient == 0)
                {
                    dir = "straight on";
                }
                else if (gradient > 0)
                {
                    dir = "straight up";
                }
                else
                {
                    dir = "straight down";
                }
                direction += (tunnels == 1) ? dir : (" and " + dir);
            }

            leading = ((tunnels > 1) ? "are passages" : "is a tunnel") + " leading " + direction;

            return leading;
        }

        public static Room GetRoomById(int id)
        {
            try
            {
                string sql = "SELECT * FROM Room WHERE ID=?";
                object[] args = { id };
                List<Room> rooms = Database.db.Query<Room>(sql, args);
                if (rooms.Count == 0)
                {
                    Log.Error(MainActivity.TAG, "Oops we have no room number " + id);
                    return null;
                }
                return rooms[0];
            }
            catch (SQLiteException ex)
            {
                Log.Debug(MainActivity.TAG, ex.Message);
                return null;
            }
        }
    }
}