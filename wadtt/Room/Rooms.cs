using Android.Util;
using System.Collections.Generic;

namespace wadtt
{
    public class Rooms
    {

        public enum Movement { left, right, straight, back };
        public enum Style { cave, room, outside };

        // roomCondition can be anything from goblin or cat in a room, to room is roomid 13.
        public delegate int roomCondition(int roomId);

        public static int InRange(int roomId, int range, roomCondition func)
        {
            int count = func(roomId);
            int newRange = range - 1;

            if (range > 0)
            {
                Room thisRoom = Room.GetRoomById(roomId);
                if (thisRoom.Left > 0)
                {
                    count += InRange(thisRoom.Left, newRange, func);
                }

                if (thisRoom.Right > 0)
                {
                    count += InRange(thisRoom.Right, newRange, func);
                }

                if (thisRoom.Straight > 0)
                {
                    count += InRange(thisRoom.Straight, newRange, func);
                }
            }

            return count;
        }

        public static Route shortestRoute(int roomId, roomCondition func)
        {
            Route route = new Route();
            return bestRoute(roomId, route, func);
        }

        public static int searchLimit = 4;
        public static Route bestRoute(int roomId, Route route, roomCondition func)
        {
            int result = func(roomId);
            if (result != 0)
            {
                // We have found a match. This is the best route this way.
                // Effectively "a leaf node"
                return route;
            }

            if (route.Count() > searchLimit)
            {
                //Log.Debug(MainActivity.TAG, "Room {0} Search limit.. returning null", roomId);
                return null;
            }

            if (route.Contains(roomId))
            {
                // We have already been here, we are looping.
                //Log.Debug(MainActivity.TAG, "Already visited room {0} returning null", roomId);
                return null;
            }

            Room thisRoom = Room.GetRoomById(roomId);
            Route bestWay = null;

            // Strip out empty routes and those that immediately cycle back to themselves.
            if ((thisRoom.Left > 0) && (thisRoom.Left != roomId))
            {
                route.saveRoom(roomId);
                Route leftRoute = new Route(route);
                leftRoute = bestRoute(thisRoom.Left, leftRoute, func);
                if (leftRoute != null)
                {
                    bestWay = new Route(leftRoute);
                }
            }

            if ((thisRoom.Right > 0) && (thisRoom.Right != roomId))
            {
                route.saveRoom(roomId);
                Route rightRoute = new Route(route);
                rightRoute = bestRoute(thisRoom.Right, rightRoute, func);
                if ((bestWay == null) || (bestWay.isEmpty()) || ((rightRoute != null) && (!rightRoute.isEmpty()) && (rightRoute.Count() < bestWay.Count())))
                {
                    bestWay = new Route(rightRoute);
                }
            }

            if ((thisRoom.Straight > 0) && (thisRoom.Straight != roomId))
            {
                route.saveRoom(roomId);
                Route straightRoute = new Route(route);
                straightRoute = bestRoute(thisRoom.Straight, straightRoute, func);
                if ((bestWay == null) || (bestWay.isEmpty()) || 
                    ((straightRoute != null) && (!straightRoute.isEmpty()) && (straightRoute.Count() < bestWay.Count())))
                {
                    bestWay = new Route(straightRoute);
                }
            }

            return bestWay;
        }

        public static int getGradient(int fromRoomId, int toRoomId)
        {
            //   0 Flat.
            // > 0 Uphill.
            // < 0 Downhill.
            Room from = Room.GetRoomById(fromRoomId);
            Room to = Room.GetRoomById(toRoomId);
            int gradient = to.Level - from.Level;

            Log.Debug(MainActivity.TAG, "{0} -> {1} gradient: {2} - {3} = {4}",
                      fromRoomId, toRoomId, to.Level, from.Level, gradient);
            return gradient;
        }

        // We can have rooms and caves (any non-room is a cave)

        private static string[] youSee = { " you see ", "you can see ", "you notice ", " " };
        private static string[] thereIs = { "", "there is a", "there's a" };
        private static string[] thereAre = { "", "there are " };
        private static string[] noSmells = { "There are no unusual smells", "You smell nothing" };
        private static string[] noSounds = { "There are no unusual sounds", "You hear nothing unusual" };

        public static string roomSmells(int roomid)
        {
            string cats = Cats.catSmells(roomid);
            string goblins = Goblins.goblinSmells(roomid);
            string spiders = Spiders.spiderSmells(roomid);
            return ((goblins != null) || (cats != null) || (spiders != null))
                   ? " " + goblins + " " + cats + " " + spiders : Utils.getAny(noSmells); ;
        }

        public static string roomSounds(int roomid)
        {
            string cats = Cats.catSounds(roomid);
            string goblins = Goblins.goblinSounds(roomid);
            string spiders = Spiders.spiderSounds(roomid);
            return ((goblins != null) || (cats != null) || (spiders != null))
                   ? " " + goblins + " " + cats + " " + spiders : Utils.getAny(noSounds);
        }

        public static string roomCreatures(IPlayer player, int roomId)
        {
            ICreatureHandler creatureHandler = player.CurrentEnvironment.getCreatureHandler();
            List<Creature> creatures = creatureHandler.inRoom(roomId);
            if (creatures.Count == 0)
            {
                return "";
            }

            string around = Utils.getAny(youSee);
            around += (creatures.Count == 1) ? Utils.getAny(thereIs) : Utils.getAny(thereAre);
            around += " " + creatures.Count + "creature";
            around += (creatures.Count == 1) ? "here." : "s here.";
            return around;
        }

        public static string roomContents(IPlayer player, int roomid)
        {
            IBaggageHandler baggageHandler = player.CurrentEnvironment.getBaggageHandler();
            List<string> items = baggageHandler.getManifest(Room.GetRoomById(roomid));
            if (items.Count == 0)
            {
                return "";
            }

            string holding = Utils.getAny(youSee);
            holding += (items.Count == 1) ? Utils.getAny(thereIs) : Utils.getAny(thereAre);
            holding += Utils.getContents(items);
            return holding; 
        }

        // FIXME: This should not be in this file..
        static string[] carry = { "You are carrying ", "You have ", "You hold ",
                                          "You are holding " };
        public static string bagContents(IPlayer player)
        {
            IBaggageHandler baggageHandler = player.CurrentEnvironment.getBaggageHandler();
            string manifest = Utils.getContents(baggageHandler.getManifest(player));
            if (manifest.Length == 0)
            {
                return "";
            }

            string holding = Utils.getAny(carry);
            holding += manifest;
            return holding;
        }

        public static Style majorRoomStyle(int fromRoomId, int toRoomId)
        {
            if (fromRoomId == 0)
            {
                // We have no prior room.
                return Room.GetRoomById(toRoomId).RoomStyle;
            }

            // If we are moving from or to a room, not a cave, that takes precedence.
            if (Room.GetRoomById(fromRoomId).RoomStyle == Style.room)
            {
                return Style.room;
            }

            if (Room.GetRoomById(toRoomId).RoomStyle == Style.room)
            {
                return Style.room;
            }

            return Room.GetRoomById(fromRoomId).RoomStyle;
        }

        //The room I am currently dictates whether we use steps or enter on via a cave tunnel.
        // A library for example will always have steps down into a cave.
        public static string getGradientText(int gradient, Rooms.Style roomStyle)
        {
            string[] caveStraight = { "stand within", "enter",  "find your way into",
                                      "walk into",    "find yourself in ", "stand in" };

            string[] caveDown = { "slide into",   "descend into",      "crawl down into",
                                  "walk down into", "slip down into",  "lower yourself into"};

            string[] caveUp = { "climb up into", "haul yourself up into", "walk up into",
                               "crawl up into", "pull yourself up into", "ascend into" };

            string[] roomStraight = { "stand within", "enter",  "find your way into ",
                                      "walk into",    "find yourself in", "stand in" };

            string[] roomDown = { "step down into",   "descend into",  "descend the steps into",
                                  "follow the stairway to", "walk down into", "walk down steps to" };

            string[] roomUp = { "step up into", "ascend into", "ascend the steps into",
                                "follow the stairs up to", "walk up into", "walk up the steps to" };

            string desc = "You ";
            if (gradient == 0)
            {
                desc += (roomStyle == Rooms.Style.cave) ? Utils.getAny(caveStraight) : Utils.getAny(roomStraight);
            }
            else if (gradient > 0)
            {
                desc += (roomStyle == Rooms.Style.cave) ? Utils.getAny(caveUp) : Utils.getAny(roomUp);
            }
            else // if (gradient < 0)
            {
                desc += (roomStyle == Rooms.Style.cave) ? Utils.getAny(caveDown) : Utils.getAny(roomDown);
            }
            return desc + " ";
        }
    }
}