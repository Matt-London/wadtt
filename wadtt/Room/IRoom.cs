namespace wadtt
{
    public interface IRoom
    {
        int ID { get; set; }
        string Desc { get; set; }
        int Level { get; set; }
        Sound.Noise Sound { get; set; }
        int Left { get; set; }
        int Straight { get; set; }
        int Right { get; set; }
        string Floor { get; set; }

        string getContents();
        string getRawContents();
        int getTunnelCount();
        string getLeading();        // Move it as it is text!!
        // Text to go in a separate file too
    }
}