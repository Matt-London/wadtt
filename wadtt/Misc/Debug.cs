using System;

namespace wadtt
{
    public class Debug
    {
        public static void assert(bool condition)
        {
            if (!condition)
            {
                throw new NullReferenceException();
            }
        }
    }
}