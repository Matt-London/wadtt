using Android.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace wadtt
{
    class Utils
    {
        public static IEnumerable<T> GetValues<T>()
        {
            // Enum.GetValues returns an Array which is not strongly-typed.
            // A call of ToList() avoids the inefficiency associated with
            // walking an IEnumerable<T> multiple times
            // produced by LINQ methods with deferred execution.
            return Enum.GetValues(typeof(T)).Cast<T>().ToList();
        }

        // Return some random string from a choice of phrases.
        static int choice = 0;
        public static string getAny(string[] list)
        {
            if ( choice >= list.Length )
            {
                choice = 0;
            }
 
            return list[choice++];
        }

        public static string getContents(List<string> manifest)
        {
            if (manifest.Count == 0)
            {
                return "";
            }

            string contents = "";
            for (int i = 0; i < manifest.Count; i++)
            {
                if ( (manifest.Count > 1) && (i > 0) )
                {
                    contents += (i < (manifest.Count - 1)) ? ", " : " and ";
                }
                contents += manifest[i];
            }

            contents += ".";
            return contents;
        }

        public static string getCountAsText(int count)
        {
            switch (count)
            {
                case 1:
                    return "a ";
                default:
                    return count.ToString() + " ";
            }
        }

        public static string getSpeed(int speed)
        {
            string hint;
            switch (speed)
            {
                case 1:
                    hint = "You step into the darkness.";
                    break;

                case 2:
                    hint = "The caves are quite warm. You certainly won't require a jumper.";
                    break;

                case 4:
                    hint = "You can hear faint footsteps behind you. But nothing to worry about.";
                    break;

                case 6:
                    hint = "The footsteps are more persistent. We are trying to find you an alternative route.";
                    break;

                case 8:
                    hint = "Umm I seriously think you need to speed up.";
                    break;

                default:
                    hint = "";
                    break;
            }
            return hint;
        }
    }
}

