using Android.Util;

namespace wadtt
{
    class Exercise
    {
        public static void ExerciseOne()
        {
            int roomCount = Room.GetRoomCount();
            for (int roomId = 2; roomId <= roomCount; roomId++)
            {
                Route r = Route.shortestRouteToRoom(1, roomId);
                if (r == null)
                {
                    Log.Debug(MainActivity.TAG, "Room 1 -> {0} : No route", roomId);
                    continue;
                }

                string routeText = r.routeAsText();
                if (routeText.Length == 0)
                {
                    Log.Debug(MainActivity.TAG, "Room 1 -> {0} : Too far", roomId);
                    continue;
                }

                Log.Debug(MainActivity.TAG, "Room 1 -> {0} : Route is {1}", roomId, routeText);
            }
        }

        public static void ExerciseTwo(int playerLocation)
        {
            string locations = Goblins.locate();
            string response = "Goblins are " + ((locations.Length > 0) ? locations : "asleep");
            Log.Debug(MainActivity.TAG, response);

            locations = Spiders.locate();
            response = "Spiders are " + ((locations.Length > 0) ? locations : "asleep");
            Log.Debug(MainActivity.TAG, response);

            locations = Cats.locate();
            response = "Cats are " + ((locations.Length > 0) ? locations : "asleep");
            Log.Debug(MainActivity.TAG, response);

            Log.Debug(MainActivity.TAG, "Player is in room {0}", playerLocation);
        }

        public static void ExerciseThree()
        {
            // Let the Goblins wander 300 times and then report:
            // Least visited, most visited rooms.
            // Any that "complete" the game.
            // Same with Spiders and cats.
        }
    }
}