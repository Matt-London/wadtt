using Android.Util;
using SQLite;
using System.Collections.Generic;

namespace wadtt
{
    public class Spider : Creature
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public Dimensions Size { get; set; }
        public int Count { get; set; }

        public enum Dimensions { baby, small, fullygrown, big, verylarge, enormous };

        public Spider()
        {
        }

        public Spider(Spider spider)
        {
            // Classic! there is no copy constructor in C#!!!
            ID = spider.ID;
            Size = spider.Size;
            Count = spider.Count;
            StartRoom = spider.StartRoom;
            Left = spider.Left;
            Right = spider.Right;
            Straight = spider.Straight;
            Back = spider.Back;

        }

        public string dimensionsToText()
        {
            switch (Size)
            {
                case Dimensions.baby:
                    return "baby";

                case Dimensions.small:
                    return "small";

                case Dimensions.fullygrown:
                    return "fully grown";

                case Dimensions.big:
                    return "big";

                case Dimensions.verylarge:
                    return "very large";

                case Dimensions.enormous:
                    return "enormous";

                default:
                    return "unknown sized";
            }
        }

        public static int GetTotalCount()
        {
            return Database.GetTotalCount("spider");
        }

        public static Spider GetSpider(int id)
        {
            try
            {
                string sql = "SELECT * FROM Spider WHERE ID=?";
                object[] args = { id };

                if (Database.db == null)
                {
                    return null;
                }

                List<Spider> spiders = Database.db.Query<Spider>(sql, args);
                if (spiders.Count == 0)
                {
                    Log.Error(MainActivity.TAG, "Oops we have no Spider #" + id);
                    return null;
                }
                return spiders[0];
            }
            catch (SQLiteException ex)
            {
                Log.Debug(MainActivity.TAG, ex.Message);
                return null;
            }
        }
    }
}