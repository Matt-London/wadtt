using System.Collections.Generic;

namespace wadtt
{
    public class Cats
    {
        private static List<ICreature> cats = new List<ICreature>();
        private static int wakeup = 2;

        private static string[] oneCat = { "A cat lies stretched out asleep in the corner.",
                                           "A cat walks casually up to you.",
                                           "There is a cat asleep in the corner.",
                                           "From the edge of the room a cat stares at you." };

        private static string[] twoCats = { " cats lie asleep in the corner.",
                                           " cats bound up to you purring.",
                                           " cats miaow loudly and scamper into the corner.",
                                           " pairs of cats eyes stare at you from the darkness." };

        public static string catSounds(int roomId)
        {
            return null;
        }

        public static string catSmells(int roomId)
        {
            return null;
        }

        public static string catsInRoomDescribe(int roomId)
        {
            int cats = catsInRoom(roomId);
            if (cats == 0)
            {
                return "";
            }


            if (cats == 1)
            {
                return Utils.getAny(oneCat);
            }

            return cats.ToString() + Utils.getAny(twoCats);
        }

        // Cat movement rules.
        // - Cats can swim, contrary to myth, but they don't like water.
        // - Cats like to eat small spiders
        // - Cats kill bigger spiders.
        // - Goblins like cats.
        // - Cats hate music.

        public static string locate()
        {
            if (wakeup > 0)
            {
                // They are still sleeping..
                return "";
            }

            string here = "";
            for (int i = 0; i < Cat.GetTotalCount(); ++i)
            {
                here += (here.Length > 0) ? ", " : "";
                here += "room " + cats[i].getCurrentRoom() + " ";
            }

            return " in " + here;
        }

        public static int catsInRoom(int roomId)
        {
            int catCount = 0;
            for (int i = 0; i < Cat.GetTotalCount(); ++i)
            {
                if (roomId == cats[i].getCurrentRoom())
                {
                    catCount++;
                }
            }

            return catCount;
        }

        public static int catsInRange(int roomId, int range)
        {
            Rooms.roomCondition f = delegate (int roomid)
            {
                return catsInRoom(roomid);
            };

            return Rooms.InRange(roomId, range, f);
        }

        public static Route shortestRouteToCat(int roomId)
        {
            Rooms.roomCondition f = delegate (int roomid)
            {
                return catsInRoom(roomid);
            };

            return Rooms.shortestRoute(roomId, f);
        }
    }
}