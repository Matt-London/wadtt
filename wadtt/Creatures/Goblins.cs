using Android.Util;
using System.Collections.Generic;

namespace wadtt
{
    public class Goblins
    {
        private static List<ICreature> goblins = new List<ICreature>();
        private static int wakeup = 10;

        // FIXME: 1. Goblins can steal things and other things too....
        // Goblin movement rules.
        // - Goblins enter the cave at the entrance.
        // - Goblins wake up after you have made <wakeup> moves.
        // - Goblins move in a manner determined by their attributes
        // - When Goblins get older they stay together.
        // - Goblins will finally die and new ones can enter the caves.
        // - One goblin is subservient
        // - Two goblins are cheeky and will generally take anything lying on the floor.
        // - Three goblins will generally steal an item from you.
        // - Four goblins steal all you own and dump you in lost property.
        // - Five goblins or more goblins will kill you.
        //
        // Except
        // 1. Goblins hate anything musical and will break it if on the floor,
        // 2. Avoid you if you carry it.
        // 3. Run away leaving anything they are carrying if you play it.

        public static string locate()
        {
            if (wakeup > 0)
            {
                // They are still sleeping..
                return "";
            }

            string here = "";
            for (int i = 0; i < Goblin.GetTotalCount(); ++i)
            {
                here += (here.Length > 0) ? ", " : "";
                here += "room " + goblins[i].getCurrentRoom() + " ";
            }

            return " in " + here;
        }

        private static string[] subservient = { "A solitary goblin runs away.",
                                                "A goblin bows lows and leaves.",
                                                "A single goblin studies you for a moment before leaving." };

        private static string[] rosemary = { "The air is strong with the smell of rosemary.",
                                             "The smell of rosemary hangs heavy in the air.",
                                             "There is the smell of rosemary.",
                                             "You can smell rosemary." };

        private static string[] apples = { "There is a smell of rotten apples.",
                                           "The air is sickly sweet with the smell of old apples.",
                                           "Something smells in here, a bit like old cider.",
                                           "There is a waft of old apples."};

        private static string[] thyme = { "The air is light with the smell of thyme.",
                                          "The smell of thyme hangs lazily in the air.",
                                          "There is the faint smell of thyme.",
                                          "You can faintly smell thyme." };

        private static string[] playing = { "You can hear a violin playing nearby.",
                                            "You hear the banging of a hammer.",
                                            "A drum is beaten in a nearby room.",
                                            "You can hear hear faint music playing."};

        private static string[] swearing = { "You can hear swearing from a nearby passage.",
                                             "You hear faint swearing.",
                                             "You can make out an unknown language, but it is clearly swear words.",
                                             "You think you can hear someone or something swearing."};

        private static string[] mutterings = { "Far away you can hear faint muttering.",
                                               "Deep in the caves you can hear faint muttering.",
                                               "You can hear the faint echo of someone whispering.",
                                               "You think you can hear someone or something swearing."};

        public static string goblinSounds(int roomId)
        {
            int goblins = goblinsInRoom(roomId);
            if (goblins == 0)
            {
                if (goblinsInRange(roomId, 1) > 0)
                {
                    return Utils.getAny(playing);
                }

                if (goblinsInRange(roomId, 2) > 0)
                {
                    return Utils.getAny(swearing);
                }

                if (goblinsInRange(roomId, 3) > 0)
                {
                    return Utils.getAny(mutterings);
                }
            }
            return "";
        }

        public static string goblinSmells(int roomId)
        {
            int goblins = goblinsInRoom(roomId);
            if (goblins == 0)
            {
                if (goblinsInRange(roomId, 1) > 0)
                {
                    return Utils.getAny(rosemary);
                }

                if (goblinsInRange(roomId, 2) > 0)
                {
                    return Utils.getAny(apples);
                }

                if (goblinsInRange(roomId, 3) > 0)
                {
                    return Utils.getAny(thyme);
                }
            }
            return "";
        }

        public static string goblinsInRoomDescribe(int roomId)
        {
            int goblins = goblinsInRoom(roomId);
            if (goblins == 0)
            {
                if (goblinsInRange(roomId, 1) > 0)
                {
                    return Utils.getAny(rosemary);
                }

                if (goblinsInRange(roomId, 2) > 0)
                {
                    return Utils.getAny(apples);
                }

                if (goblinsInRange(roomId, 3) > 0)
                {
                    return Utils.getAny(mutterings);
                }

                return "";
            }


            if (goblins == 1)
            {
                // One goblin is subservient.
                return Utils.getAny(subservient);
            }

            return "{0} goblins stand as you enter.";
        }

        public static int goblinsInRoom(int roomId)
        {
            int goblinCount = 0;
            for (int i = 0; i < Goblin.GetTotalCount(); ++i)
            {
                if ( roomId == goblins[i].getCurrentRoom() )
                {
                    goblinCount++;
                }
            }

            return goblinCount;
        }

        public static int goblinsInRange(int roomId, int range)
        {
            Rooms.roomCondition f = delegate(int roomid)
            {
                return goblinsInRoom(roomid);
            };

            return Rooms.InRange(roomId, range, f);
        }

        public static Route shortestRouteToGoblin(int roomId)
        {
            Rooms.roomCondition f = delegate (int roomid)
            {
                return goblinsInRoom(roomid);
            };

            return Rooms.shortestRoute(roomId, f);
        }
    }
}