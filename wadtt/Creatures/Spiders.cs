using Android.Util;
using System.Collections.Generic;

namespace wadtt
{
    public class Spiders
    {
        private static List<ICreature> spiders = new List<ICreature>();
        private static int wakeup = 2;

        // Cat movement rules.
        // - Cats can swim, contrary to myth, but they don't like water.
        // - Cats like to eat small spiders
        // - Cats kill bigger spiders.
        // - Goblins like cats.
        // - Cats hate music.

        public static string locate()
        {
            if (wakeup > 0)
            {
                // They are still sleeping..
                return "";
            }

            string here = "";
            for (int i = 0; i < Spider.GetTotalCount(); ++i)
            {
                here += (here.Length > 0) ? ", " : "";
                here += "room " + spiders[i].getCurrentRoom() + " ";
            }

            return " in " + here;
        }

        public static int spidersInRoom(int roomId)
        {
            int spiderCount = 0;
            for (int i = 0; i < Spider.GetTotalCount(); ++i)
            {
                if (roomId == spiders[i].getCurrentRoom())
                {
                    spiderCount++;
                }
            }

            return spiderCount;
        }

        public static int spidersInRange(int roomId, int range)
        {
            Rooms.roomCondition f = delegate (int roomid)
            {
                return spidersInRoom(roomid);
            };

            return Rooms.InRange(roomId, range, f);
        }

        public static Route shortestRouteToSpider(int roomId)
        {
            Rooms.roomCondition f = delegate (int roomid)
            {
                return spidersInRoom(roomid);
            };

            return Rooms.shortestRoute(roomId, f);
        }

        private static string[] cobwebs = { "Your hair brushes against cobwebs.",
                                            "Cobwebs float across your face.",
                                            "There are cobwebs everywhere." };

        private static string[] oneSpider = { "There is a ", "You see a ", "A "};

        private static string[] spiderWork = { " spinning a web.",
                                               " sitting silently in a web.",
                                               " staring at you from the cobwebs above. " };

        private static string[] plusSpiders = { "There are ", "You see ", "" };

        public static string spiderSounds(int roomId)
        {
            return null;
        }

        public static string spiderSmells(int roomId)
        {
            return null;
        }

        public static string spidersInRoomDescribe(int roomId)
        {
            int spiders = spidersInRoom(roomId);
            if (spiders == 0)
            {
                if (spidersInRange(roomId, 2) > 0)
                {
                    return Utils.getAny(cobwebs);
                }
                return "";
            }

            // Find the biggest spider size!!

            if (spiders == 1)
            {
                return Utils.getAny(oneSpider) + Utils.getAny(spiderWork);
            }

            return Utils.getAny(plusSpiders) + spiders.ToString() + Utils.getAny(plusSpiders);
        }
    }
}