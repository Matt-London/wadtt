using Android.Util;
using SQLite;
using System.Collections.Generic;

namespace wadtt
{
    public class Cat : Creature
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Desc { get; set; }
        public int Count { get; set; }

        public Cat()
        {
        }

        public Cat(Cat cat)
        {
            // Classic! there is no copy constructor in C#!!!
            ID = cat.ID;
            Desc = cat.Desc;
            Count = cat.Count;
            StartRoom = cat.StartRoom;
            Left = cat.Left;
            Right = cat.Right;
            Straight = cat.Straight;
            Back = cat.Back;
            
        }

        public static int GetTotalCount()
        {
            return Database.GetTotalCount("Cat");
        }

        public static Cat GetCat(int id)
        {
            try
            {
                string sql = "SELECT * FROM Cat WHERE ID=?";
                object[] args = { id };

                if (Database.db == null)
                {
                    return null;
                }

                List<Cat> items = Database.db.Query<Cat>(sql, args);
                if (items.Count == 0)
                {
                    Log.Error(MainActivity.TAG, "Oops we have no Cat #" + id);
                    return null;
                }
                return items[0];
            }
            catch (SQLiteException ex)
            {
                Log.Debug(MainActivity.TAG, ex.Message);
                return null;
            }
        }
    }
}