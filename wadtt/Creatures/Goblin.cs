using Android.Util;
using SQLite;
using System.Collections.Generic;

namespace wadtt
{
    public class Goblin : Creature
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public int Count { get; set; }
        public string Baggage { get; set; }

        public Goblin()
        {
            // Used by SQLite
        }

        public Goblin(Goblin goblin)
        {
            // Classic! there is no copy constructor in C#!!!
            ID = goblin.ID;
            Count = goblin.Count;
            Left = goblin.Left;
            Right = goblin.Right;
            Straight = goblin.Straight;
            Back = goblin.Back;
            Baggage = goblin.Baggage;
        }

        public static int GetTotalCount()
        {
            return Database.GetTotalCount("Goblin");
        }

        public static Goblin GetGoblin(int id)
        {
            try
            {
                string sql = "SELECT * FROM Goblin WHERE ID=?";
                object[] args = { id };

                if (Database.db == null)
                {
                    return null;
                }

                List<Goblin> items = Database.db.Query<Goblin>(sql, args);
                if (items.Count == 0)
                {
                    Log.Error(MainActivity.TAG, "Oops we have no Goblin #" + id);
                    return null;
                }
                return items[0];
            }
            catch (SQLiteException ex)
            {
                Log.Debug(MainActivity.TAG, ex.Message);
                return null;
            }
        }
    }
}