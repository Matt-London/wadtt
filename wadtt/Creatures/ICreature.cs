using System.Collections.Generic;

namespace wadtt
{
    public interface ICreature
    {
        Environment CurrentEnvironment { get; set; }

        int getNextRoom();
        int getCurrentRoom();
        int getID();

        void refresh();

        Route getRouteHere();
        string getRouteHereAsText();

        List<string> carrying();
    }
}