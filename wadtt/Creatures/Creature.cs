using Java.Util;
using Android.Util;
using System.Collections.Generic;

namespace wadtt
{
    public class Creature : ICreature
    {
        // This is a join of EnvironmentCreatureList & CreatureList & EnvironmentList
        // Some of these should be readonly I think!!
        public int ID { get; set; }
        public string Name { get; set; }
        public int Left { get; set; }
        public int Right { get; set; }
        public int Straight { get; set; }
        public int Back { get; set; }
        public int StartRoom { get; set; }
        public int Delay { get; set; }

        protected Route m_route = null;
        protected int m_roomId = 0;
        protected static direction m_next = direction.left;
        protected enum direction { left, right, straight, back };
        protected int m_left = 0;
        protected int m_right = 0;
        protected int m_straight = 0;
        protected int m_back = 0;

        public Environment CurrentEnvironment { get; set; }
        private string TAG = "Creature";

        public void refresh()
        {
            // Called first time a creature moves and then when move options are exhausted.
            m_next = getRandomNewDirection();
            m_left = Left;
            m_right = Right;
            m_straight = Straight;
            m_back = Back;
        }

        public int getFirstRoom()
        {
            if (StartRoom <= 0)
            {
                int newStartRoom = CurrentEnvironment.getStartRoomId();
                Log.Error(TAG, "Creature: " + ID + " StartRoom: " + StartRoom + " modified to: " + newStartRoom);
                StartRoom = newStartRoom;
            }
            return StartRoom;
        }

        public Creature()
        {
            m_route = new Route();
        }

        public bool register(Environment env)
        {
            CurrentEnvironment = env;
            return true;
        }

        public int getID()
        {
            return ID;
        }

        public int getCurrentRoom()
        {
            if (m_roomId == 0)
            {
                m_roomId = getFirstRoom();
            }

            return m_roomId;
        }

        public void setCurrentRoom(int roomId)
        {
            m_roomId = roomId;
        }

        protected direction getRandomNewDirection()
        {
            Random random = new Random();
            bool a = random.NextBoolean();
            bool b = random.NextBoolean();

            if (a && b)
            {
                Log.Debug(TAG, "Creature: " + ID + " moves left");
                return direction.left;
            }

            if (a)
            {
                Log.Debug(TAG, "Creature: " + ID + " moves right");
                return direction.right;
            }

            if (b)
            {
                Log.Debug(TAG, "Creature: " + ID + " moves straight on");
                return direction.straight;
            }

            Log.Debug(TAG, "Creature: " + ID + " goes back");
            return direction.back;
        }

        public int getNextRoom()
        {
            int skipped = 0;
            bool looping = false;
            while (true)
            {
                if (skipped > 3)
                {
                    // We cannot find a route with our existing combination of back, right, left, straight options.
                    refresh();
                    if (looping)
                    {
                        Log.Debug(TAG, "Creature: " + ID + " is struggling to leave room " + getCurrentRoom());
                        return getCurrentRoom();
                    }
                    looping = true;
                    skipped = 0;
                }

                switch (m_next)
                {
                    case direction.left:
                        m_next = direction.right;
                        if (m_left > 0)
                        {
                            m_left--;
                            int l = Room.GetRoomById(getCurrentRoom()).Left;
                            if (l != 0)
                            {
                                // Save the current room before we switch.
                                m_route.saveRoom(getCurrentRoom());
                                setCurrentRoom(l);
                                return l;
                            }
                        }
                        skipped++;
                        break;

                    case direction.right:
                        m_next = direction.straight;
                        if (m_right > 0)
                        {
                            m_right--;
                            int r = Room.GetRoomById(getCurrentRoom()).Right;
                            if (r != 0)
                            {
                                // Save the current room before we switch.
                                m_route.saveRoom(getCurrentRoom());
                                setCurrentRoom(r);
                                return r;
                            }
                        }
                        skipped++;
                        break;

                    case direction.straight:
                        m_next = direction.back;
                        if (m_straight > 0)
                        {
                            m_straight--;
                            int s = Room.GetRoomById(getCurrentRoom()).Straight;
                            if (s != 0)
                            {
                                // Save the current room before we switch.
                                m_route.saveRoom(getCurrentRoom());
                                setCurrentRoom(s);
                                return s;
                            }
                        }
                        skipped++;
                        break;

                    case direction.back:
                        m_next = direction.left;
                        if (m_back > 0)
                        {
                            m_back--;
                            if (m_route.getCount() > 0)
                            {
                                // A goblin can always retreat.
                                if (m_route.goBack(ref m_roomId))
                                {
                                    return getCurrentRoom();
                                }
                            }
                        }
                        skipped++;
                        break;
                }
            }
        }

        public Route getRouteHere()
        {
            return m_route;
        }

        public string getRouteHereAsText()
        {
            return m_route.routeAsText();
        }

        public List<string> carrying()
        {
            IBaggageHandler baggageHandler = CurrentEnvironment.getBaggageHandler();
            return baggageHandler.getManifest(this);
        }
    }
}