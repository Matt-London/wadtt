namespace wadtt
{

    // Now we can do clever stuff like recognise:
    // <verb><item>
    // Of course it doesn't mean it makes sense. It's purely the intent we understand
    // "eat boat"
    // "stroke goblin"
    // "speak to mirror"
    public class POSCheck
    {
        public static bool isMoveVerb(Lexicon.lex token)
        {
            switch (token)
            {
                case Lexicon.lex.ascend:
                case Lexicon.lex.back:
                case Lexicon.lex.climb:
                case Lexicon.lex.descend:
                case Lexicon.lex.exit:
                case Lexicon.lex.forward:
                case Lexicon.lex.go:
                case Lexicon.lex.leave:
                case Lexicon.lex.pass:
                case Lexicon.lex.retreat:
                case Lexicon.lex.Return:
                case Lexicon.lex.run:
                case Lexicon.lex.walk:
                    return true;

                default:
                    return false;
            }
        }

        public static bool isCreature(Lexicon.lex token)
        {
            switch (token)
            {
                case Lexicon.lex.cat:
                case Lexicon.lex.cats:
                case Lexicon.lex.goblin:
                case Lexicon.lex.goblins:
                case Lexicon.lex.spider:
                case Lexicon.lex.spiders:
                    return true;

                default:
                    return false;
            }
        }

        public static bool isPersonalPronoun(Lexicon.lex token)
        {
            switch (token)
            {
                case Lexicon.lex.i:
                case Lexicon.lex.we:
                case Lexicon.lex.you:
                    return true;

                default:
                    return false;
            }
        }

        public static bool isIndefiniteAdverb(Lexicon.lex token)
        {
            switch (token)
            {
                case Lexicon.lex.anything:
                case Lexicon.lex.something:
                case Lexicon.lex.someone:
                case Lexicon.lex.anyone:
                    return true;

                default:
                    return false;
            }
        }

        public static bool isItem(Lexicon.lex token)
        {
            switch (token)
            {
                case Lexicon.lex.bed:
                case Lexicon.lex.boat:
                case Lexicon.lex.bottle:
                case Lexicon.lex.bowl:
                case Lexicon.lex.broom:
                case Lexicon.lex.can:
                case Lexicon.lex.candle:
                case Lexicon.lex.candles:
                case Lexicon.lex.ceiling:
                case Lexicon.lex.chair:
                case Lexicon.lex.coin:
                case Lexicon.lex.coins:
                case Lexicon.lex.cup:
                case Lexicon.lex.drink:
                case Lexicon.lex.floor:
                case Lexicon.lex.flute:
                case Lexicon.lex.fork:
                case Lexicon.lex.gold:
                case Lexicon.lex.ground:
                case Lexicon.lex.head:
                case Lexicon.lex.key:
                case Lexicon.lex.knife:
                case Lexicon.lex.liquid:
                case Lexicon.lex.Lock:
                case Lexicon.lex.match:
                case Lexicon.lex.matches:
                case Lexicon.lex.mattress:
                case Lexicon.lex.mirror:
                case Lexicon.lex.noise:
                case Lexicon.lex.noises:
                case Lexicon.lex.porridge:
                case Lexicon.lex.smell:
                case Lexicon.lex.smells:
                case Lexicon.lex.sound:
                case Lexicon.lex.sounds:
                case Lexicon.lex.spoon:
                case Lexicon.lex.table:
                case Lexicon.lex.tin:
                case Lexicon.lex.wall:
                case Lexicon.lex.walls:
                case Lexicon.lex.water:
                case Lexicon.lex.whistle:
                case Lexicon.lex.window:
                case Lexicon.lex.windows:
                    return true;

                default:
                    return false;
            }
        }

        public static bool isLocation(Lexicon.lex token)
        {
            switch (token)
            {
                case Lexicon.lex.cave:
                case Lexicon.lex.cavern:
                case Lexicon.lex.cellar:
                case Lexicon.lex.chamber:
                case Lexicon.lex.distance:
                case Lexicon.lex.hall:
                case Lexicon.lex.location:
                case Lexicon.lex.place:
                case Lexicon.lex.room:
                case Lexicon.lex.stairway:
                case Lexicon.lex.tunnel:
                case Lexicon.lex.tunnels:
                    return true;

                default:
                    return false;
            }
        }

        public static bool isNumber(Lexicon.lex token)
        {
            switch (token)
            {
                case Lexicon.lex.one:
                case Lexicon.lex.two:
                case Lexicon.lex.three:
                case Lexicon.lex.four:
                case Lexicon.lex.five:
                case Lexicon.lex.six:
                case Lexicon.lex.seven:
                    return true;

                default:
                    return false;
            }
        }

        public static bool isOpening(Lexicon.lex token)
        {
            switch (token)
            {
                case Lexicon.lex.door:
                case Lexicon.lex.opening:
                case Lexicon.lex.passage:
                case Lexicon.lex.staircase:
                case Lexicon.lex.stairs:
                case Lexicon.lex.stairway:
                case Lexicon.lex.tunnel:
                    return true;

                default:
                    return false;
            }
        }

        public static bool isHandVerb(Lexicon.lex token)
        {
            switch (token)
            {
                case Lexicon.lex.bang:
                case Lexicon.lex.Break:
                case Lexicon.lex.carry:
                case Lexicon.lex.close:
                case Lexicon.lex.drop:
                case Lexicon.lex.get:
                case Lexicon.lex.hold:
                case Lexicon.lex.knock:
                case Lexicon.lex.light:
                case Lexicon.lex.Lock:
                case Lexicon.lex.pay:
                case Lexicon.lex.pick:
                case Lexicon.lex.place:
                case Lexicon.lex.plant:
                case Lexicon.lex.pull:
                case Lexicon.lex.push:
                case Lexicon.lex.put:
                case Lexicon.lex.strike:
                case Lexicon.lex.stroke:
                case Lexicon.lex.sweep:
                case Lexicon.lex.take:
                case Lexicon.lex.Throw:
                case Lexicon.lex.tie:
                case Lexicon.lex.turn:
                case Lexicon.lex.unhook:
                case Lexicon.lex.untie:
                case Lexicon.lex.use:
                    return true;

                default:
                    return false;
            }
        }

        public static bool isSenseVerb(Lexicon.lex token)
        {
            switch (token)
            {
                case Lexicon.lex.hear:
                case Lexicon.lex.see:
                case Lexicon.lex.smell:
                    return true;

                default:
                    return false;
            }
        }

        public static bool isAmOrAre(Lexicon.lex token)
        {
            switch (token)
            {
                case Lexicon.lex.am:
                case Lexicon.lex.are:
                    return true;

                default:
                    return false;
            }
        }

        public static bool isCanOrDo(Lexicon.lex token)
        {
            switch (token)
            {
                case Lexicon.lex.can:
                case Lexicon.lex.Do:
                    return true;

                default:
                    return false;
            }
        }

        public static bool isTheOrThis(Lexicon.lex token)
        {
            switch (token)
            {
                case Lexicon.lex.the:
                case Lexicon.lex.This:
                    return true;

                default:
                    return false;
            }
        }

        public static bool isTheWord(Lexicon.lex token)
        {
            switch (token)
            {
                case Lexicon.lex.my:
                case Lexicon.lex.our:
                case Lexicon.lex.the:
                case Lexicon.lex.your:
                    return true;

                default:
                    return false;
            }
        }

        public static bool isDirection(Lexicon.lex token)
        {
            switch (token)
            {
                case Lexicon.lex.back:
                case Lexicon.lex.down:
                case Lexicon.lex.forward:
                case Lexicon.lex.left:
                case Lexicon.lex.right:
                case Lexicon.lex.straight:
                case Lexicon.lex.up:
                    return true;

                default:
                    return false;
            }
        }

        public static bool isMultipleDeterminer(Lexicon.lex token)
        {
            switch (token)
            {
                case Lexicon.lex.any:
                case Lexicon.lex.many:
                case Lexicon.lex.some:
                    return true;

                default:
                    return false;
            }
        }

        public static bool isSingleDeterminer(Lexicon.lex token)
        {
            switch (token)
            {
                case Lexicon.lex.a:
                case Lexicon.lex.an:
                case Lexicon.lex.any:
                case Lexicon.lex.the:
                    return true;

                default:
                    return false;
            }
        }

        public static bool isGeneralPlace(Lexicon.lex token)
        {
            switch (token)
            {
                case Lexicon.lex.about:
                case Lexicon.lex.around:
                case Lexicon.lex.available:
                case Lexicon.lex.here:
                case Lexicon.lex.present:
                case Lexicon.lex.visible:
                    return true;

                default:
                    return false;
            }
        }
    }
}