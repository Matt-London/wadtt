using Android.Speech.Tts;
using Android.Util;
using System.Collections.Generic;

/*
 *     [UI] Voice input
 *               |
 *               V
 *     [UI] A string of words:  "I think I will have a sleep on the bed"
 *               |
 *               V
 *    [Tokeniser] Splits words into an array of lowercase strings:
 *                "i", "think", "i", "will", "have", "a", "sleep", "on", "the", "bed"
 *               |
 *               V
 *    [Tokeniser] Converts into a Queue of lexical tokens:
 *                LinkedList<Lexicon.Token> : lex.i, lex.think, lex,i, lex.will, lex.have, lex.a,
 *                                          lex.sleep, lex.on. lex.the, lex.bed
 *
 *    [Tokeniser] Reduce to remove any words that add nothing to the meaning
 *
 *    [POF]       Look for simple sentences
 *                verb
 *                verb noun
 *
 */
namespace wadtt
{
    public class Engine
    {
        public static void enterRoom(Game game, IPlayer player, int roomId)
        {
            player.enterRoom(roomId);
            game.getSound().newSound(game.getActivity(), player.CurrentRoom.Sound);

            string around = player.lookAround(false);
            game.getTextToSpeech().Speak(around, QueueMode.Flush, null);
            UI.display(game, player, around);
        }

        private static string[] noWay = { "I see no way ", "There is no way ", "You can't go ",
                                          "I see no passages leading ", "There is no route "};
        private static string[] what = { "I really don't understand ", "what is ", "Sorry I don't understand " };
        private static string[] ups =   { "There is more than one way leading up?", "Which way up do you mean?" };
        private static string[] downs = { "There is more than one way down?", "Which way down do you mean?" };

        public static void execute(LinkedList<Lexicon.Token> tokens, Game game, IPlayer player)
        {
            string response = null;

            if (tokens.Count == 1)
            {
                Lexicon.lex token = tokens.First.Value.lex;
                response = executeSingleAction(token, game, player);
            }
            else
            {
                // take night vision glasses
                response = executeSimpleInterAction(tokens, game, player);
            }

            // FIXME: Not the final resting place, but after every user move, creatures can move.
            player.CurrentEnvironment.getCreatureHandler().move();

            if (response != null)
            {
                game.getTextToSpeech().Speak(response, QueueMode.Flush, null);
            }

        }

        private static void executeMoveRoom(Lexicon.lex action, Game game, IPlayer player)
        {
            int newRoomId = 0;
            switch (action)
            {
                case Lexicon.lex.left:
                    newRoomId = player.CurrentRoom.Left;
                    break;

                case Lexicon.lex.right:
                    newRoomId = player.CurrentRoom.Right;
                    break;

                case Lexicon.lex.straight:
                    newRoomId = player.CurrentRoom.Straight;
                    break;

                default:
                    return;
            }

            player.leaveCurrentRoom();
            enterRoom(game, player, newRoomId);
        }

        enum Way { None = 0, OneWay = 1, MultipleWays = 2 };
        private static Way findWay(Lexicon.lex action, IPlayer player)
        {
            switch (action)
            {
                case Lexicon.lex.left:
                    if (player.CurrentRoom.Left == 0)
                    {
                        return Way.None;
                    }
                    break;

                case Lexicon.lex.right:
                    if (player.CurrentRoom.Right == 0)
                    {
                        return Way.None;
                    }
                    break;

                case Lexicon.lex.straight:
                case Lexicon.lex.forward:
                    if (player.CurrentRoom.Straight == 0)
                    {
                        return Way.None;
                    }
                    break;

                default:
                    return Way.None;
            }
            return Way.OneWay;
        }

        public static Items.ItemState cleverNounMatch(LinkedList<Lexicon.Token> tokens, out int itemId, out string describe)
        {
            string nounSignal = null;
            string noun;
            if (tokens.Count > 0)
            {
                var token = tokens.First.Value.lex;
                if (token == Lexicon.lex.the)
                {
                    tokens.RemoveFirst();
                    nounSignal = token.ToString();
                }
            }

            Items.ItemState itemState = nounMatch(tokens, out itemId, out noun);

            describe = (nounSignal != null) ? (nounSignal + " " + noun) : noun;
            return itemState;
        }

        public static Items.ItemState nounMatch(LinkedList<Lexicon.Token> tokens, out int itemId, out string describe)
        {
            Items.ItemState bestState = Items.ItemState.unknown;
            itemId = 0;
            describe = "";

            var enumerator = tokens.GetEnumerator();
            int wordCount = 0;
            while (enumerator.MoveNext())
            {
                wordCount++;
                var nextToken = enumerator.Current.lex;
                if (describe.Length > 0)
                {
                    describe += " ";
                }
                describe += (nextToken == Lexicon.lex.unknown) ? enumerator.Current.raw : nextToken.ToString();
                Items.ItemState state = Items.getItemId(describe, out itemId);
                if (state == Items.ItemState.known)
                {
                    for (int i = 1; i <= wordCount; i++)
                    {
                        // We have matched these words as a noun.
                        tokens.RemoveFirst();
                    }
                    return state;
                }

                bestState = (state == Items.ItemState.ambiguous) ? state : bestState;
            }

            return bestState;
        }

        public static string executeSimpleInterAction(LinkedList<Lexicon.Token> tokens, 
                        Game game, IPlayer player)
        {
            IBaggageHandler baggageHandler = player.CurrentEnvironment.getBaggageHandler();
            string response = null;

            if (tokens.Count != 2)
            {
                response = Utils.getAny(what) + tokens.Count + " tokens";
            }

            var verb = tokens.First.Value;
            tokens.RemoveFirst();
            var noun = tokens.First.Value;
            Log.Debug(MainActivity.TAG, "Executing simple interaction: {0} {1}", verb.lex, noun.lex);

            switch (verb.lex)
            {
                case Lexicon.lex.drop:
                    if (noun.lex == Lexicon.lex.all)
                    {
                        int count = baggageHandler.transferAllItems(player, player.CurrentRoom);
                        string s = (count > 1) ? "s" : "";
                        response = (count == 0) ? "You were carrying nothing."
                                                : "You have discarded " + count + " item" + s + ".";
                        break;
                    }
                    else
                    {
                        int itemId;
                        string describe;
                        var state = cleverNounMatch(tokens, out itemId, out describe);
                        if (state == Items.ItemState.unknown)
                        {
                            response = "I don't know what a " + describe + "is";
                            break;
                        }

                        if (state == Items.ItemState.ambiguous)
                        {
                            response = "There is more than one item that matches " + describe;
                            break;
                        }

                        if (!baggageHandler.transferItem(itemId, player, player.CurrentRoom))
                        {
                            response = "I cannot see " + describe;
                            break;
                        }

                        response = "You drop " + describe;
                    }
                    break;

                case Lexicon.lex.look:
                case Lexicon.lex.see:
                    if ((noun.lex == Lexicon.lex.here) || (noun.lex == Lexicon.lex.near))
                    {
                        return player.lookAround(true);
                    }

                    response = "Unknown item " + noun;
                    break;

                case Lexicon.lex.take:
                    if (noun.lex == Lexicon.lex.all)
                    {
                        int count = baggageHandler.transferAllItems(player.CurrentRoom, player);
                        string s = (count > 1) ? "s" : "";
                        response = (count == 0) ? "The room is empty."
                                                : "You have picked up " + count + " item" + s + ".";
                        break;
                    }
                    else
                    {
                        int itemId;
                        string describe;
                        var state = cleverNounMatch(tokens, out itemId, out describe);
                        if (state == Items.ItemState.unknown)
                        {
                            response = "I don't know what a " + describe + "is";
                            break;
                        }

                        if (state == Items.ItemState.ambiguous)
                        {
                            response = "There is more than one item that matches " + describe;
                            break;
                        }

                        if (!baggageHandler.transferItem(itemId, player.CurrentRoom, player))
                            {
                            response = "I cannot see " + describe;
                            break;
                        }

                        response = "You pick up " + describe;
                    }
                    break;

                case Lexicon.lex.what:
                    switch (noun.lex)
                    {
                        case Lexicon.lex.carry:
                        case Lexicon.lex.carrying:
                        case Lexicon.lex.got:
                        case Lexicon.lex.have:
                        case Lexicon.lex.hold:
                        case Lexicon.lex.holding:
                            var carriage = player.bagContents();
                            response = (carriage.Length == 0) ? "You are carrying nothing." : carriage;
                            break;

                        default:
                            break;
                    }
                    break;

                default:
                    string verbWord = (verb.lex != Lexicon.lex.unknown) ? verb.lex.ToString() : verb.raw;
                    string nounWord = (noun.lex != Lexicon.lex.unknown) ? noun.lex.ToString() : noun.raw;
                    response = "I don't understand the phrase " + verbWord + " and " + nounWord;
                    break;
            }

            return response;

        }

        private static string executeSingleAction(Lexicon.lex action, 
                    Game game, IPlayer player)
        {
            Log.Debug(MainActivity.TAG, "Executing single action: {0}", action);
            string response = null;

            // We process som every simple commands in this function:
            // left, right, straight, up, down, back
            // hear, smell, see
            // hint, help, score
            // tides, route, baggage
            // goblins, cats, spiders

            switch (action)
            {
                case Lexicon.lex.hear:
                case Lexicon.lex.listen:
                case Lexicon.lex.sound:
                case Lexicon.lex.sounds:
                    {
                        response = player.listen();
                    }
                    break;

                case Lexicon.lex.smell:
                case Lexicon.lex.smells:
                    response = player.smell();
                    break;

                case Lexicon.lex.see:
                case Lexicon.lex.look:
                    {
                        response = player.lookAround(true);
                    }
                    break;

                case Lexicon.lex.hint:
                    response = Hint.getEasyHint();
                    break;

                case Lexicon.lex.score:
                    response = Score.getScoreText();
                    break;

                case Lexicon.lex.help:
                    response = "I cannot help you, though I can provide hints at a price";
                    break;

                case Lexicon.lex.tide:
                case Lexicon.lex.tides:
                    {
                        int tideLevel = Tide.checkTide();
                        response = "You are on level " + player.CurrentRoom.Level + " and the tide is level " + tideLevel;
                    }
                    break;

                case Lexicon.lex.route:
                    {
                        string route = player.getRouteHereAsText();
                        response = "The route here is " + ((route.Length > 0) ? route : "empty");
                    }
                    break;

                case Lexicon.lex.baggage:
                case Lexicon.lex.carrying:
                    {
                        var carriage = player.bagContents();
                        response = (carriage.Length == 0) ? "You are carrying nothing." : carriage;
                    }
                    break;

                case Lexicon.lex.goblin:
                case Lexicon.lex.goblins:
                    {
                        string locations = Goblins.locate();
                        response = "Goblins are " + ((locations.Length > 0) ? locations : "asleep");
                    }
                    break;

                case Lexicon.lex.cat:
                case Lexicon.lex.cats:
                    {
                        string locations = Cats.locate();
                        response = "Cats are " + ((locations.Length > 0) ? locations : "asleep");
                    }
                    break;

                case Lexicon.lex.spider:
                case Lexicon.lex.spiders:
                    {
                        string locations = Spiders.locate();
                        response = "Spiders are " + ((locations.Length > 0) ? locations : "asleep");
                    }
                    break;

                case Lexicon.lex.forward:
                case Lexicon.lex.left:
                case Lexicon.lex.right:
                case Lexicon.lex.straight:
                    Way way = findWay(action, player);
                    if (way == Way.None)
                    {
                        response = Utils.getAny(noWay) + action.ToString();
                        break;
                    }

                    executeMoveRoom(action, game, player);
                    break;

                case Lexicon.lex.Return:
                    {
                        int currentRoomId = player.CurrentRoom.ID;
                        if (!player.getRouteHere().goBack(ref currentRoomId))
                        {
                            response = Utils.getAny(noWay) + "back.";
                            break;
                        }

                        enterRoom(game, player, currentRoomId);
                    }
                    break;

                case Lexicon.lex.ascend:
                case Lexicon.lex.up:
                    {
                        bool ambiguous = false;
                        int currentRoomId = player.CurrentRoom.ID;
                        int roomUp = 0;
                        if (player.CurrentRoom.Left != 0)
                        {
                            if (Rooms.getGradient(currentRoomId, player.CurrentRoom.Left) > 0)
                            {
                                roomUp = player.CurrentRoom.Left;
                            }
                        }

                        if ( (!ambiguous) && (player.CurrentRoom.Right != 0))
                        {
                            if (Rooms.getGradient(currentRoomId, player.CurrentRoom.Right) > 0)
                            {
                                if (roomUp != 0)
                                {
                                    ambiguous = true;
                                }
                                roomUp = player.CurrentRoom.Right;
                            }
                        }

                        if ((!ambiguous) && (player.CurrentRoom.Straight != 0))
                        {
                            if (Rooms.getGradient(currentRoomId, player.CurrentRoom.Straight) > 0)
                            {
                                if (roomUp != 0)
                                {
                                    ambiguous = true;
                                }
                                roomUp = player.CurrentRoom.Straight;
                            }
                        }

                        if (ambiguous)
                        {
                            response = Utils.getAny(ups);
                        }
                        else if (roomUp == 0)
                        {
                            response = Utils.getAny(noWay) + "up";
                        }
                        else
                        {
                            player.leaveCurrentRoom();
                            enterRoom(game, player, roomUp);
                        }
                    }
                    break;

                case Lexicon.lex.descend:
                case Lexicon.lex.down:
                    {
                        bool ambiguous = false;
                        int currentRoomId = player.CurrentRoom.ID;
                        int roomDown = 0;
                        if (player.CurrentRoom.Left != 0)
                        {
                            if (Rooms.getGradient(currentRoomId, player.CurrentRoom.Left) < 0)
                            {
                                roomDown = player.CurrentRoom.Left;
                            }
                        }

                        if ((!ambiguous) && (player.CurrentRoom.Right != 0))
                        {
                            if (Rooms.getGradient(currentRoomId, player.CurrentRoom.Right) < 0)
                            {
                                if (roomDown != 0)
                                {
                                    ambiguous = true;
                                }
                                roomDown = player.CurrentRoom.Right;
                            }
                        }

                        if ((!ambiguous) && (player.CurrentRoom.Straight != 0))
                        {
                            if (Rooms.getGradient(currentRoomId, player.CurrentRoom.Straight) < 0)
                            {
                                if (roomDown != 0)
                                {
                                    ambiguous = true;
                                }
                                roomDown = player.CurrentRoom.Straight;
                            }
                        }

                        if (ambiguous)
                        {
                            response = Utils.getAny(downs);
                        }
                        else if (roomDown == 0)
                        {
                            response = Utils.getAny(noWay) + "down";
                        }
                        else
                        {
                            player.leaveCurrentRoom();
                            enterRoom(game, player, roomDown);
                        }
                    }
                    break;

                default:
                    response = Utils.getAny(what) + action.ToString();
                    break;
            }

            return response;
        }
    }
}
