using System.Collections.Generic;

namespace wadtt
{
    public class Parser
    {
        public static LinkedList<Lexicon.Token> findTokens(string input)
        {
            var tokeniser = new Tokeniser(input);
            return tokeniser.getTokens();
        }
    }
}