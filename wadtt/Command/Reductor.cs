using System.Collections.Generic;

// Note that we prefer lots of small transformations rather than a larger more specific one.
// So for example "are" "there" "any" "biscuits" "here"
//                "are" "any" "biscuits" "here"
//                "biscuits" "here"
// And leave it at that for reduction because "here" and "around" are subtly different I think.
//
namespace wadtt
{
    public class Reductor
    {
        public static bool reduceToLeft(LinkedList<Lexicon.Token> q, POS.pos pos)
        {
            // For example: left arch -> left
            if (q.Count < 1)
            {
                return false;
            }

            // Lookahead to next token.
            var nToken = q.First.Value.lex;
            if (!POS.POSMatch(pos, nToken))
            {               
                return false;
            }

            // We have matched on token lookahead, so discard the next token we would be reading.
            q.RemoveFirst();
            return true;
        }

        public static bool reduceToLeft(LinkedList<Lexicon.Token> q, Lexicon.lex expectedNtoken, POS.pos nnpos)
        {
            // For example: <token just read> <Expected Next Token> <Part of Speech nnpos> --> <token just read>

            // Lookahead to next token.
            // Initially, the enumerator is positioned before the first element in the collection.
            // At this position, Current is undefined. Therefore, you must call MoveNext to advance
            // the enumerator to the first element of the collection before reading the value of Current.
            var enumerator = q.GetEnumerator();
            if (!enumerator.MoveNext())
            {
                return false;
            }

            var token = enumerator.Current.lex;
            if (token != expectedNtoken)
            {
                return false;
            }

            // Lookahead to next token.
            if (!enumerator.MoveNext())
            {
                return false;
            }

            token = enumerator.Current.lex;
            if (!POS.POSMatch(nnpos, token))
            {
                return false;
            }

            // We have matched on both lookaheads, so discard the next two tokens.
            q.RemoveFirst();
            q.RemoveFirst();
            return true;
        }

        public static bool reduceToLeft(LinkedList<Lexicon.Token> q, POS.pos npos, POS.pos nnpos)
        {
            // For example: <token just read> <Part of Speech npos> <Part of Speech nnpos> --> <token just read>

            // Lookahead to next token.
            // Initially, the enumerator is positioned before the first element in the collection.
            // At this position, Current is undefined. Therefore, you must call MoveNext to advance
            // the enumerator to the first element of the collection before reading the value of Current.
            var enumerator = q.GetEnumerator();
            if (!enumerator.MoveNext())
            {
                return false;
            }

            var token = enumerator.Current.lex;
            if (!POS.POSMatch(npos, token))
            {
                return false;
            }

            // Lookahead to next token.
            if (!enumerator.MoveNext())
            {
                return false;
            }

            token = enumerator.Current.lex;
            if (!POS.POSMatch(nnpos, token))
            {
                return false;
            }

            // We have matched on both lookaheads, so discard the next two tokens.
            q.RemoveFirst();
            q.RemoveFirst();
            return true;
        }

        public static bool reduceToRight(LinkedList<Lexicon.Token> q, ref Lexicon.lex token, POS.pos npos)
        {
            // For example: <token just read> <Part of Speech npos> --> <token represented by Part of Speech npos>
            if (q.Count < 1)
            {
                return false;
            }

            // Lookahead to next token.
            var nToken = q.First.Value;
            if (!POS.POSMatch(npos, nToken.lex))
            {
                return false;
            }

            // Use the next token, discarding the current one.
            q.RemoveFirst();
            token = nToken.lex;
            return true;
        }

        public static bool reduceToRight(LinkedList<Lexicon.Token> q, ref Lexicon.lex token, POS.pos npos, POS.pos nnpos)
        {
            // For example: can i|we|you, hear|listen|see -> hear|listen|see
            var enumerator = q.GetEnumerator();
            if (!enumerator.MoveNext())
            {
                return false;
            }

            var nextToken = enumerator.Current.lex;
            if (!POS.POSMatch(npos, nextToken))
            {
                return false;
            }

            // Lookahead to next token.
            if (!enumerator.MoveNext())
            {
                return false;
            }

            nextToken = enumerator.Current.lex;
            if (!POS.POSMatch(nnpos, nextToken))
            {
                return false;
            }

            // We have matched on both lookaheads, so discard the current and next token and make the following the current
            // Example: "i will swim" -> "swim"
            q.RemoveFirst();
            q.RemoveFirst();
            token = nextToken;
            return true;
        }


        // Actually reduce to right. LookAhead:1
        public static bool reduce(LinkedList<Lexicon.Token> q, Lexicon.lex expectedNToken)
        {
            // "are there goblins here" -> "are goblins here"
            if (q.Count < 1)
            {
                return false;
            }

            // Lookahead at next token..
            var nToken = q.First.Value.lex;
            if (nToken != expectedNToken)
            {
                return false;
            }

            q.RemoveFirst();
            return true;
        }

        // Actually reduce to right. LookAhead:2
        public static bool reduce(LinkedList<Lexicon.Token> q, Lexicon.lex expectedNToken, Lexicon.lex expectedNNToken)
        {
            // Lookahead one token.
            var enumerator = q.GetEnumerator();
            if (!enumerator.MoveNext())
            {
                return false;
            }

            var nextToken = enumerator.Current.lex;
            if (nextToken != expectedNToken)
            {
                return false;
            }

            // Lookahead two tokens.
            if (!enumerator.MoveNext())
            {
                return false;
            }

            nextToken = enumerator.Current.lex;
            if (nextToken != expectedNNToken)
            {
                return false;
            }

            // We have matched on both lookaheads, so discard the current and next token.
            // Example: "are there any" -> "are"
            q.RemoveFirst();
            q.RemoveFirst();
            return true;
        }

        // Actually reduce to right. LookAhead:3
        public static bool reduce(LinkedList<Lexicon.Token> q,
                            Lexicon.lex expectedNToken, Lexicon.lex expectedNNToken,
                            Lexicon.lex expectedNNNToken)
        {
            // Lookahead one token.
            var enumerator = q.GetEnumerator();
            if (!enumerator.MoveNext())
            {
                return false;
            }

            var nextToken = enumerator.Current.lex;
            if (nextToken != expectedNToken)
            {
                return false;
            }

            // Lookahead two tokens.
            if (!enumerator.MoveNext())
            {
                return false;
            }

            nextToken = enumerator.Current.lex;
            if (nextToken != expectedNNToken)
            {
                return false;
            }

            // Lookahead three tokens.
            if (!enumerator.MoveNext())
            {
                return false;
            }

            nextToken = enumerator.Current.lex;
            if (nextToken != expectedNNNToken)
            {
                return false;
            }

            // We have matched on both lookaheads, so discard the current and next token.
            // Example: "give me a hint" -> "hint"
            q.RemoveFirst();
            q.RemoveFirst();
            q.RemoveFirst();
            return true;
        }

        // Actually reduce to right. LookAhead:4
        public static bool reduce(LinkedList<Lexicon.Token> q,
                    Lexicon.lex expectedNToken, Lexicon.lex expectedNNToken,
                    Lexicon.lex expectedNNNToken, Lexicon.lex expectedNNNNToken)
        {
            // Lookahead one token.
            var enumerator = q.GetEnumerator();
            if (!enumerator.MoveNext())
            {
                return false;
            }

            var nextToken = enumerator.Current.lex;
            if (nextToken != expectedNToken)
            {
                return false;
            }

            // Lookahead two tokens.
            if (!enumerator.MoveNext())
            {
                return false;
            }

            nextToken = enumerator.Current.lex;
            if (nextToken != expectedNNToken)
            {
                return false;
            }

            // Lookahead three tokens.
            if (!enumerator.MoveNext())
            {
                return false;
            }

            nextToken = enumerator.Current.lex;
            if (nextToken != expectedNNNToken)
            {
                return false;
            }

            // Lookahead four tokens.
            if (!enumerator.MoveNext())
            {
                return false;
            }

            nextToken = enumerator.Current.lex;
            if (nextToken != expectedNNNNToken)
            {
                return false;
            }

            // We have matched on both lookaheads, so discard the current and next token.
            // Example: "can i have a hint" -> "hint"
            q.RemoveFirst();
            q.RemoveFirst();
            q.RemoveFirst();
            q.RemoveFirst();
            return true;
        }

        public static Lexicon.Token reduceLocation(LinkedList<Lexicon.Token> q, Lexicon.lex token)
        {
            if (reduceToLeft(q, POS.pos.LOCATION))
            {
                return new Lexicon.Token(token);
            }
            if (reduceToLeft(q, POS.pos.OPENING))
            {
                return new Lexicon.Token(token);
            }
            return new Lexicon.Token(token);
        }
    }
}